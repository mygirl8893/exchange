<?php
function is_email($email){
    if(eregi("^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email))
		return true;
	else
		return false;
}

function coin($coin){
    $arr = explode('.',$coin);
	if(count($arr) == 2){
	    return $arr[0].'.'.substr($arr[1],0,6);
	}else{
	    return $coin;
	}
}

function chkNum($str){
	$str = trim($str);
	if(empty($str)) return false;
    if(!is_numeric($str)) return false;
	if(floatval($str) <= 0) return false;
	return true;
}

function chkStr($str){
	$str = trim($str);
    if(empty($str)) return false;
	return true;
}

function createOrderNo() {
    $year_code = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
    return $year_code[intval(date('Y')) - 2010] .
    strtoupper(dechex(date('m'))) . date('d') .
    substr(time(), -5) . substr(microtime(), 2, 5) . strtoupper(sprintf('d', rand(0, 99)));
}

function getBank($bankFlag){
    switch($bankFlag){
        case 'ZGYH':
            return '中国银行';
        case 'ZGNYYH':
            return '中国农业银行';
        case 'ZGGSYH':
            return '中国工商银行';
        case 'ZGJSYH':
            return '中国建设银行';
        default: return '获取银行名称失败';

    }
}

function  limitBank($zgyh,$zgnyyh,$zggsyh,$zgjsyh,$str){
    $flag=false;
         if($zgyh&&$str=='ZGYH'){
             $flag=true;
         }
            if($zgnyyh&&$str=='ZGNYYH'){
                $flag=true;
            }
            if($zggsyh&&$str=='ZGGSYH'){
                $flag=true;
            }
            if($zgjsyh&&$str=='ZGJSYH'){
                $flag=true;
            }
    return $flag;
}
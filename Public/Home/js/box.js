var box = {

  loadModal: function(url){
    $.get(url, function(rs){
      responseHandler.run(rs, box.openModal)
    })
  },

  openModal: function(content){
    if($("#modal").length > 0) {
      $("#modal").html(content).modal();
    } else {
      $(content).modal()
      .on("hidden", function remove() {
        $(this).remove()
      })
    }
  },

  removeModal: function(){
    $("#modal").modal('hide');
  },

  ajaxSubmit: function(e, url, data, callback){
    e.preventDefault();
    $.post(url, data)
      .success(function(response, status, xhr){
        responseHandler.run(response, callback);
      })
      .error(function(xhr, textStatus, errorThrown){
        //bootbox.alert(xhr.responseText);
        alert(xhr.responseText);
      });
  },

  updateGrid: function(grid){
    $.fn.yiiGridView.update(grid);
  },

  confirm: function(e, msg, url, data, callback){
    e.preventDefault();
    /*
    bootbox.confirm(msg, function(result) {
      if(result){
        box.ajaxSubmit(e, url, data, callback);
      }
    });*/
    if(confirm(msg)){
      box.ajaxSubmit(e, url, data, callback);
    }
  },

  getCursorPosition: function(e){
    var p = $(e).getCursorPosition();
    $(e).attr('offset', p);
  }

};

$(document).ready(function() {
  jQuery("body").ajaxComplete(
    function(event, request, options) {
      if (request.responseText == loginRequiredAjaxResponse) {
        box.loadModal(config.url + '/site/ajaxLogin');
      }
    }
  );
});
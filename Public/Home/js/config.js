var config = {
  url: '/',
  locale: 'en_us',

  init: function(o){
    for(x in o){
      this[x] = o[x];
    }
  },

  getString: function(k){
    return appLocale[this.locale][k];
  }
}

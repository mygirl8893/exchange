var highchart = {
  rangeInput : false,
  lang : 'en_us',
  currentMonth : '',
  requestUrl : '',
  targetCurrency : 'BTC'
};

highchart.init = function(o){
  for(x in o){
    this[x] = o[x];
  }
  this.getChartData(this.currentMonth);
}

highchart.getChartData = function(startMonth){
  $.ajax({
    type: 'POST',
    url: this.requestUrl,
    data: {startMonth : startMonth},
    dataType: 'json',
    beforeSend: function(){
      $('#highchart_range_selector a.dropdown-toggle').attr("disabled", "disabled");
    },
    success: function(r){
      responseHandler.run(r, function(handler) {
        highchart.showChart(r);
        $('#highchart_range_selector a.dropdown-toggle').html(startMonth + '<span class="caret"></span>');
        $('#highchart_range_selector a.dropdown-toggle').removeAttr('disabled');
      });
    }
  });
}

highchart.showChart = function(data){
  var highstock = {
    source : {name : "RMB"},
    target : {name : highchart.targetCurrency}
  };
  var datas = data,
    rates = [],
    vols = [];
  for (i = 0; i < datas.length; i++) {
    rates.push([datas[i][0], datas[i][1], datas[i][2], datas[i][3], datas[i][4]]);
    vols.push([datas[i][0], datas[i][5]])
  }

  Highcharts.setOptions({
    lang: {
		months: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
		weekdays: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],		
		shortMonths:['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月']
    },
    credits: {
    	enabled:!0,text:"btc.aliyun1.com/",
		href:"http://btc.aliyun.comd/"
    }
  });

  data.main_chart = new Highcharts.StockChart({
    chart: {
      renderTo: 'graph',
      width: $('#graph').width()
    },
    xAxis: {
      type: 'datetime'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      candlestick: {
        color: '#0ab92b',
        upColor: '#f01717',
        tooltip: {
          pointFormat : '<span style="color:{series.color};font-weight:bold">{series.name}</span><br/>' +
            highchart.getLang('Open')+': {point.open}<br/>' +
            highchart.getLang('High')+': {point.high}<br/>' +
            highchart.getLang('Low')+': {point.low}<br/>' +
            highchart.getLang('Close')+': {point.close}<br/>'
        }
      }
    },
    tooltip: {
		xDateFormat:'%Y-%m-%d %H:%M %A',//日期
		backgroundColor: '#FFF',
		borderColor:'#666',
		borderRadius:'0',
		style:{color:'#333',padding: '10',lineHeight: '20px'},
		crosshairs:{width:1,  color:"#999",  dashStyle:"" },//十字线
		shared: true //全部据点数据  
		//positioner: function () {return {x:80,y:30}},				
		
    },
    scrollbar: {
      enabled: false
    },
    navigator: {
      enabled: false
    },
    rangeSelector: {
      buttons: [
        {type: 'minute', count: 15, text: highchart.getLang('15m')},
        {type: 'minute', count: 30, text: highchart.getLang('30m')},
        {type: 'minute', count: 60, text: highchart.getLang('1h')},
        {type: 'minute', count: 120,text: highchart.getLang('2h')},
        {type: 'minute', count: 360,text: highchart.getLang('6h')},
        {type: 'minute', count: 720,text: highchart.getLang('12h')},
        {type: 'day',    count: 1,  text: highchart.getLang('1d')},
        {type: 'all', text: highchart.getLang('all')}
      ],
      selected: 5,
      inputEnabled: this.rangeInput ? true : false
    },
    yAxis: [{
      labels: {
        style: {
          color: '#CC3300'
        }
      },
      title: {
        text: highchart.getLang('price') + ' ['+ highchart.getLang(highstock.source.name) +']',
        style: {
          color: '#CC3300'
        }
      }
    }, {
      labels: {
        style: {
          color: '#4572A7'
        }
      },
      title: {
        text: highchart.getLang('volume') + ' [' + highchart.getLang(highstock.target.name) + ']',
        style: {
          color: '#4572A7'
        }
      },
      opposite: true
    }],
    series: [{
      animation: false,
      name: highchart.getLang('volume') + ' ['+ highchart.getLang(highstock.target.name) +']',
      type: 'column',
      color: '#4572A7',
      marker: {
        enabled: false
      },
      yAxis: 1,
      data: vols
    }, {
      animation: true,
      name: highchart.getLang('price') + ' ['+ highchart.getLang(highstock.source.name) +']',
      type: 'candlestick',
      marker: {
        enabled: false
      },
      data: rates
    }]
  });
}

highchart.getLang = function(s){
  return highchartLocale[this.lang][s];
}
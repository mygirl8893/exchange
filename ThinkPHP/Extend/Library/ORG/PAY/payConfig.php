<?php
$PayConfig=array(
    /* 其他设置 */
    'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => '127.0.0.1', // 服务器地址
    'DB_NAME' => 'pay', // 数据库名
    'DB_USER' => 'root', // 用户名
    'DB_PWD' => 'lb123', // 密码
    'DB_PORT' => '3306', // 端口
    'DB_PREFIX' => 'think_', // 数据库表前缀

    /* 支付设置 */
    'pay' => array(
        //财付通配置
        'tenpay' => array(
            // 加密key，开通财付通账户后给予
            'key' => 'abc96f1cf04e76d4465fdaf01cb36e4d',
            // 合作者ID，财付通有该配置，开通财付通账户后给予
            'partner' => '1206996401'
        ),
        //支付宝配置
        'alipay' => array(
            // 收款账号邮箱
            'email' => 'yjfmlsx@163.com',
            // 加密key，开通支付宝账户后给予
            'key' => 'tn026z5mrt59c160e301xhwc9mtfq9q6',
            // 合作者ID，支付宝有该配置，开通易宝账户后给予
            'partner' => '2088002043626720'
        ),

        //贝宝配置
        'yeepay' => array(
            'key' => '7MR988uo494qXsHw823D7Ccl54c27Mu7R38jw1veiPSomqfK0Tyt10qIc8K',
            'partner' => '10012042942'
        ),

        //银联配置
        'unionpay' => array(
            'v_mid' => '37283',
            'key' => 'yjfmlsxym654321'
        )
    )
);

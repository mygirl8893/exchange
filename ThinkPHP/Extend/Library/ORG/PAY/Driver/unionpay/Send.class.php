<?

class Send
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var string
     */
    public $sendUrl = 'https://pay3.chinabank.com.cn/PayGate';
    /**
     * @var string
     */
    public $backUrl;

    /**
     * @param $configA
     */
    public function __construct($configA)
    {

        $configA['v_url'] = $this->backUrl;
        $configA['v_moneytype'] = "CNY";

        //下面是选增字段，配置在Config里面就增加了
        $configB = array(
            'remark1' => '', //备注字段1
            'remark2' => '', //备注字段2
            'v_rcvname' => '', // 收货人
            'v_rcvaddr' => '', // 收货地址
            'v_rcvtel' => '', // 收货人电话
            'v_rcvpost' => '', // 收货人邮编
            'v_rcvemail' => '', // 收货人邮件
            'v_rcvmobile' => '', // 收货人手机号
            'v_ordername' => '', // 订货人姓名
            'v_orderaddr' => '', // 订货人地址
            'v_ordertel' => '', // 订货人电话
            'v_orderpost' => '', // 订货人邮编
            'v_orderemail' => '', // 订货人邮件
            'v_ordermobile' => '', // 订货人手机号
        );
        $this->config = array_merge($configA, $configB);
        $this->backUrl=$this->config['backUrl'];
        $this->config['v_md5info'] = $this->putMd5();
        $this->ShowHtml();
    }


    /**
     * @return string
     */
    private
    function putMd5()
    {
        $text = $this->config['v_amount'] . $this->config['v_moneytype']
            . $this->config['v_oid'] . $this->config['v_mid'] . $this->config['v_url']
            . $this->config['key']; //md5加密拼凑串,注意顺序不能变
        return strtoupper(md5($text)); //md5函数加密并转化成大写字母
    }

    /**
     *
     */
    private function ShowHtml()
    {
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>
<body onLoad="javascript:document.E_FORM.submit()">';
        echo '<form method = "post" name ="E_FORM" action = "' . $this->sendUrl . '" > ';
        foreach ($this->config as $key => $value) {
            echo '<input type = "hidden" name = "' . $key . '" value = "' . $value . '" > ';
        }
        echo '</form ></body ></html > ';
    }
}

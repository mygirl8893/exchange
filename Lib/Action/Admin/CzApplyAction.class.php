<?php
// 本类由系统自动生成，仅供测试用途
class CzApplyAction extends CommonAction {
	private $CzApply;
	private $ChongZhi;
	private $User;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->CzApply=D('CzApply');
		$this->ChongZhi=D('ChongZhi');
		$this->User=D('User');
	}

    public function index(){
        if(intval($_GET['userid']) > 0) {
			$where = ' and t_cz_apply.userid='.$_GET['userid'];
			$url = '/userid/'.$_GET['userid'];
		}

		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->CzApply->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

		$status = $_GET['status'] ? $_GET['status'] : 0;
		$urls = '/status/'.$_GET['status'].$url;
        
		$rs=$this->CzApply->join('t_type on t_type.id=t_cz_apply.typeid')->join('t_user on t_user.id=t_cz_apply.userid')->field('t_cz_apply.*,t_type.nickname,t_user.username')->where('t_cz_apply.status='.$status.$where)->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();
		$this->assign('list',$rs);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('status',$status);
		$this->assign('urls',$urls);
		$this->assign('url',$url);

		$this->display('./Tpl/Admin/CzApply.html');
    }

	public function set(){
		if(!chkNum($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$id = intval($_GET['id']);
		$data['id'] = $id;
		$data['status'] = 1;

        $ca = $this->CzApply->where('id='.$id)->find();
        $cz = $this->ChongZhi->where('userid='.$ca['userid'].' and typeid='.$ca['typeid'])->find();

		$mo = new Model();
		$mo->startTrans();

        $rs = $mo->table('t_cz_apply')->save($data);
		
		$d['id'] = $cz['id'];
		$d['goldnum'] = coin($cz['goldnum'] + $ca['goldnum']);

        $rs1 = $mo->table('t_chong_zhi')->save($d);

		//推荐奖励
		$count = $this->CzApply->where('userid='.$ca['userid'])->count();
		$user = $this->User->where('id='.$ca['userid'])->find();
		if($count <= 0 && chkStr($user['invit'])){
		    $user1 = $this->User->where('inviturl=\''.$user['invit'].'\'')->find();
			if(chkStr($user1['invit'])){
				$cz1 = $this->ChongZhi->where('userid='.$user1['id'].' and typeid='.$ca['typeid'])->find();

				$d1['id'] = $cz1['id'];
				$d1['goldnum'] = coin($cz1['goldnum'] + $ca['goldnum'] * $this->site['award1'] /100);
				if(chkNum($d1['goldnum'])) $award[] = $mo->table('t_chong_zhi')->save($d1);

				$user2 = $this->User->where('inviturl=\''.$user1['invit'].'\'')->find();
				if(chkStr($user2['invit'])){
					$cz2 = $this->ChongZhi->where('userid='.$user2['id'].' and typeid='.$ca['typeid'])->find();

					$d2['id'] = $cz2['id'];
					$d2['goldnum'] = coin($cz2['goldnum'] + $ca['goldnum'] * $this->site['award2'] /100);
					$award[] = $mo->table('t_chong_zhi')->save($d2);

					$user3 = $this->User->where('inviturl=\''.$user2['invit'].'\'')->find();
					if(chkStr($user3['invit'])){
						$cz3 = $this->ChongZhi->where('userid='.$user3['id'].' and typeid='.$ca['typeid'])->find();

						$d3['id'] = $cz3['id'];
						$d3['goldnum'] = coin($cz3['goldnum'] + $ca['goldnum'] * $this->site['award3'] /100);
						$award[] = $mo->table('t_chong_zhi')->save($d3);
					}
				}
			}
		}

        $f = true;
        foreach($award as $val){
		    if(!$val) $f = false;
		}

		if($rs && $rs1 && $f){
			$mo->commit();
			$this->assign('jumpUrl','?s=Admin/CzApply');
			$this->success('操作成功！');
		}else{
			$mo->rollback();
		    $this->error('操作失败！');
		}
    }
}
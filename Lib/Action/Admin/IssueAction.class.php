<?php
// 本类由系统自动生成，仅供测试用途
class IssueAction extends CommonAction {
	private $Issue;

	public function __construct(){
		parent::__construct();

        if($this->role !== 0 && $this->role !== 1){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Issue=D('Issue');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->Issue->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
		
		$rs = $this->Issue->limit(($page-1)*$per_num.','.$per_num)->select();

		foreach($rs as $k => $v){
		    $rs[$k]['addtime'] = date('Y-m-d',strtotime($v['addtime']));
			$rs[$k]['status'] = $v['status']>0 ? '已结束' : '进行中';
		}

		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/Issue.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('修改失败！');
		}
		$value=$this->Issue->where('id='.$_GET['id'])->find();
		$value['addtime'] = chkStr($value['addtime'])?date('Y-m-d',strtotime($value['addtime'])):date('Y-m-d',time());
		$this->assign($value);
		$this->display('./Tpl/Admin/Issue.html');
    }

	public function add(){
		$this->assign('addtime',date('Y-m-d',time()));
		$this->display('./Tpl/Admin/Issue.html');
    }

	public function update(){

        if(trim($_POST['num'])=='' || !chkNum($_POST['price']) || !chkNum($_POST['limit']) || !chkStr($_POST['addtime'])){
		   $this->error('信息不完整！');
		   exit;
		}

		$data['num']=coin($_POST['num']);
		$data['nownum']=coin($_POST['num']-$_POST['issuenum']);
		$data['issuenum']=coin($_POST['issuenum']);
		$data['price']=coin($_POST['price']);
		$data['limit']=coin($_POST['limit']);
		$data['addtime']=trim($_POST['addtime']);
		$data['info']=stripslashes($_POST['info']);

		if(chkNum($_POST['id'])){
			$data['id']=$_POST['id'];

			if($data['issuenum']<$data['num']) 
				$data['status'] = 0;
			else
				$data['status'] = 1;

			if($this->Issue->save($data)!==false){
				$this->assign('jumpUrl','?s=Admin/Issue');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}else{
			if($this->Issue->add($data)){
				$this->assign('jumpUrl','?s=Admin/Issue');
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}
    }

	public function del(){
		if(!chkNum($_GET['id'])){
		   $this->error('删除失败！');
		   exit(0);
		}

	    if($this->Issue->where('id='.$_GET['id'])->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->Issue->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
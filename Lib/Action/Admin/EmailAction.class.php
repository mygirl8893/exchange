<?php
// 本类由系统自动生成，仅供测试用途
class EmailAction extends CommonAction {
	private $Email;

	public function __construct(){
		parent::__construct();

        if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Email=D('Email');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->Email->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
        
		$rs=$this->Email->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();

		foreach($rs as $key => $val){
		    if($val['id']==1){
			    $rs[$key]['class']='用户注册';
			}elseif($val['id']==2){
			    $rs[$key]['class']='忘记密码';
			}
		}
		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/Email.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$value=$this->Email->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/Email.html');
    }

	public function update(){
		if(empty($_POST['id']) || empty($_POST['title']) || empty($_POST['content'])){
		   $this->error('信息不完整！');
		}

		$data['title']=$_POST['title'];
		$data['content']=stripslashes($_POST['content']);
		$data['id']=$_POST['id'];

		if($this->Email->save($data)){
			$this->assign('jumpUrl','?s=Admin/Email');
			$this->success('修改成功！');
		}else{
			$this->error('修改失败！');
		}
    }
}
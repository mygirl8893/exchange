<?php
// 本类由系统自动生成，仅供测试用途
class TiXianAction extends CommonAction {
	private $TiXian;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->TiXian=D('TiXian');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TiXian->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

		$status = $_GET['status'] ? $_GET['status'] : 0;
		$urls = '/status/'.$_GET['status'];
        
		$rs=$this->TiXian->join('t_type on t_type.id=t_ti_xian.typeid')->join('t_user on t_user.id=t_ti_xian.userid')->field('t_ti_xian.*,t_type.name,t_user.username')->where('status='.$status)->limit(($page-1)*$per_num.','.$per_num)->select();
		$this->assign('list',$rs);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('status',$status);
		$this->assign('urls',$urls);
		$this->display('./Tpl/Admin/TiXian.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$data['id']=$_GET['id'];
		$data['status'] = 1;
		
		if($this->TiXian->save($data)){
			$this->assign('jumpUrl','?s=Admin/TiXian');
			$this->success('操作成功！');
		}else{
		    $this->error('操作失败！');
		}
    }

	public function del(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
	    $id=$_GET['id'];
	    if($this->TiXian->where('id='.$id)->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->TiXian->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
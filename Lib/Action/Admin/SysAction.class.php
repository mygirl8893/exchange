<?php
class SysAction extends CommonAction {
	private $Sys;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Sys=D('Sys');
	}
    public function index(){
		$value=$this->Sys->where('id=1')->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/sys.html');
    }

	public function update(){
		if(!chkNum($_POST['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$data['id']=$_POST['id'];
		$data['title']=$_POST['title'];
		$data['keyword']=$_POST['keyword'];
        $data['description']=$_POST['description'];
		$data['copyright']=$_POST['copyright'];
		$data['tongji']=$_POST['tongji'];
		$data['smtp']=$_POST['smtp'];
		$data['email']=$_POST['email'];
		$data['pwd']=$_POST['pwd'];
		$data['auth']=$_POST['auth'];
		$data['txfee']=$_POST['txfee'];
		$data['award1']=$_POST['award1'];
		$data['award2']=$_POST['award2'];
		$data['award3']=$_POST['award3'];
        $data['saleflag']=$_POST['saleflag'];
        $data['buyflag']=$_POST['buyflag'];
        $data['huigouflag']=$_POST['huigouflag'];

        $data['chongzhiu']=$_POST['chongzhiu'];
        $data['chongzhid']=$_POST['chongzhid'];

        $data['tixianu']=$_POST['tixianu'];
        $data['tixiand']=$_POST['tixiand'];

        $data['huigouu']=$_POST['huigouu'];
        $data['huigoud']=$_POST['huigoud'];

        $data['txtype']=$_POST['txtype'];

        $data['bank_zgyh']=$_POST['bank_zgyh'];
        $data['bank_zgnyyh']=$_POST['bank_zgnyyh'];
        $data['bank_zggsyh']=$_POST['bank_zggsyh'];
        $data['bank_zgjsyh']=$_POST['bank_zgjsyh'];

        $data['pay_zhifubao']=$_POST['pay_zhifubao'];
        $data['pay_caifutong']=$_POST['pay_caifutong'];
        $data['pay_yibao']=$_POST['pay_yibao'];
        $data['pay_wangyin']=$_POST['pay_wangyin'];





        $data['qq']=$_POST['qq'];
		$data['qun']=$_POST['qun'];
		$data['extcoin']=$_POST['extcoin'];
		$data['exttime']=$_POST['exttime'];
		$data['extper']=$_POST['extper'];
		$data['wait']=$_POST['wait'];
		$data['alipay']=$_POST['alipay'];
		$data['alipaylink']=$_POST['alipaylink'];
		$data['ikey']=$_POST['ikey'];
		$data['skey']=$_POST['skey'];
		
        if($_FILES["logo"]["tmp_name"]){
			move_uploaded_file($_FILES["logo"]["tmp_name"],"upload/" . $_FILES["logo"]["name"]);
			$data['logo'] = "upload/" . $_FILES["logo"]["name"];
		}else{
		    $data['logo'] = $_POST['logoimg'];
		}
		
		if($this->Sys->save($data)){
		    $this->success('修改成功！');
		}else{
		    $this->error('修改失败！');
		}
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class AdminAction extends CommonAction {
	private $Admin;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Admin=D('Admin');
	}
    public function index(){
		$list=$this->Admin->select();

        $arr = array('超级管理员','文章管理员','币种管理员');
        foreach($list as $k => $v){
		    $list[$k]['catename'] = $arr[$v['cate']];
		}

		$this->assign('list',$list);
		$this->assign('module','list');
		$this->display('./Tpl/Admin/admin_set.html');
    }

	public function set(){
		if(!chkNum($_GET['id'])){
		    $this->error('修改失败！');
			exit;
		}
		$value=$this->Admin->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/admin_set.html');
    }

	public function add(){
	    $this->display('./Tpl/Admin/admin_set.html');
	}

	public function update(){
		if(!chkStr($_POST['username']) || !chkStr($_POST['password'])){
		   $this->error('信息不完整');
		}

        $data['cate']=$_POST['cate'];
		$data['username']=$_POST['username'];
		$data['password']=md5($_POST['password']);

        if(chkNum($_POST['id'])){
			$data['id'] = $_POST['id'];
			
			if($this->Admin->save($data)){
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}else{
			if($this->Admin->add($data)){
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}
    }
}
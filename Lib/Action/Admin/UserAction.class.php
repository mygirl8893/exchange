<?php
// 本类由系统自动生成，仅供测试用途
class UserAction extends CommonAction {
	private $User;
	private $Type;
	private $ChongZhi;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->User=D('User');
		$this->Type=D('Type');
		$this->ChongZhi=D('ChongZhi');
	}
    public function index(){
         
		$url_param = '';
		$where = '';
        if(empty($_GET['key_email'])){
		    $key_email = '';
		}else{
			$key_email = $_GET['key_email'];
		    $where .= ' and email = \''.$_GET['key_email'].'\' ';
			$url_param .= '/key_email/'.$_GET['key_email'];
		}
		if(empty($_GET['key_username'])){
		    $key_username = '';
		}else{
			$key_username = $_GET['key_username'];
		    $where .= ' and username = \''.$_GET['key_username'].'\' ';
			$url_param .= '/key_username/'.$_GET['key_username'];
		}

		$perpage = 10;
        $count = $this->User->where('1=1'.$where)->count('id');
		$pagenum = ceil($count/$perpage);
        $page = intval($_GET['page']);

		if($page > $pagenum) $page = $pagenum;
		if($page < 1) $page = 1; 

		$list=$this->User->where('1=1'.$where)->order('id desc')->limit(($page - 1)*$perpage,10)->select();
        
		foreach($list as $key => $val){
			$list[$key]['types'] = $this->Type->join('t_chong_zhi cz on (cz.typeid = t_type.id and cz.userid='.$val['id'].')')->field('cz.goldnum,cz.gdgold,t_type.name,cz.url')->select();
		}

		$this->assign('list',$list);
		$this->assign('endpage',$pagenum);
		$this->assign('page',$page);
		$this->assign('count',$count);
		$this->assign('pagenum',$pagenum);
		$this->assign('module','list');
		$this->assign('url_param',$url_param);
		$this->assign('key_email',$key_email);
		$this->assign('key_username',$key_username);

		$this->display('./Tpl/Admin/User.html');
    }

	public function add(){
		$this->display('./Tpl/Admin/User.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$value=$this->User->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/User.html');
    }

	public function chongzhi(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$types = $this->Type->select();
		$this->assign('types',$types);
		$this->assign('id',$_GET['id']);
		$this->assign('module','chongzhi');

		$value=$this->User->where('id='.$_GET['id'])->find();
		$this->assign($value);

		$this->display('./Tpl/Admin/User.html');
    }

	public function chongzhido(){
		if(empty($_POST['goldnum'])){
		    $this->error('请输入充值数量！');
		}
	    if(!empty($_POST['userid']) && !empty($_POST['typeid'])){
			
			$isexist = $this->ChongZhi->where('userid='.$_POST['userid'].' and typeid='.$_POST['typeid'])->find();

			$mo = new Model();
			$mo->startTrans();

			if($isexist){
				$data['goldnum']=$_POST['goldnum'] + $isexist['goldnum'];

                $rs1 = $mo->table('t_chong_zhi')->where('userid='.$_POST['userid'].' and typeid='.$_POST['typeid'])->save($data);

                $data['userid']=$_POST['userid'];
		        $data['typeid']=$_POST['typeid'];
				$data['goldnum']=$_POST['goldnum'];
				$data['addtime']=date('Y-m-d H:i:s',time());
                $rs2 = $mo->table('t_chong_zhi_log')->add($data);

				if($rs1 && $rs2){
					$mo->commit();
					$this->assign('jumpUrl','?s=Admin/User');
					$this->success('充值成功！');
				}else{
					$mo->rollback();
				    $this->error('充值失败！');
				}
			}else{
				$data['goldnum']=$_POST['goldnum'];
				$data['userid']=$_POST['userid'];
		        $data['typeid']=$_POST['typeid'];
				$data['addtime']=date('Y-m-d H:i:s',time());

				$rs1 = $mo->table('t_chong_zhi')->add($data);
				$rs2 = $mo->table('t_chong_zhi_log')->add($data);

				if($rs1 && $rs2){
					$mo->commit();
				    $this->assign('jumpUrl','?s=Admin/User');
			        $this->success('充值成功！');
				}else{
					$mo->rollback();
				    $this->error('充值失败！');
				}
			}
		}else{
		    $this->error('错误！');
		}
	}

	public function update(){

        if(empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password'])){
		    $this->error('输入的信息不完整！');
		}

		$data['username']=$_POST['username'];
		$data['email']=$_POST['email'];
		//$data['isclose']=$_POST['isclose'];
		$data['password']=md5($_POST['password']);
		$data['pwdshow']=$_POST['password'];

		if(empty($_POST['id'])){
			$data['addtime']=date('Y-m-d H:i:s',time());
		    if($this->User->add($data)){
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}else{
			$data['id']=$_POST['id'];
		    if($this->User->save($data)){
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}
    }

	public function del(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}

	    if($this->User->where('id='.$_GET['id'])->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class OwnAction extends CommonAction {
	private $Own;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Own=D('Own');
	}
    public function index(){
		$list=$this->Own->select();

		$this->assign('list',$list);
		$this->assign('module','list');
		$this->display('./Tpl/Admin/own.html');
    }

	public function set(){
		if(!chkNum($_GET['id'])){
		    $this->error('修改失败！');
			exit;
		}
		$value=$this->Own->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/own.html');
    }

	public function update(){
		if(!chkStr($_POST['name']) || !chkStr($_POST['nickname']) || !chkStr($_POST['num'])){
		   $this->error('信息不完整');
		}

        $data['name']=$_POST['name'];
		$data['nickname']=$_POST['nickname'];
		$data['num']=$_POST['num'];
		$data['id'] = $_POST['id'];
		
		if($this->Own->save($data)!==false){
			$this->assign('jumpUrl','?s=Admin/Own');
			$this->success('修改成功！');
		}else{
			$this->error('修改失败！');
		}
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class BuyAction extends CommonAction {
	private $Buy;
	private $Issue;
	private $User;

	public function __construct(){
		parent::__construct();

        if($this->role !== 0 && $this->role !== 1){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Buy=D('Buy');
		$this->Issue=D('Issue');
		$this->User=D('User');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->Buy->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
		
		$rs = $this->Buy->limit(($page-1)*$per_num.','.$per_num)->order('id desc')->select();

		foreach($rs as $k => $v){
			$rs[$k]['status'] = $v['status']>0 ? '已回购' : '未回购';
			$user = $this->User->where('id='.$v['userid'])->find();
			$rs[$k]['username'] = $user['username'];
		}

		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/Buy.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('修改失败！');
		}
		$value=$this->Buy->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/Buy.html');
    }

	public function add(){
        $issue = $this->Issue->where('status=0')->find();
		$this->assign('price',$issue['price']);
        $this->assign('limit',$issue['limit']);
		$this->assign('addtime',date('Y-m-d',time()));
		$this->display('./Tpl/Admin/Buy.html');
    }

	public function update(){

        if(trim($_POST['num'])=='' || !chkNum($_POST['price']) || !chkNum($_POST['limit'])){
		   $this->error('信息不完整！');
		   exit;
		}

        $data['issueid']=$_POST['issueid'];
		$data['num']=coin($_POST['num']);
		$data['nownum']=coin($_POST['num']);
		$data['price']=coin($_POST['price']);
		$data['limit']=coin($_POST['limit']);
		
		if(chkNum($_POST['id'])){
			$data['id']=$_POST['id'];

			if($data['num']<=0)
				$data['status'] = 1;
			else
				$data['status'] = 0;

			if($this->Buy->save($data)!==false){
				$this->assign('jumpUrl','?s=Admin/Buy');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}else{
			$data['addtime']=date('Y-m-d',time());
			if($this->Buy->add($data)){
				$this->assign('jumpUrl','?s=Admin/Buy');
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}
    }

	public function del(){
		if(!chkNum($_GET['id'])){
		   $this->error('删除失败！');
		   exit(0);
		}

	    if($this->Buy->where('id='.$_GET['id'])->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->Buy->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class AdAction extends CommonAction {
	private $Ad;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Ad=D('Ad');
	}

    public function index(){
		
		$rs=$this->Ad->select();

		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->display('./Tpl/Admin/Ad.html');
    }

	public function set(){
		if(!chkNum($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$value=$this->Ad->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/Ad.html');
    }

	public function add(){
		$this->display('./Tpl/Admin/Ad.html');
    }

	public function update(){

        if(!chkStr($_POST['url']) || !chkStr($_POST['sort']) || !chkStr($_FILES["img"]["name"])){
		   $this->error('信息不完整！');
		   exit;
		}

		$data['url']=$_POST['url'];
		$data['sort']=$_POST['sort'];
		move_uploaded_file($_FILES["img"]["tmp_name"],"upload/" . $_FILES["img"]["name"]);
		$data['img'] = "upload/" . $_FILES["img"]["name"];

		if(chkNum($_POST['id'])){
			$data['id']=$_POST['id'];
			if($this->Ad->save($data)){
				$this->assign('jumpUrl','?s=Admin/Ad');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}else{
			if($this->Ad->add($data)){
				$this->assign('jumpUrl','?s=Admin/Ad');
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}
    }

	public function del(){
		if(!chkNum($_GET['id'])){
		   $this->error('删除失败！');
		   exit(0);
		}

	    if($this->Ad->where('id='.$_GET['id'])->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }
}
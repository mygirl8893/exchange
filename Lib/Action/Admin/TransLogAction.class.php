<?php
// 本类由系统自动生成，仅供测试用途
class TransLogAction extends CommonAction {
	private $TransLog;
	private $Type;
	private $User;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->TransLog=D('TransLog');
		$this->TypeBox=D('TypeBox');
		$this->User=D('User');
	}

    public function index(){
		if(!empty($_GET['userid'])){
		    $where = 'and userid='.$_GET['userid'];
			$urls = '/userid/'.$_GET['userid'];
		}


		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TransLog->where('1=1 '.$where)->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

        
		$rs=$this->TransLog->where('1=1 '.$where)->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();
		foreach($rs as $key => $val){
		    $username = $this->User->where('id='.$val['userid'])->field('username')->find();
			$name = $this->TypeBox->join('t_type t on t.id=t_type_box.name1')->join('t_type t1 on t1.id=t_type_box.name2')->where('t_type_box.id='.$val['typeboxid'])->field('t.nickname as names1,t1.nickname as names2')->find();
			$rs[$key]['username']=$username['username'];
			$rs[$key]['name1']=$name['names1'];
			$rs[$key]['name2']=$name['names2'];
		}
		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('urls',$urls);
		$this->display('./Tpl/Admin/TransLog.html');
    }

	public function del(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
	    $id=$_GET['id'];
	    if($this->TransLog->where('id='.$id)->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->TransLog->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
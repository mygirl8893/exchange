<?php
// 本类由系统自动生成，仅供测试用途
class CommonAction extends Action {
	protected $site;
	protected $role;

	public function __construct(){

		if (!isset($_SESSION['USER_ADMIN_ID']) || empty($_SESSION['USER_ADMIN_ID'])) {
			$this->assign('jumpUrl','/?s=Admin/Login');
			$this->error('对不起,您还没有登录,请先登录！');
			exit;
		}

        $Sys=D('Sys');
		$this->site = $Sys->where('id=1')->find();

		$Admin = D('Admin');
		$rs = $Admin->where('id='.$_SESSION['USER_ADMIN_ID'])->find();
		$this->role = intval($rs['cate']);

		if($rs['cate']==0){
		    $menu[0] = array('name'=>'币种管理','url'=>'?s=Admin/Type');
			$menu[1] = array('name'=>'市场管理','url'=>'?s=Admin/TypeBox');
			$menu[2] = array('name'=>'会员管理','url'=>'?s=Admin/User');
			$menu[3] = array('name'=>'充值地址','url'=>'?s=Admin/ChongZhiUrl');
			$menu[4] = array('name'=>'充值申请','url'=>'?s=Admin/CzApply');
			$menu[5] = array('name'=>'提现申请','url'=>'?s=Admin/TiXian');
			$menu[6] = array('name'=>'交易记录','url'=>'?s=Admin/TransLog');
			$menu[7] = array('name'=>'邮件管理','url'=>'?s=Admin/Email');
			$menu[8] = array('name'=>'文章管理','url'=>'?s=Admin/Art');
			$menu[9] = array('name'=>'网站设置','url'=>'?s=Admin/Sys');
			$menu[10] = array('name'=>'管理员','url'=>'?s=Admin/Admin');
			$menu[11] = array('name'=>'广告图管理','url'=>'?s=Admin/Ad');
			$menu[] = array('name'=>'发行认购','url'=>'?s=Admin/Issue');
			$menu[] = array('name'=>'财富币认购','url'=>'?s=Admin/Buy');
			$menu[] = array('name'=>'财富币回购','url'=>'?s=Admin/Hg');
		}elseif($rs['cate']==1){
			$menu[0] = array('name'=>'文章管理','url'=>'?s=Admin/Art');
		}elseif($rs['cate']==2){
			$menu[0] = array('name'=>'币种管理','url'=>'?s=Admin/Type');
			$menu[1] = array('name'=>'市场管理','url'=>'?s=Admin/TypeBox');
		}

		$this->assign('menu',$menu);
	}
}
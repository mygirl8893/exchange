<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends CommonAction {
	public function __construct(){
		parent::__construct();
		$this->assign('loginname',$_SESSION['USER_ADMIN_KEY']);
	}

    public function index(){
	    $this->display('./Tpl/Admin/index.html');
    }

	public function top(){
		$mo = new Model();
		$arr = $mo->table('t_ti_xian_log')->field('SUM(shouxu) as feesum')->find();
		$this->assign('feesum',$arr['feesum']);
	    $this->display('./Tpl/Admin/top.html');
    }

	public function left(){
	    $this->display('./Tpl/Admin/left.html');
    }

	public function welcome(){

		$mo = new Model();

		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $mo->table('t_ti_xian_log tl')->group('userid')->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
		
		$tixian_log = $mo->table('t_ti_xian_log tl')->join('t_user u on u.id=tl.userid')->field('SUM(tl.shouxu) as feesum,u.id,u.username')->limit(($page-1)*$per_num.','.$per_num)->select();

		$this->assign('tixian_log',$tixian_log);

	    $this->display('./Tpl/Admin/welcome.html');
    }
}
?>
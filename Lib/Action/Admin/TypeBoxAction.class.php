<?php
// 本类由系统自动生成，仅供测试用途
class TypeBoxAction extends CommonAction {
	private $TypeBox;
	private $Type;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0 && $this->role !== 2){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->TypeBox=D('TypeBox');
		$this->Type=D('Type');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TypeBox->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
        
		$rs=$this->TypeBox->order('sort asc')->limit(($page-1)*$per_num.','.$per_num)->select();
		
		foreach($rs as $key => $val){
		    $name1=$this->Type->where('id='.$val['name1'])->field('name')->find();
			$rs[$key]['name1str']=$name1['name'];
			$name2=$this->Type->where('id='.$val['name2'])->field('name')->find();
			$rs[$key]['name2str']=$name2['name'];
		}
		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/TypeBox.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$id=$_GET['id'];
		$value=$this->TypeBox->where('id='.$id)->find();
		$this->assign($value);

		$rs=$this->Type->order('sort asc')->select();
		$this->assign('bts',$rs);
		$this->display('./Tpl/Admin/TypeBox.html');
    }

	public function update(){
		if(empty($_POST['name1']) || empty($_POST['name2']) || ($_POST['name1'] == $_POST['name2'])){
		    $this->error('币种为空或者币种1和币种2相同！请重新选择');
		}
		$data['name1']=$_POST['name1'];
		$data['name2']=$_POST['name2'];
		$data['info']=$_POST['info'];
        $data['sort']=is_numeric($_POST['sort']) ? $_POST['sort'] : 0;

		if(empty($_POST['id'])){
		    if($this->TypeBox->add($data)){
				$this->assign('jumpUrl','?s=Admin/TypeBox');
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}else{
			$data['id']=$_POST['id'];
		    if($this->TypeBox->save($data)){
				$this->assign('jumpUrl','?s=Admin/TypeBox');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}
    }

	public function add(){
        $rs=$this->Type->order('sort asc')->select();
		$this->assign('bts',$rs);
		$this->display('./Tpl/Admin/TypeBox.html');
    }

	public function del(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
	    $id=$_GET['id'];
	    if($this->TypeBox->where('id='.$id)->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->TypeBox->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
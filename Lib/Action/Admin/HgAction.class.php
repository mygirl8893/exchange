<?php
// 本类由系统自动生成，仅供测试用途
class HgAction extends CommonAction {
	private $HgApply;
	private $ChongZhi;
	private $User;
	private $Type;

	public function __construct(){
		parent::__construct();

        if($this->role !== 0 && $this->role !== 1){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->HgApply=D('HgApply');
        $this->Buy=D('Buy');
		$this->ChongZhi=D('ChongZhi');
		$this->User=D('User');
		$this->Type=D('Type');
        $this->Issue=D('');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->HgApply->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
		
		$rs = $this->HgApply->limit(($page-1)*$per_num.','.$per_num)->order('id desc')->select();

		foreach($rs as $k => $v){
			$rs[$k]['status'] = $v['status']>0 ? '已回购' : '未回购';
			$user = $this->User->where('id='.$v['userid'])->find();
			$rs[$k]['username'] = $user['username'];
		}

		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/HgApply.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('修改失败！');
		}
		$value=$this->HgApply->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/HgApply.html');
    }

	public function add(){
        $issue = $this->Issue->where('status=0')->find();
		$this->assign('price',$issue['price']);
        $this->assign('limit',$issue['limit']);
		$this->assign('addtime',date('Y-m-d',time()));
		$this->display('./Tpl/Admin/HgApply.html');
    }

	public function update(){

//        if(trim($_POST['num'])=='' || !chkNum($_POST['price']) || !chkNum($_POST['limit'])){
//		   $this->error('信息不完整！');
//		   exit;
//		}

        $data['issueid']=$_POST['issueid'];
		$data['num']=coin($_POST['num']);
		$data['nownum']=coin($_POST['num']);
		$data['price']=coin($_POST['price']);
		$data['limit']=coin($_POST['limit']);
		
		if(chkNum($_POST['id'])){
			$data['id']=$_POST['id'];

			if($data['num']<=0)
				$data['status'] = 1;
			else
				$data['status'] = 0;

			if($this->HgApply->save($data)!==false){
				$this->assign('jumpUrl','?s=Admin/Hg');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}else{
			$data['addtime']=date('Y-m-d',time());
			if($this->HgApply->add($data)){
				$this->assign('jumpUrl','?s=Admin/Hg');
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}
    }

	public function del(){
		if(!chkNum($_GET['id'])){
		   $this->error('删除失败！');
		   exit(0);
		}

	    if($this->HgApply->where('id='.$_GET['id'])->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->HgApply->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }

	public function apply(){

        if(!chkNum($_GET['id'])){
		   $this->error('审核失败！');
		   exit;
		}

		$hg = $this->HgApply->where('id='.$_GET['id'])->find();

        $type = $this->Type->where('yuan=1')->find();
        $cz = $this->ChongZhi->where('userid='.$hg['userid'].' and typeid='.$type['id'])->find();

        $d['id'] = $cz['id'];
		$d['goldnum'] = coin($cz['goldnum']-$hg['num']);

		$m = new Model();
		$m->startTrans();

        $rs = $m->table('t_chong_zhi')->save($d);

		$d1['id'] = $hg['id'];
		$d1['status'] = 1;
		$rs1 = $m->table('t_hg_apply')->save($d1);



		if($rs && $rs1){
            $d2['status']=1;
            $this->Buy->where('buyid='.$hg['id'])->save($d2);
            // 提交事务
            $m->commit();
            $this->assign('jumpUrl','?s=Admin/Hg');
			$this->success('审核成功！');
		}else{
            // 事务回滚
            $m->rollback();
			$this->error('审核失败！');
		}
    }
}
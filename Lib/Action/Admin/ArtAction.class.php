<?php
// 本类由系统自动生成，仅供测试用途
class ArtAction extends CommonAction {
	private $Art;

	public function __construct(){
		parent::__construct();

        if($this->role !== 0 && $this->role !== 1){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Art=D('Art');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->Art->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
		
		$rs = $this->Art->limit(($page-1)*$per_num.','.$per_num)->select();

        $cate = array('serv'=>'客户服务','about'=>'关于我们','mserv'=>'服务条款','warn'=>'风险提示','help'=>'一般问题','news'=>'网站公告','reg'=>'注册协议','fact'=>'币工厂说明');
		foreach($rs as $key => $val){
		    $rs[$key]['catename'] = $cate[$val['cate']];
		}

		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/Art.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$value=$this->Art->where('id='.$_GET['id'])->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/Art.html');
    }

	public function add(){
		$this->display('./Tpl/Admin/Art.html');
    }

	public function update(){

        if(!chkStr($_POST['title']) || !chkStr($_POST['content'])){
		   $this->error('信息不完整！');
		   exit;
		}

		$data['title']=$_POST['title'];
		$data['content']=stripslashes($_POST['content']);
		$data['cate']=$_POST['cate'];
		$data['addtime']=date('Y-m-d H:i:s',time());

		if(chkNum($_POST['id'])){
			$data['id']=$_POST['id'];

			if($this->Art->save($data)){
				$this->assign('jumpUrl','?s=Admin/Art');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}else{
			if($this->Art->add($data)){
				$this->assign('jumpUrl','?s=Admin/Art');
				$this->success('添加成功！');
			}else{
				$this->error('添加失败！');
			}
		}
    }

	public function del(){
		if(!chkNum($_GET['id'])){
		   $this->error('删除失败！');
		   exit(0);
		}

	    if($this->Art->where('id='.$_GET['id'])->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){
			$ids = implode(',',$_POST['id']);
	
			if($this->Art->where('id in ('.$ids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class ChongZhiUrlAction extends CommonAction {
	private $ChongZhi;
	private $Type;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->ChongZhi=D('ChongZhi');
		$this->Type=D('Type');
	}

    public function index(){

		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->ChongZhi->group('userid')->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

		$rs=$this->ChongZhi->field('GROUP_CONCAT(id) as ids,userid,GROUP_CONCAT(typeid) as typeids,GROUP_CONCAT(url) as urls')->group('userid')->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();
		
        foreach($rs as $key => $val){
            $typeids = explode(',',$val['typeids']);
			$urls = explode(',',$val['urls']);
			$ids = explode(',',$val['ids']);

            foreach($typeids as $k => $v){
			    $trs = $this->Type->where('id='.$v)->field('nickname')->find();
				$rs[$key]['types'][] = array('typename'=>$trs['nickname'],'url'=>$urls[$k],'id'=>$ids[$k],'typeid'=>$v,'userid'=>$val['userid']);
			}
		}

		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/ChongZhiUrl.html');
    }

	public function set(){
        $id = trim($_GET['id']);
		$userid = trim($_GET['userid']);
		$typename = trim($_GET['typename']);

		if(empty($id) || empty($userid) || empty($typename)){
		   $this->error('对不起，您没有权限！');
		}

		$value=$this->ChongZhi->where('id='.$id)->field('url')->find();

        $value['id'] = $id;
		$value['userid'] = $userid;
		$value['typename'] = $typename;

		$this->assign($value);
		$this->assign('module','set');
		$this->display('./Tpl/Admin/ChongZhiUrl.html');
    }

	public function update(){

		if($_POST['module']=='add'){

			if(empty($_POST['typeid']) || empty($_POST['url'])){
				$this->error('你不能添加！');
				exit;
			}

			$url = $_POST['url'];
			$url = str_replace(chr(10),"",$url);
	        $url = explode(chr(13),$url);

			$maxuid = $this->ChongZhi->where('typeid='.$_POST['typeid'])->field('MAX(userid) as maxuid')->find();
            $trs = $this->Type->select();

			$i=0;
			foreach($url as $key => $val){
			    $data[$i]['userid'] = $key+1+$maxuid['maxuid'];
				$data[$i]['url'] = $val;
				$data[$i]['typeid'] = $_POST['typeid'];
                $i++;
				foreach($trs as $k => $v){
					if($v['id'] != $_POST['typeid'] ){
						$data[$i]['userid'] = $key+1+$maxuid['maxuid'];
						$data[$i]['url'] = '';
						$data[$i]['typeid'] = $v['id'];
						$i++;
					}
				}
			}

			

			if($this->ChongZhi->addAll($data)){
				$this->assign('jumpUrl','?s=Admin/ChongZhiUrl');
				$this->success('添加成功！');
			}
		}elseif($_POST['module']=='addone'){
		    if(empty($_POST['userid']) || empty($_POST['typeid']) || empty($_POST['url'])){
				$this->error('你不能添加！');
				exit;
			}

			$data['userid'] = $_POST['userid'];
			$data['typeid'] = $_POST['typeid'];
			$data['url'] = $_POST['url'];
            
			if($this->ChongZhi->add($data)){
				$this->assign('jumpUrl','?s=Admin/ChongZhiUrl');
			    $this->success('添加成功！');
			}else{
			    $this->error('添加失败！');
			}
		}else{
			if(empty($_POST['id']) || empty($_POST['url'])){
				$this->error('你不能修改！');
				exit;
			}

			$data['id'] = $_POST['id'];
			$data['url'] = $_POST['url'];
            
			if($this->ChongZhi->save($data)){
			    $this->assign('jumpUrl','?s=Admin/ChongZhiUrl');
			    $this->success('修改成功！');
			}else{
			    $this->error('修改失败！');
			}
		}
    }

	public function add(){
        $types = $this->Type->select();
		$this->assign('types',$types);
		$this->assign('module','add');
		$this->display('./Tpl/Admin/ChongZhiUrl.html');
    }

	public function addone(){

        if(empty($_GET['userid']) || empty($_GET['typeid'])){
			$this->error('你不能添加！');
			exit;
		}
		$type = $this->Type->where('id='.$_GET['typeid'])->find();
 
        $this->assign('nickname',$type['nickname']);
		$this->assign('typeid',$_GET['typeid']);
		$this->assign('userid',$_GET['userid']);
		$this->assign('module','addone');
		$this->display('./Tpl/Admin/ChongZhiUrl.html');
    }

	public function del(){
		if(empty($_GET['userid'])){
		   $this->error('对不起，您没有权限！');
		}
	    $userid=$_GET['userid'];
	    if($this->ChongZhi->where('userid='.$userid)->delete()){
		    $this->success('删除成功！');
		}else{
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['userid']) && is_array($_POST['userid'])){
			$userids = implode(',',$_POST['userid']);
	
			if($this->ChongZhi->where('userid in ('.$userids.')')->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class TypeAction extends CommonAction {
	private $Type;
	private $ChongZhi;

	public function __construct(){
		parent::__construct();

		if($this->role !== 0 && $this->role !== 2){
		    $this->error('对不起，您没有权限！');
			exit;
		}

	    $this->Type=D('Type');
		$this->ChongZhi=D('ChongZhi');
	}

    public function index(){
		$per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->Type->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}
        
		$rs=$this->Type->order('sort asc')->limit(($page-1)*$per_num.','.$per_num)->select();
		$this->assign('list',$rs);
		$this->assign('module','list');
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->display('./Tpl/Admin/type.html');
    }

	public function set(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
		$id=$_GET['id'];
		$value=$this->Type->where('id='.$id)->find();
		$this->assign($value);
		$this->display('./Tpl/Admin/type.html');
    }

	public function update(){
		if(!chkStr($_POST['name']) || !chkStr($_POST['nickname'])){
		   $this->error('信息不完整！');
		}
		$data['name']=$_POST['name'];
		$data['nickname']=$_POST['nickname'];
		$data['info']=stripslashes($_POST['info']);
        $data['sort']=is_numeric($_POST['sort']) ? $_POST['sort'] : 0;
		$data['main']=is_numeric($_POST['main']) ? $_POST['main'] : 0;
		$data['sellmax']=is_numeric($_POST['sellmax']) ? $_POST['sellmax'] : 0;
		$data['sellmin']=is_numeric($_POST['sellmin']) ? $_POST['sellmin'] : 0;
		$data['buymin']=is_numeric($_POST['buymin']) ? $_POST['buymin'] : 0;
		$data['up']=is_numeric($_POST['up']) ? $_POST['up'] : 0;
		$data['down']=is_numeric($_POST['down']) ? $_POST['down'] : 0;
		$data['url']=$_POST['url'];
		$data['port']=$_POST['port'];
		$data['username']=$_POST['username'];
		$data['password']=$_POST['password'];
		$data['divday']=$_POST['divday'];
		$data['divrates']=$_POST['divrates'];
        $data['yuan']=$_POST['yuan'];

		if(empty($_POST['id'])){
			$cz = $this->ChongZhi->group('userid')->field('userid')->select();
			if($cz){
			    $mo = new Model();
				$mo->startTrans();

				$tid = $mo->table('t_type')->add($data);
                
				foreach($cz as $key => $val){
				    $cz[$key]['typeid']=$tid;
				}

				$iscz = $mo->table('t_chong_zhi')->addAll($cz);

				if($tid && $iscz){
					$mo->commit();
				    $this->assign('jumpUrl','?s=Admin/Type');
					$this->success('添加成功！');
				}else{
					$mo->rollback();
					$this->error('添加失败！');
				}

			}else{
				if($this->Type->add($data)){
					$this->assign('jumpUrl','?s=Admin/Type');
					$this->success('添加成功！');
				}else{
					$this->error('添加失败！');
				}
			}
		}else{
			$data['id']=$_POST['id'];
		    if($this->Type->save($data)){
				$this->assign('jumpUrl','?s=Admin/Type');
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}
    }

	public function add(){
		$this->assign('main',0);
		$this->display('./Tpl/Admin/type.html');
    }

	public function del(){
		if(empty($_GET['id'])){
		   $this->error('对不起，您没有权限！');
		}
	    $id=$_GET['id'];

        $m = new Model();
		$m->startTrans();
		$rs = $m->table('t_chong_zhi')->where('typeid='.$id)->delete();

		$rs1 = $m->table('t_type')->where('id='.$id)->delete();

	    if($rs && $rs1){
			$m->commit();
		    $this->success('删除成功！');
		}else{
			$m->rollback();
		    $this->error('删除失败！');
		}
    }

	public function delAll(){

		if(!empty($_POST['id']) && is_array($_POST['id'])){

			$ids = implode(',',$_POST['id']);

			$m = new Model();
		    $m->startTrans();

			$rs = $m->table('t_chong_zhi')->where('typeid in ('.$ids.')')->delete();

		    $rs1 = $m->table('t_type')->where('id in ('.$ids.')')->delete();
	
			if($rs && $rs1){
				$m->commit();
				$this->success('删除成功！');
			}else{
				$m->rollback();
				$this->error('删除失败！');
			}
		}else{
		   $this->error('删除失败！'); 
		}
    }
}
﻿<?php

class UserAction extends CommonAction
{
    private $User;
    private $ChongZhi;
    private $Type;
    private $TransLog;
    private $ChongZhiLog;
    private $TiXianLog;
    private $Wallet;
    private $CzApply;
    private $TiXian;
    private $Fact;
    private $Bank;
    private $Issue;
    private $Buy;
    private $HgApply;

    public function __construct()
    {
        parent::__construct();
        $this->checkAuth();
        $this->User = D('User');
        $this->ChongZhi = D('ChongZhi');
        $this->Type = D('Type');
        $this->TransLog = D('TransLog');
        $this->ChongZhiLog = D('ChongZhiLog');
        $this->TiXianLog = D('TiXianLog');
        $this->Wallet = D('Wallet');
        $this->CzApply = D('CzApply');
        $this->TiXian = D('TiXian');
        $this->Fact = D('Fact');
        $this->Bank = D('Bank');
        $this->Issue = D('Issue');
        $this->Buy = D('Buy');
        $this->HgApply = D('HgApply');
    }

    public function index()
    {
        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
        $this->assign($user);

        $chongzhi = $this->ChongZhi->join('t_type as t on t.id=t_chong_zhi.typeid')->field('t.nickname,t_chong_zhi.*')->where('userid=' . $_SESSION['USER_KEY_ID'])->select();
        foreach ($chongzhi as $key => $val) {
            $chongzhi[$key]['total'] = $val['goldnum'] + $val['gdgold'];
            $chongzhi[$key]['total'] = round($chongzhi[$key]['total'], 8);
        }


        //认购总量
        $issue = $this->Issue->where('status=0')->find();
        $list = $this->Buy->where('userid=' . $_SESSION['USER_KEY_ID'])->select();
        foreach ($list as $k => $v) {
            $list[$k]['downnum'] = coin($v['num'] - $v['nownum']);
            $list[$k]['sumprice'] = coin($v['num'] * $v['price']);
        }
        $this->assign('buysum', coin($issue['num'] - $issue['nownum']));

        //获取用户个人信息
        $user=$this->getPersonInfo();
        $this->assign($user);

        $this->assign('chongzhi', $chongzhi);
        $this->assign('empty', '<tr><td colspan=5>没有找到数据</td></tr>');
        $this->display('./Tpl/Home/user_zijin.html');
    }

    public function setpwd()
    {
        $this->display('./Tpl/Home/user_pw.html');
    }

    public function updatepwd()
    {

        $oldpassword = trim($_POST['oldpassword']);
        $password = trim($_POST['password']);
        $repassword = trim($_POST['repassword']);

        if (empty($oldpassword) || empty($password) || empty($repassword)) {
            $this->error('修改失败');
            exit(0);
        }

        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();

        if ($user['password'] != md5($oldpassword)) {
            $this->error('修改失败');
            exit(0);
        }

        if (strlen($password) < 6) {
            $this->error('密码至少六位!');
            exit(0);
        }

        if ($password != $repassword) {
            $this->error('两次输入的密码不同！');
            exit(0);
        }

        $data['id'] = $_SESSION['USER_KEY_ID'];
        $data['password'] = md5($password);

        if ($this->User->save($data)) {
            $this->assign('jumpUrl', $this->path . '/User/safe');
            $this->success('修改成功！');
        } else {
            $this->error('修改失败！');
        }
    }

    //财务明细
    public function detail()
    {

        $type = trim($_GET['type']);
        if (empty($type)) {
            $type = 'trans';
        }

        $types = $this->Type->select();

        $coin = trim($_GET['coin']);
        if ($coin) {
            foreach ($types as $key => $val) {
                if ($coin == $val['id']) $types[$key]['style'] = "cur";
            }
            $where = ' and typeid=' . $coin;
        } else {
            $types[0]['style'] = "cur";
            $coin = $types[0]['id'];
        }

        $this->assign('types', $types);
        $this->assign('coin', $coin);


        if ($type == 'trans') {
            if ($coin) {
                $list = $this->TransLog->join('t_type_box as tb on (tb.id=t_trans_log.typeboxid and tb.name1=' . $coin . ')')->where('t_trans_log.userid=' . $_SESSION['USER_KEY_ID'])->select();
            } else {
                $list = $this->TransLog->where('userid=' . $_SESSION['USER_KEY_ID'])->select();
            }

            $this->assign('empty', '<tr><td colspan=7>没有找到数据</td></tr>');
        } elseif ($type == 'chongzhi') {
            $list = $this->CzApply->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and status=1 ' . $where)->select();
            $this->assign('empty', '<tr><td colspan=2>没有找到数据</td></tr>');
        } elseif ($type == 'tixian') {
            $list = $this->TiXian->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and status=1 ' . $where)->select();
            $this->assign('empty', '<tr><td colspan=4>没有找到数据</td></tr>');
        }

        $this->assign('list', $list);
        $this->assign('type', $type);
        $this->display('./Tpl/Home/user_detail.html');
    }


    function safe()
    {
        $user=$this->getPersonInfo();
        $this->assign($user);
        $this->display('./Tpl/Home/user_safe.html');
    }

    private function getPersonInfo(){
        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();

        foreach ($user as $key => $val) {
            $user[$key] = trim($val);
        }

        if (!chkStr($user['xm']) || !chkStr($user['card'])) {
            $user['chkReal'] = 0;
        }

        if (!chkStr($user['moble'])) {
            $user['chkMoble'] = 0;
        }

        if (!chkStr($user['transpw'])) {
            $user['chkTransPw'] = 0;
        }

        if (!chkStr($user['password'])) {
            $user['chkPw'] = 0;
        }

        if (!chkStr($user['email'])) {
            $user['chkEmail'] = 0;
        }

        if (!chkStr($user['alipay'])) {
            $user['chkAlipay'] = 0;
        }
        return $user;
    }


    function chkReal()
    {
        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
        if (chkStr($user['xm']) && chkStr($user['card'])) {
            $this->assign('jumpUrl', $this->path . '/User/safe');
            $this->error('对不起！您已经认证了！');
            exit;
        }

        $data['id'] = $_SESSION['USER_KEY_ID'];
        $data['xm'] = trim($_POST['xm']);
        $data['card'] = trim($_POST['card']);

        if (!chkStr($data['xm']) || !chkStr($data['card'])) {
            $this->display('./Tpl/Home/user_real.html');
        } elseif ($this->User->save($data)) {
            $this->assign('jumpUrl', $this->path . '/User/safe');
            $this->success('认证成功！');
        } else {
            $this->error('认证失败！');
        }
    }

    function chkMoble()
    {
        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
        if (!empty($user['moble'])) {
            $this->assign('jumpUrl', $this->path . '/User/safe');
            $this->error('对不起！您已经认证了！');
            exit;
        }

        $data['id'] = $_SESSION['USER_KEY_ID'];
        $data['moble'] = trim($_POST['moble']);
        $code = trim($_POST['code']);

        if (!chkStr($data['moble'])) {
            $this->display('./Tpl/Home/user_moble.html');
        } else {
            if (!chkStr($code) || $code != $_SESSION['pin'] || $data['moble'] != $_SESSION['phone']) {
                $this->assign('jumpUrl', '/?s=Home/User/chkMoble');
                $this->error('验证码错误！');
                exit;
            }

            if ($this->User->save($data)) {
                $_SESSION['pin'] = '';
                $_SESSION['phone'] = '';
                $this->assign('jumpUrl', $this->path . '/User/safe');
                $this->success('认证成功！');
            } else {
                $this->error('认证失败！');
            }
        }
    }

    function setTransPw()
    {
        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
        if (chkStr($user['transpw'])) {
            $this->assign('jumpUrl', $this->path . '/User/safe');
            $this->error('对不起！您已经设置了！');
            exit;
        }

        $password = trim($_POST['password']);
        $retranspw = trim($_POST['retranspw']);
        $data['transpw'] = trim($_POST['transpw']);

        if (!chkStr($password) || !chkStr($data['transpw']) || !chkStr($retranspw)) {
            $this->display('./Tpl/Home/user_transpw.html');
        } else {

            if ($user['password'] != md5($password) || $retranspw != $data['transpw']) {
                $this->error('设置失败！');
                exit;
            }

            $data['id'] = $_SESSION['USER_KEY_ID'];
            $data['transpw'] = md5($data['transpw']);

            if ($this->User->save($data)) {
                $this->assign('jumpUrl', $this->path . '/User/safe');
                $this->success('设置成功！');
            } else {
                $this->error('设置失败！');
            }
        }
    }

    function updateTransPw()
    {

        $oldtranspw = trim($_POST['oldtranspw']);
        $retranspw = trim($_POST['retranspw']);
        $data['transpw'] = trim($_POST['transpw']);

        if (!chkStr($oldtranspw) || !chkStr($data['transpw']) || !chkStr($retranspw)) {
            $this->display('./Tpl/Home/user_transpw_update.html');
        } else {

            $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();

            if ($user['transpw'] != md5($oldtranspw) || $retranspw != $data['transpw']) {
                $this->error('修改失败！');
                exit;
            }

            $data['id'] = $_SESSION['USER_KEY_ID'];
            $data['transpw'] = md5($data['transpw']);

            if ($this->User->save($data)) {
                $this->assign('jumpUrl', $this->path . '/User/safe');
                $this->success('修改成功！');
            } else {
                $this->error('修改失败！');
            }
        }
    }

    function resetTransPw()
    {

        $retranspw = trim($_POST['retranspw']);
        $data['transpw'] = trim($_POST['transpw']);
        $code = trim($_POST['code']);

        if (!chkStr($code) || !chkStr($data['transpw']) || !chkStr($retranspw)) {
            $this->display('./Tpl/Home/user_transpw_reset.html');
        } else {

            if (!chkStr($code) || $code != $_SESSION['pin']) {
                $this->assign('jumpUrl', '/?s=Home/User/resetTransPw');
                $this->error('验证码错误！');
                exit;
            }

            if ($retranspw != $data['transpw']) {
                $this->error('两次输入的密码不同！');
                exit;
            }

            $data['id'] = $_SESSION['USER_KEY_ID'];
            $data['transpw'] = md5($data['transpw']);

            if ($this->User->save($data)) {
                $this->assign('jumpUrl', $this->path . '/User/safe');
                $this->success('重置成功！');
            } else {
                $this->error('重置失败！');
            }
        }
    }

    function invit()
    {
        $user = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
        $list = $this->User->where('invit=\'' . $user['inviturl'] . '\' and isclose=1')->select();

        foreach ($list as $key => $val) {
            $list[$key]['chkReal'] = !chkStr($val['xm']) || !chkStr($val['card']) ? 0 : 1;
            $list[$key]['chkMoble'] = !chkStr($val['moble']) ? 0 : 1;
            $list[$key]['chkEmail'] = !chkStr($val['email']) ? 0 : 1;
        }
        $this->assign('invitnum', count($list));
        $this->assign('list', $list);

        $user['inviturl'] = 'http://' . $_SERVER['HTTP_HOST'] . $this->path . '/Login/reg/invit/' . $user['inviturl'];
        $this->assign($user);
        $this->assign('empty', '<tr><td colspan="5">没有找到数据</td></tr>');

        $this->display('./Tpl/Home/user_invit.html');
    }

    function chongzhi()
    {

        //判断充值上限下限
        $this->assign('chongzhiu', $this->sysv['chongzhiu']);
        $this->assign('chongzhid', $this->sysv['chongzhid']);

        $this->assign('pay_zhifubao', $this->sysv['pay_zhifubao']);
        $this->assign('pay_caifutong', $this->sysv['pay_caifutong']);
        $this->assign('pay_yibao', $this->sysv['pay_yibao']);
        $this->assign('pay_wangyin', $this->sysv['pay_wangyin']);

        $types = $this->Type->select();
        foreach ($types as $key => $val) {
            $cz = $this->ChongZhi->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $val['id'])->field('url')->find();
            if ($val['yuan'] == 1) {
                $types[$key]['url'] = $this->sysv['alipay'];
            } else {
                $types[$key]['url'] = $cz['url'];
            }
            $types[$key]['tid'] = intval($_GET['tid']) > 0 ? $_GET['tid'] : 0;
        }
        $this->assign('url', $types[0]['url']);
        $this->assign('types', $types);

        $typeid =5;// trim($_POST['typeid']);
        $goldnum = trim($_POST['goldnum']);
        $paytype = trim($_POST['paytype']);
        $url = trim($_POST['url']);
        $code = trim($_POST['code']);
        $OrderNo = createOrderNo();

        if (!chkNum($typeid) || !chkNum($goldnum) || !chkStr($code)) {
            $czApply = $this->CzApply->join('t_type t on t.id=t_cz_apply.typeid')->field('t.nickname,t_cz_apply.*')->where('t_cz_apply.userid=' . $_SESSION['USER_KEY_ID'])->order('id DESC')->select();
            $this->assign('list', $czApply);
            $this->assign('empty', '<tr><td colspan="4">没有找到数据</td></tr>');
            $this->display('./Tpl/Home/user_cz_apply.html');
        } else {
            $checkcode = strtolower($_SESSION['checkcode']);
            if ($code != $checkcode) {
                $this->error('验证码错误！');
                exit;
            }

            if ($goldnum < $this->sysv['chongzhid'] || $goldnum > $this->sysv['chongzhiu'])
                $this->error('您输入的金额超出网站限定！');

            $data['typeid'] = $typeid;
            $data['orderno'] = $OrderNo;
            $data['url'] = $url;
            $data['goldnum'] = coin($goldnum);
            $data['userid'] = $_SESSION['USER_KEY_ID'];
            $data['addtime'] = date('Y-m-d H:i:s', time());

            if ($this->CzApply->add($data)) {
                //$this->success('充值申请已提交，请等待审核！');
                $_SESSION['checkcode'] = '';

              //  if ($typeid == '5') {
                    //对接充值平台
                  //  $JumpUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/Pay/?' . 'paytype=' . $paytype . '&money=' . $goldnum . '&OrderNo=' . $OrderNo;
                  //  header("Location:$JumpUrl");
                 //   exit();
              //  }

                $this->assign('goldnum',$data['goldnum']);
                $this->assign('orderno', $data['orderno']);

                $this->display('./Tpl/Home/user_cz_alert.html');
                //对接充值平台


               // if ($_POST['yuan'] == 0)
               //     $this->display('./Tpl/Home/user_cz_alert.html');
               // else
                //    $this->display('./Tpl/Home/user_cz_alert1.html');


            } else {
                $this->error('提交失败');
            }
        }
    }

    public function alipay()
    {
        $this->error('对不起，暂时还不能人民币充值！');
    }

    function tixian()
    {

        //判断提现上限下限
        $this->assign('tixianu', $this->sysv['tixianu']);
        $this->assign('tixiand', $this->sysv['tixiand']);


        $this->assign('tixianflag',$this->sysv['txtype']);//提现种类开关
        $types = $this->Type->select();
        foreach ($types as $key => $val) {
            $wallet = $this->Wallet->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $val['id'])->field('url')->find();
            if ($val['yuan'] == 1) {
                $types[$key]['yuan'] = 1;
                $types[$key]['url'] = explode('|', $wallet['url']);
            } else {
                $types[$key]['yuan'] = 0;
                $types[$key]['url'] = $wallet['url'];
            }

            $types[$key]['tid'] = intval($_GET['tid']) > 0 ? $_GET['tid'] : 0;

            $cc = $this->ChongZhi->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $val['id'])->find();
            $types[$key]['coin'] = $cc['goldnum'];
        }

        $this->assign('url', $types[0]['url']);
        $this->assign('types', $types);

        $typeid = trim($_POST['typeid']);
        $goldnum = trim($_POST['goldnum']);
        $code = trim($_POST['code']);
        $realnum = trim($_POST['realnum']);
        $shouxu = trim($_POST['shouxu']);
        $url = trim($_POST['url']);
        $code = trim($_POST['code']);











        if (!isset($_POST['typeid'])) {
            $tixian = $this->TiXian->join('t_type t on t.id=t_ti_xian.typeid')->field('t.nickname,t_ti_xian.*')->where('t_ti_xian.userid=' . $_SESSION['USER_KEY_ID'])->select();

            $a=$this->sysv['bank_zgyh'];
            $b=$this->sysv['bank_zgnyyh'];
            $c=$this->sysv['bank_zggsyh'];
            $d=$this->sysv['bank_zgjsyh'];
            //获取姓名

            $NameArr = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();

            $Arr[0]=$NameArr['alipay'].'|姓名:'.$NameArr['xm'];
            $userBank = D('UserBank')->where('user_id=' . $_SESSION['USER_KEY_ID'])->select();
            foreach ($userBank as &$value) {
                if(!limitBank($a,$b,$c,$d,$value['bankflag'])) continue;
                $value['bankflag'] = getBank($value['bankflag']);
                $Arr[$value['id']]=$value['bankflag'].'尾数为***'.substr($value['cardnum'],strlen($value['cardnum'])-4,4);
            }

            $UserMoney=$this->getUserMoney();//获取用户余额
            $this->assign('RMBYE', $UserMoney[0]);
            $this->assign('XNBYE', $UserMoney[1]);


            $this->assign('Arr', $Arr);
            $this->assign('username', $NameArr['xm']);
            $this->assign('list', $tixian);
            $this->assign('txfee', $this->sysv['txfee']);
            $this->assign('empty', '<tr><td colspan="7">没有找到数据</td></tr>');
            $this->display('./Tpl/Home/user_tx_apply.html');
        } else {

            if ($goldnum < $this->sysv['tixiand'] || $goldnum > $this->sysv['tixianu'])
                $this->error('您的本次提现金额超出网站限定！');


            $u = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
            if (!chkStr($_POST['transpw']) || $u['transpw'] != md5($_POST['transpw'])) {
                $this->assign('jumpUrl', '/?s=Home/User/tixian');
                $this->error('交易密码错误！');
                exit;
            }

            if (!chkStr($code) || $code != $_SESSION['pin']) {
                $this->assign('jumpUrl', '/?s=Home/User/tixian');
                $this->error('验证码错误！');
                exit;
            }

            $a=$this->sysv['bank_zgyh'];
            $b=$this->sysv['bank_zgnyyh'];
            $c=$this->sysv['bank_zggsyh'];
            $d=$this->sysv['bank_zgjsyh'];
            //获取姓名

            $NameArr = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();

            $Arr[0]='支付宝：'.$NameArr['alipay'].'|姓名:'.$NameArr['xm'];
            $userBank = D('UserBank')->where('user_id=' . $_SESSION['USER_KEY_ID'])->select();
            foreach ($userBank as &$value) {
                if(!limitBank($a,$b,$c,$d,$value['bankflag'])) continue;
                $value['bankflag'] = getBank($value['bankflag']);
                $Arr[$value['id']]=$value['bankflag'].'姓名：'.$NameArr['xm'].'|卡号:'.$value['cardnum'].'|开户行：'.$value['cardmark'];
            }



            $data['typeid'] = $typeid;
            $data['goldnum'] = coin($goldnum);
            $data['realnum'] = coin($goldnum - $goldnum * $shouxu / 100);
            $data['shouxu'] = $shouxu;
            $data['url'] =$Arr[$url];
            $data['userid'] = $_SESSION['USER_KEY_ID'];
            $data['addtime'] = date('Y-m-d H:i:s', time());



            $mo = new Model();
            $mo->startTrans();

            $cz = $this->ChongZhi->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $typeid)->find();

            $u_d['id'] = $cz['id'];
            $u_d['goldnum'] = $cz['goldnum'] - $goldnum;
            $rs = $mo->table('t_chong_zhi')->save($u_d);

            $rs1 = $mo->table('t_ti_xian')->add($data);

            if ($rs && $rs1) {
                $mo->commit();
                $this->success('提现申请已提交，请等待审核！');
            } else {
                $mo->rollback();
                $this->error('提交失败');
            }
        }
    }

    function wallet()
    {
        $rs = $this->Wallet->join('t_type t on t.id=t_wallet.typeid')->where('t_wallet.userid=' . $_SESSION['USER_KEY_ID'])->field('t.nickname as tname,t.yuan,t_wallet.*')->select();
        foreach ($rs as $k => $v) {

            if ($v['yuan'] == 1) {
                $a = explode('|', $v['url']);
                $rs[$k]['url'] = '支付宝帐号：' . $a[0] . ' &nbsp;&nbsp; 银行卡帐号：' . $a[1];
            }
        }

        $this->assign('list', $rs);
        $this->display('./Tpl/Home/user_wallet.html');
    }

    function setWallet()
    {
        if (!chkNum($_GET['id'])) {
            $this->error('操作失败！');
            exit;
        }
        $rs = $this->Wallet->where('id=' . $_GET['id'])->find();
        if ($_SESSION['USER_KEY_ID'] != $rs['userid']) {
            $this->error('非法访问！');
            exit;
        }


        $this->assign($rs);

        $rs1 = $this->Type->where('id=' . $rs['typeid'])->find();
        $this->assign('yuan', $rs1['yuan']);


        $this->display('./Tpl/Home/user_setwallet.html');
    }

    function updateWallet()
    {
        if (!chkNum($_POST['id'])) {
            $this->error('修改失败！');
            exit;
        }

        $data['id'] = $_POST['id'];

        if (chkNum($_POST['yuan'])) {
            if (!chkStr($_POST['alipay']) && !chkStr($_POST['bank'])) {
                $this->error('支付宝帐号或银行卡号不能为空！');
            }
            $data['url'] = $_POST['alipay'] . '|' . $_POST['bank'];
        } else {
            if (!chkStr($_POST['url'])) {
                $this->error('地址不能为空！');
            }
            $data['url'] = $_POST['url'];
        }

        if ($this->Wallet->save($data)) {
            $this->assign('jumpUrl', $this->path . '/User/wallet');
            $this->success('修改成功！');
        } else {
            $this->error('修改失败！');
        }
    }


    public function addUserBank()
    {
        if (!isset($_POST['cardnum'])) {
            session('POST', true);

            $name = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->field('xm')->select();
            $this->assign('name', $name[0]['xm']);

            $this->display('./Tpl/Home/user_addUserBank.html');
            exit();
        }
        if (!session('POST')) {
            session('POST', true);
            $this->error('你已经提交过，请不要刷新提交！');
            exit();
        }
        session('POST', false);

        if (!chkStr($_POST['bankflag'])) {
            $this->error('银行不能为空！');
        }
        if (!chkStr($_POST['cardnum'])) {
            $this->error('卡号不能为空！');
        }

        if (!chkStr($_POST['cardmark'])) {
            $this->error('开户行不能为空！');
        }

        if (!chkStr($_POST['name'])) {
            $this->error('户主名不能为空！');
        }
        $name = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->field('xm')->select();
        $name = $name[0]['xm'];
        $data['user_id'] = $_SESSION['USER_KEY_ID'];
        $data['bankflag'] = $_POST['bankflag'];
        $data['cardnum'] = $_POST['cardnum'];
        $data['cardmark'] = $_POST['cardmark'];
        $data['name'] = $name;
        if (D('UserBank')->add($data)) {
            $this->assign('jumpUrl', $this->path . '/User/showUserBank');
            $this->success('添加成功！');

        } else {
            $this->error('添加失败！');
        }


    }

    public function showUserBank()
    {

        $userBank = D('UserBank')->where('user_id=' . $_SESSION['USER_KEY_ID'])->select();

        foreach ($userBank as &$value) {
            $value['bankflag'] = getBank($value['bankflag']);
        }
        //print_r($userBank);die();
        $this->assign('userBank', $userBank);

        $this->display('./Tpl/Home/user_showUserBank.html');

    }

    public function AlterUserBank()
    {
        if (!isset($_POST['cardnum'])) {
            session('POST', true);

            $info = D('UserBank')->where('user_id=' . $_SESSION['USER_KEY_ID'].' And id='.$_GET['id'])->select();

            $this->assign('info', $info[0]);

            $this->display('./Tpl/Home/user_AlterUserBank.html');
            exit();
        }
        if (!session('POST')) {
            session('POST', true);
            $this->error('你已经提交过，请不要刷新提交！');
            exit();
        }
        session('POST', false);

        if (!chkStr($_POST['bankflag'])) {
            $this->error('银行不能为空！');
        }
        if (!chkStr($_POST['cardnum'])) {
            $this->error('卡号不能为空！');
        }

        if (!chkStr($_POST['cardmark'])) {
            $this->error('开户行不能为空！');
        }

        if (!chkStr($_POST['name'])) {
            $this->error('户主名不能为空！');
        }
        $name = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->field('xm')->select();
        $name = $name[0]['xm'];
        $data['user_id'] = $_SESSION['USER_KEY_ID'];
        $data['bankflag'] = $_POST['bankflag'];
        $data['cardnum'] = $_POST['cardnum'];
        $data['cardmark'] = $_POST['cardmark'];
        $data['name'] = $name;
        if (D('UserBank')->where('user_id='.$_SESSION['USER_KEY_ID'].' and id='.$_POST['id'])->save($data)) {
            $this->assign('jumpUrl', $this->path . '/User/showUserBank');
            $this->success('修改成功！');

        } else {
            $this->error('修改失败！');
        }

    }

    public function delUserBank()
    {
        if (!chkNum($_GET['id'])) {
            $this->error('ID号不能为空！');
        }

        $result = D('UserBank')->where('id =' . $_GET['id'])->delete();

        if ($result !== false) {
            $this->assign('jumpUrl', $this->path . '/User/showUserBank');
            $this->success('删除成功！');
        } else {
            $this->error('删除数据失败！');
        }


    }


    function fact()
    {
        $rs = $this->Fact->field('*,DATE_FORMAT(addtime,"%Y-%m-%d") as addtime')
            ->where('userid=' . $_SESSION['USER_KEY_ID'])
            ->group('DATE_FORMAT(addtime,"%Y-%m-%d")')->select();

        $this->assign('list', $rs);
        $t = $this->Type->where('main=1')->find();

        $this->assign('typeid', $t['id']);

        $cz = $this->ChongZhi->where('typeid=' . $t['id'])->field('SUM(goldnum) as total')->find();
        $this->assign('allnum', $cz['total']);

        $fact = $this->Fact->where('addtime between CURDATE()-interval 1 day and CURDATE()')->field('SUM(coin) as total')->find();
        $this->assign('lastnum', $fact['total']);

        $ucz = $this->ChongZhi->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $t['id'])->find();

        $yield = coin(intval($ucz['goldnum'] + $ucz['gdgold']) / intval($cz['total'])) * 2;
        $this->assign('yield', $yield);

        $this->display('./Tpl/Home/user_fact.html');
    }

    function work()
    {
        $data['coin'] = $_POST['y'];
        $data['typeid'] = $_POST['t'];
        $data['userid'] = $_SESSION['USER_KEY_ID'];
        $data['addtime'] = date('Y-m-d H:i:s', time());
        $this->Fact->add($data);
    }

    function bank()
    {
        $types = $this->Type->select();
        foreach ($types as $key => $val) {
            $wallet = $this->Wallet->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $val['id'])->field('url')->find();
            $types[$key]['tid'] = intval($_GET['tid']) > 0 ? $_GET['tid'] : 0;

            $cc = $this->ChongZhi->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $val['id'])->find();
            $types[$key]['coin'] = $cc['goldnum'];
        }
        $this->assign('types', $types);

        $typeid = trim($_POST['typeid']);
        $coin = trim($_POST['coin']);
        $transpw = trim($_POST['transpw']);
        $code = strtolower($_POST['code']);
        $checkcode = strtolower($_SESSION['checkcode']);

        if (!chkNum($typeid) || !chkNum($coin)) {
            $rs = $this->Bank->join('t_type t on t.id=t_bank.typeid')->field('t.nickname,t_bank.*')->where('t_bank.userid=' . $_SESSION['USER_KEY_ID'])->select();
            $this->assign('list', $rs);
            $this->assign('empty', '<tr><td colspan="7">没有找到数据</td></tr>');
            $this->display('./Tpl/Home/user_bank.html');
        } else {
            $u = $this->User->where('id=' . $_SESSION['USER_KEY_ID'])->find();
            if (!chkStr($_POST['transpw']) || $u['transpw'] != md5($_POST['transpw'])) {
                $this->error('交易密码错误！');
                exit;
            }

            if ($code != $checkcode) {
                $this->error('验证码错误！');
                exit;
            }

            $data['typeid'] = $typeid;
            $data['coin'] = coin($coin);
            $data['userid'] = $_SESSION['USER_KEY_ID'];
            $data['addtime'] = date('Y-m-d H:i:s', time());

            $mo = new Model();
            $mo->startTrans();

            $cz = $this->ChongZhi->where('userid=' . $_SESSION['USER_KEY_ID'] . ' and typeid=' . $typeid)->find();

            $u_d['id'] = $cz['id'];
            $u_d['goldnum'] = coin($cz['goldnum'] - $coin);
            $rs = $mo->table('t_chong_zhi')->save($u_d);

            $rs1 = $mo->table('t_bank')->add($data);

            if ($rs && $rs1) {
                $mo->commit();
                $this->success('操作成功！');
            } else {
                $mo->rollback();
                $this->error('操作失败');
            }
        }
    }

    public function buy()
    {
        $issue = $this->Issue->where('status=0')->find();

        if (empty($_POST)) {
            $list = $this->Buy->where('userid=' . $_SESSION['USER_KEY_ID'])->select();
            foreach ($list as $k => $v) {
                $list[$k]['downnum'] = coin($v['num'] - $v['nownum']);
                $list[$k]['sumprice'] = coin($v['num'] * $v['price']);
            }
            $this->assign('list', $list);
            $this->assign('issueid', $issue['id']);
            $this->assign('price', $issue['price']);
            $this->assign('limit', $issue['limit']);
            $this->assign('issued', $issue['num']);
            $this->assign('surplus', $issue['nownum']);
            $this->assign('buysum', coin($issue['num'] - $issue['nownum']));
            $this->assign('info', $issue['info']);
            $this->assign('empty', '<tr><td colspan=7>没有找到数据！</td></tr>');

            $sum = $this->Buy->where('issueid=' . $issue['id'] . ' and userid=' . $_SESSION['USER_KEY_ID'])->sum('num');

            $oldable = coin($issue['limit'] - $sum);
            $newable = $issue['nownum'] > $oldable ? $oldable : $issue['nownum'];
            $this->assign('ablenum', $newable);

            $this->assign('sum', $sum);

            $ilist = $this->Issue->order('addtime asc')->select();
            $i = 1;
            foreach ($ilist as $k => $v) {
                $ilist[$k]['title'] = '第' . $i . '期';
                $i++;
            }
            $this->assign('ilist', $ilist);

            $this->display('./Tpl/Home/user_buy.html');
        } else {
            if (!$issue) {
                $this->error('对不起！该期认购已结束。');
            }
            $d['userid'] = $_SESSION['USER_KEY_ID'];
            $d['issueid'] = $_POST['issueid'];
            $d['price'] = $_POST['price'];
            $d['limit'] = $_POST['limit'];
            $d['num'] = $_POST['num'];
            $d['nownum'] = $_POST['num'];
            $d['addtime'] = date('Y-m-d H:i:s', time());

            $sum = $this->Buy->where('issueid=' . $d['issueid'] . ' and userid=' . $d['userid'])->sum('num');

            if (!chkNum($d['num']) || !chkNum($d['issueid']) || !chkNum($d['price']) || !chkNum($d['limit']))
                $this->error('认购失败！');
            if ($d['num'] > $d['limit'] || $sum + $d['num'] > $d['limit']) $this->error('认购量超过限制！');

            $m = new Model();
            $m->startTrans();
            $rs = $m->table('t_buy')->add($d);

            $d1['id'] = $issue['id'];
            $d1['issuenum'] = $issue['issuenum'] + $d['num'];
            $d1['nownum'] = $issue['nownum'] - $d['num'] > 0 ? coin($issue['nownum'] - $d['num']) : 0;

            if ($d1['nownum'] <= 0)
                   $d1['status'] = 1;
            $rs1 = $m->table('t_issue')->save($d1);

            $type = $this->Type->where('yuan=1')->find();
            $cz = $this->ChongZhi->where('typeid=' . $type['id'] . ' and userid=' . $_SESSION['USER_KEY_ID'])->find();

            if (!chkNum($cz['goldnum']) || $cz['goldnum'] < $d['price'] * $d['num']) {
                $m->rollback();
                $this->assign('jumpUrl', '/?s=Home/User/chongzhi');
                $this->error('您的人民币余额不足，请充值！');
            }

            $d2['id'] = $cz['id'];
            $d2['goldnum'] = coin($cz['goldnum'] - $d['price'] * $d['num']);
            $rs2 = $m->table('t_chong_zhi')->save($d2);

            $maint = $this->Type->where('main=1')->find();

            if ($maint) {
                //打入账户
               // $nownums = $this->Buy->where('issueid=' . $d['issueid'] . ' and userid=' . $d['userid'])->sum('nownum');
                $cz1 = $this->ChongZhi->where('typeid=' . $maint['id'] . ' and userid=' . $_SESSION['USER_KEY_ID'])->find();

                $d3['id'] = $cz1['id'];
                $d3['goldnum'] = coin($cz1['goldnum'] + $d['num']);
                $rs3 = $m->table('t_chong_zhi')->save($d3);
            } else {
                $rs3 = true;
            }

            if ($rs && $rs1 && $rs2 && $rs3) {
                $m->commit();
                $this->success('认购成功！');
            } else {
                $m->rollback();
                $this->error('认购失败！');
            }
        }
    }

    public function hg()
    {
        if (empty($_POST)) {

            //判断回购上限下限
            $this->assign('huigouu', $this->sysv['huigouu']);
            $this->assign('huigoud', $this->sysv['huigoud']);


            $list = $this->HgApply->select();
            $this->assign('list', $list);

            $buy = $this->Buy->where('id=' . $_GET['id'])->find();
            $this->assign('buyid', $buy['id']);
            $this->assign('price', $buy['price']);
            $this->assign('nownum', $buy['nownum']);
            $this->assign('issueid', $buy['issueid']);
            $this->display('./Tpl/Home/user_hg_apply.html');
        } else {

            if (coin($_POST['num']) < $this->sysv['huigoud'] || coin($_POST['num']) > $this->sysv['huigouu'])
                $this->error('您的本次申请数目超出网站限定！');


           // $issue = $this->Issue->where('status=1 and id=' . $_POST['issueid'])->find();


           // if (!$issue) {
            if (!$this->sysv['huigouflag']) {
                $this->error('对不起，现在还不能申请回购！');
            }



            $d['userid'] = $_SESSION['USER_KEY_ID'];
            $d['buyid'] = $_POST['buyid'];
            $d['price'] = coin($_POST['price']);
            $d['num'] = coin($_POST['num']);
            $d['total'] = coin($_POST['num'] * $_POST['price']);
            $d['addtime'] = date('Y-m-d H:i:s', time());

            if (!chkNum($d['buyid']) || !chkNum($d['price']) || !chkNum($d['num'])) {
                $this->error('认购失败！');
            }

            $m = new Model();
            $m->startTrans();
            $rs = $m->table('t_hg_apply')->add($d);

            $buy = $this->Buy->where('id=' . $_POST['buyid'])->find();

            $d1['id'] = $buy['id'];
            $d1['nownum'] = $buy['num'] - $d['num'] > 0 ? coin($buy['num'] - $d['num']) : 0;
            $d1['status'] = -1;
            $rs1 = $m->table('t_buy')->save($d1);

            if ($rs && $rs1) {
                $m->commit();
                $this->success('提交成功！请等待审核。');
            } else {
                $m->rollback();
                $this->error('提交失败！');
            }
        }
    }


    private function getUserMoney(){
        $chongzhi = $this->ChongZhi->join('t_type as t on t.id=t_chong_zhi.typeid')->field('t.nickname,t_chong_zhi.*')->where('userid=' . $_SESSION['USER_KEY_ID'])->select();
        foreach ($chongzhi as $key => $val) {
            $chongzhi[$key]['total'] = $val['goldnum'] + $val['gdgold'];
            $chongzhi[$key]['total'] = round($chongzhi[$key]['total'], 8);
        }
        $UserMoney[0]=$chongzhi[0]['total'];//总人民币余额
        $UserMoney[1]=$chongzhi[0]['total'];//总虚拟币余额
        return $UserMoney;
    }
}
<?php
// 本类由系统自动生成，仅供测试用途
class CommonAction extends Action {
    protected $sysv;
	protected $user_info;
	protected $arts;
	protected $path;

	public function __construct(){
		$this->assign('login_user_id',$_SESSION['USER_KEY_ID']);
		$this->assign('login_user_name',$_SESSION['USER_KEY']);

        $User = D('user');
		$u = $User->where('id='.$_SESSION['USER_KEY_ID'])->find();

		$this->assign('user_info',$u);

        $Sys = D('Sys');
        $this->sysv = $Sys->where('id=1')->find();



        //买卖开关
        $this->assign('buyflag',$this->sysv['buyflag']);
        $this->assign('saleflag',$this->sysv['saleflag']);




		$this->sysv['qq'] = str_replace(chr(10),"",$this->sysv['qq']);
		$this->sysv['qq'] = explode(chr(13),$this->sysv['qq']);
		foreach($this->sysv['qq'] as $key => $val){
			$a = explode(':',$val);
		    $arr[$key]['name'] = $a[0];
			$arr[$key]['text'] = $a[1];
		}
        $this->sysv['qq'] = $arr;

		$this->sysv['qun'] = str_replace(chr(10),"",$this->sysv['qun']);
		$this->sysv['qun'] = explode(chr(13),$this->sysv['qun']);
		foreach($this->sysv['qun'] as $key => $val){
			$a1 = explode(':',$val);
		    $arr1[$key]['name'] = $a1[0];
			$arr1[$key]['text'] = $a1[1];
			$arr1[$key]['url'] = $a1[2].':'.$a1[3];
		}
        $this->sysv['qun'] = $arr1;
		$this->assign('qun',$arr1);

		$this->assign('sys',$this->sysv);
		$this->assign('path','/?s=Home');
		$this->path = '/?s=Home';

		$mo = new Model();

		$tt = $mo->table('t_type')->where('main=1')->find();
		$tb = $mo->table('t_type_box')->where('name1='.$tt['id'])->find();

		$gd = $mo->table('t_gua_dan')->where('typeboxid='.$tb['id'])->order('id desc')->find();
		$now['now'] = $gd['price'];
      
		$gd = $mo->table('t_gua_dan')->where('typeboxid='.$tb['id'])->order('price desc')->find();
		$now['high'] = $gd['price'];

		$gd = $mo->table('t_gua_dan')->where('typeboxid='.$tb['id'])->order('price asc')->find();
		$now['low'] = $gd['price'];

		$trans = $mo->table('t_trans_log')->where('typeboxid='.$tb['id'].' and zhu=1')->sum('num');
		$now['total'] = $trans;

		$sum = $mo->table('t_trans_log')->where('typeboxid='.$tb['id'].' and zhu=1 and DATEDIFF(now(),addtime)<=1')->sum('sum');
		$now['totalprice'] = $sum;

		$this->assign('now',$now);
       
		$this->arts['serv'] = $mo->table('t_art')->where('cate="serv"')->order('id asc')->select();
		$this->assign('serv',$this->arts['serv']);

		$this->arts['about'] = $mo->table('t_art')->where('cate="about"')->order('id asc')->select();
		$this->assign('about',$this->arts['about']);

		$this->arts['mserv'] = $mo->table('t_art')->where('cate="mserv"')->order('id asc')->select();
		$this->assign('mserv',$this->arts['mserv']);

		$this->arts['warn'] = $mo->table('t_art')->where('cate="warn"')->order('id asc')->find();
		$this->assign('warn',$this->arts['warn']);

		$this->arts['help'] = $mo->table('t_art')->where('cate="help"')->order('id asc')->select();
		$this->arts['news'] = $mo->table('t_art')->where('cate="news"')->order('id asc')->limit(20)->select();
		$this->assign('news',$this->arts['news']);

		$this->arts['reg'] = $mo->table('t_art')->where('cate="reg"')->order('id asc')->find();
		$this->assign('reg',$this->arts['reg']);

		$this->arts['fact'] = $mo->table('t_art')->where('cate="fact"')->order('id asc')->find();
		$this->assign('fact',$this->arts['fact']);
	}

	protected function checkAuth(){
		if (!$_SESSION['USER_KEY'] || !$_SESSION['USER_KEY_ID']) {
			$this->assign('jumpUrl',$this->path.'/Login');
			$this->error('请登录！');
			exit(0);
		}
	}

	protected function checkAuthAjax(){
		if (!$_SESSION['USER_KEY'] || !$_SESSION['USER_KEY_ID']) {
			return false;
			exit(0);
		}else{
		    return true;
		}
	}

	protected function sendEmail($uemail,$title,$content){

	    require("class.phpmailer.php");

        if($this->sysv['smtp']=='smtp.gmail.com'){
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->CharSet='UTF-8';
			$mail->Host = $this->sysv['smtp'];
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "ssl"; 
			$mail->Port = "465";
			$mail->Username = $this->sysv['email'];
			$mail->Password = $this->sysv['pwd'];
			$mail->From = $this->sysv['email'];
			$mail->FromName = $this->sysv['auth'];
			$mail->AddAddress($uemail, "user");
			$mail->IsHTML(true);
			$mail->Subject = $title;
			$mail->Body = $content;

			if(!$mail->Send())
			{
				return false;
			}else{
				return true;
			}
		}else{
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->CharSet='UTF-8';
			$mail->Host = $this->sysv['smtp'];
			$mail->SMTPAuth = true;
			$mail->Port = "25";
			$mail->Username = $this->sysv['email'];
			$mail->Password = $this->sysv['pwd'];
			$mail->From = $this->sysv['email'];
			$mail->FromName = $this->sysv['auth'];
			$uemail;$mail->AddAddress($uemail, "user");
			$mail->IsHTML(true);
			$mail->Subject = $title;
			$mail->Body = $content;

			if(!$mail->Send())
			{
				return false;
			}else{
				return true;
			}
		}
	}

    protected function getArr($uname,$upass,$host,$port,$by){
		Vendor ( 'Bitcoin.bitcoin', '', '.inc' );
		$Bitcoin = new BitcoinClient ('http',$uname,$upass,$host,$port);
		$address = $Bitcoin->getnewaddress ($by);
		return $address;
	}
}
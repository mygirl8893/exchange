<?php
class TiXianAction extends CommonAction {
    private $TiXian;
	private $ChongZhi;
	private $Type;
	private $User;

	public function __construct(){
		parent::__construct();
		$this->checkAuth();
	    $this->TiXian = D('TiXian');
		$this->Type = D('Type');
		$this->ChongZhi = D('ChongZhi');
		$this->User = D('User');
	}

    public function index(){
        $per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TiXian->where('userid='.$_SESSION['USER_KEY_ID'])->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

        $list=$this->TiXian->join('t_type t on (t.id=t_ti_xian.typeid)')->where('t_ti_xian.userid='.$_SESSION['USER_KEY_ID'])->field('t_ti_xian.*,t.name')->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();

        $this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('module','list');

		$this->display('./Tpl/Home/user_tixian.html');
    }

	public function add(){
		if(empty($_GET['typeid'])){
		    $this->error('����ʧ�ܣ�');
		}

		$chongzhi = $this->ChongZhi->where('typeid='.$_GET['typeid'].' and userid='.$_SESSION['USER_KEY_ID'])->field('goldnum,id')->find();

        $this->assign('goldnum',$chongzhi['goldnum']);
		$this->assign('czid',$chongzhi['id']);
		$this->assign('typeid',$_GET['typeid']);

	    $this->display('./Tpl/Home/user_tixian.html');
	}

	public function insert(){
		$_POST['url'] = trim($_POST['url']);
        if(empty($_POST['typeid']) || empty($_POST['goldnum']) || empty($_POST['realnum']) || empty($_POST['url']) || empty($_POST['czid'])|| empty($_POST['transpw'])){
		    $this->error('����ʧ�ܣ�');
		}

		$u = $this->User->where('userid='.$_SESSION['USER_KEY_ID'])->find();
		if(!chkStr($u['transpw']) || $u['transpw']!=md5($_POST['transpw'])){
		    $this->error('�����������');
		}
	
		$data['userid'] = $_SESSION['USER_KEY_ID'];
		$data['typeid'] = $_POST['typeid'];
		$data['goldnum'] = coin($_POST['goldnum']);
		$data['realnum'] = coin($_POST['realnum']);
		$data['url'] = $_POST['url'];
		$data['shouxu'] = coin($_POST['goldnum'] * $this->sysv['txfee']);
		$data['addtime'] = date('Y-m-d H:i:s',time());

		if($_POST['yue'] < $data['goldnum']){
		    $this->error('������');
		}

        $mo = new Model();
		$mo->startTrans();

		$rs1 = $mo->table('t_ti_xian')->add($data);
		$rs2 = $mo->table('t_ti_xian_log')->add($data);

		$rs3 = $mo->table('t_chong_zhi')->where('id='.$_POST['czid'].' and goldnum>='.$data['goldnum'])->setField('goldnum',array('exp','goldnum-'.$data['goldnum']));
		
		if($rs1 && $rs2 && $rs3){
			$mo->commit();
			$this->assign('jumpUrl',$this->path.'/TiXian');
		    $this->success('�����ɹ���');
		}else{
			$mo->rollback();
		    $this->error('����ʧ�ܣ�');
		}
    }
}
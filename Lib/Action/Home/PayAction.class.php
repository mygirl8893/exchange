<?php

/**
 * Created by PhpStorm.
 * User: Bridge
 * Date: 5/13/14
 * Time: 7:10 PM
 */
class PayAction extends CommonAction
{


    public function __construct()
    {
        parent::__construct();
        $this->User = D('User');
        $this->ChongZhi = D('ChongZhi');
        $this->Type = D('Type');
        $this->TransLog = D('TransLog');
        $this->ChongZhiLog = D('ChongZhiLog');
        $this->TiXianLog = D('TiXianLog');
        $this->Wallet = D('Wallet');
        $this->CzApply = D('CzApply');
        $this->TiXian = D('TiXian');
        $this->Fact = D('Fact');
        $this->Bank = D('Bank');
        $this->Issue = D('Issue');
        $this->Buy = D('Buy');
        $this->HgApply = D('HgApply');

    }

    public function index()
    {
        $this->checkAuth();
        $Key = trim(session("PayAl"));
        $orderNo = trim(session('OrderNo'));
        $StrB = md5($Key . $orderNo);
       if ($_GET['str'] == $StrB && session("PayAl") != null) {
            session("PayAl", null);
            if ($this->savePay($orderNo)) {

                $JumpUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/?s=Home/User';
                header("Location:$JumpUrl");
            }else{
                echo '保存个人信息失败，联系管理员!';
            }

        } else {
            echo '非法访问!';
        }
    }


    public function NotifyPaySave()
    {
        $Key = C('NotifyAutoKey');
        $orderNo = $_GET['OrderNo'];
        $SignB = md5($Key . $orderNo);
        if ($SignB == $_GET['Sign']) {
            if($this->savePay($orderNo))
                return 'success';

        } else {
                 return 'fail';
        }
    }



    private function savePay($orderNo)
    {


        if(D('OrdernoFlag')->where("orderno='$orderNo'")->find())//如果已经有保存记录，那就不会再保存了
            return true;

        $debug=true;
        $data['status'] = 1;

        $ca = $this->CzApply->where("orderno='$orderNo'")->find();

        $cz = $this->ChongZhi->where('userid=' . $ca['userid'] . ' and typeid=' . $ca['typeid'])->find();

        $mo = new Model();
        $mo->startTrans();

        if(!$ca['status'])//如果没有后台通知保存，那么我们那主动保存
          $rs = $this->CzApply->where("orderno='$orderNo'")->save($data);
        else
          $rs =true;


        $d['id'] = $cz['id'];
        $d['goldnum'] = coin($cz['goldnum'] + $ca['goldnum']);

        $rs1 = $mo->table('t_chong_zhi')->save($d);

        //推荐奖励
        $count = $this->CzApply->where('userid=' . $ca['userid'])->count();
        $user = $this->User->where('id=' . $ca['userid'])->find();
        if ($count <= 0 && chkStr($user['invit'])) {
            $user1 = $this->User->where('inviturl=\'' . $user['invit'] . '\'')->find();
            if (chkStr($user1['invit'])) {
                $cz1 = $this->ChongZhi->where('userid=' . $user1['id'] . ' and typeid=' . $ca['typeid'])->find();

                $d1['id'] = $cz1['id'];
                $d1['goldnum'] = coin($cz1['goldnum'] + $ca['goldnum'] * $this->site['award1'] / 100);
                if (chkNum($d1['goldnum'])) $award[] = $mo->table('t_chong_zhi')->save($d1);

                $user2 = $this->User->where('inviturl=\'' . $user1['invit'] . '\'')->find();
                if (chkStr($user2['invit'])) {
                    $cz2 = $this->ChongZhi->where('userid=' . $user2['id'] . ' and typeid=' . $ca['typeid'])->find();

                    $d2['id'] = $cz2['id'];
                    $d2['goldnum'] = coin($cz2['goldnum'] + $ca['goldnum'] * $this->site['award2'] / 100);
                    $award[] = $mo->table('t_chong_zhi')->save($d2);

                    $user3 = $this->User->where('inviturl=\'' . $user2['invit'] . '\'')->find();
                    if (chkStr($user3['invit'])) {
                        $cz3 = $this->ChongZhi->where('userid=' . $user3['id'] . ' and typeid=' . $ca['typeid'])->find();

                        $d3['id'] = $cz3['id'];
                        $d3['goldnum'] = coin($cz3['goldnum'] + $ca['goldnum'] * $this->site['award3'] / 100);
                        $award[] = $mo->table('t_chong_zhi')->save($d3);
                    }
                }
            }
        }

        $f = true;
        foreach ($award as $val) {
            if (!$val) $f = false;
        }
        // echo 'rs='.$rs.'rs1='.$rs1.'f='.$f;
        if ($rs && $rs1 && $f) {


            //如果上面的信息都更新成功了，那么就将 orderno  注册
            $ordernoFlag['userid']= $_SESSION['USER_KEY_ID'];
            $ordernoFlag['orderno']= $orderNo;
            $ordernoFlag['addtime']=time();
            $rsOrdernoFlag = D('OrdernoFlag')->add($ordernoFlag);

            if($rsOrdernoFlag)
                  $mo->commit();
            return true;
        } else {
            $mo->rollback();
            return false;
        }
    }
}
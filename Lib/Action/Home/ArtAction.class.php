<?php
class ArtAction extends CommonAction {
    private $Art;

	public function __construct(){
		parent::__construct();
	    $this->Art = D('Art');
	}

    public function index(){
        
		$id = $_GET['id'];
		$cate = $_GET['cate'];

		if(chkNum($id))
            $where = 'id='.$id;
		elseif(chkStr($cate))
            $where = 'cate="'.$cate.'"';
		else
			$where = 'cate="help"';

		$rs = $this->Art->where($where)->order('id asc')->find();
        $this->assign($rs);
        $list = $this->arts[$rs['cate']];
		$this->assign('list',$list);

 		$this->display('./Tpl/Home/art.html');
    }

}
<?php
class ChongZhiLogAction extends CommonAction {
    private $ChongZhiLog;

	public function __construct(){
		parent::__construct();
		$this->checkAuth();
	    $this->ChongZhiLog = D('ChongZhiLog');
	}

    public function index(){
        $per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->ChongZhiLog->where('userid='.$_SESSION['USER_KEY_ID'])->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

        $list=$this->ChongZhiLog->join('t_type t on t.id=t_chong_zhi_log.typeid')->field('t_chong_zhi_log.*,t.name')->where('t_chong_zhi_log.userid='.$_SESSION['USER_KEY_ID'])->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();

        $this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('module','list');

		$this->display('./Tpl/Home/user_chongzhilog.html');
    }
}
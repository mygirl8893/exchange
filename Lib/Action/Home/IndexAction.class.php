<?php
class IndexAction extends CommonAction {
	private $Type;
	private $TypeBox;
	private $User;
	private $GuaDan;
	private $TransLog;
	private $ChongZhi;
	private $Ad;
    private $MO;

	public function __construct(){
		parent::__construct();

		$this->TypeBox = D('TypeBox');
		$this->Type = D('Type');
		$this->User = D('User');
		$this->GuaDan = D('GuaDan');
		$this->TransLog = D('TransLog');
		$this->ChongZhi = D('ChongZhi');
		$this->Ad = D('Ad');
		$this->MO = new Model();
	}

    public function index(){
//echo $this->getArr('bitcoinrpc','EEsPrRQ2DHzAEpdTVQXHoQJzmVtPP8CXVK1nocxKkk5F','58.64.182.39','8333','tt');
/*
        Vendor ( 'Bitcoin.bitcoin', '', '.inc' );
		$bt=new BitcoinClient('http','abc','abc567','58.64.182.39','8839');
		echo $address = $bt->getnewaddress ('fdfdf');
*/
        $tb = $this->MO->table('t_type_box tb')->join(array('t_type t1 on t1.id=tb.name1 and t1.main=1','t_type t2 on t2.id=tb.name2'))->field('t1.nickname as goods,t2.nickname as coin,tb.*')->order('tb.id asc')->find();

		$this->assign($tb);

        $translog = $this->TransLog->where('typeboxid='.$tb['id'].' and zhu=1')->limit(15)->order('id desc')->select();
		foreach($translog as $k => $v){
		    $translog[$k]['addtime'] = date('H:i:s',strtotime($v['addtime']));
		}
        $this->assign('translog',$translog);
		$this->assign('empty','<tr><td colspan=4>没有找到数据</td></tr>');

		 $sell = $this->GuaDan->where('typeboxid='.$tb['id'].' and flag=0')->field('*,SUM(num) as nums')->group('price')->order('price asc')->limit(5)->select();
		foreach($sell as $key => $val){
		    $sell[$key]['total'] = coin($sell[$key]['price'] * $sell[$key]['nums']);
		}
        $this->assign('sells',$sell);

		$buy = $this->GuaDan->where('typeboxid='.$tb['id'].' and flag=1')->field('*,SUM(num) as nums')->group('price')->order('price desc')->limit(5)->select();
		foreach($buy as $key => $val){
		    $buy[$key]['total'] = coin($buy[$key]['price'] * $buy[$key]['nums']);
		}
        $this->assign('buys',$buy);

		//当前数据
		$gd = $this->GuaDan->where('typeboxid='.$tb['id'])->order('id desc')->limit(2)->select();
		$now['now'] = $gd[0]['price'];
		$now['addtime'] = date('Y-m-d H:i:s',time());
		if($gd[0]['price'] > $gd[1]['price']){
		    $now['flag'] = 1;
		}else{
		    $now['flag'] = 0;
		}

		$gd = $this->GuaDan->where('typeboxid='.$tb['id'])->order('price asc')->find();
		$now['low'] = $gd['price'];

		$gd = $this->GuaDan->where('typeboxid='.$tb['id'])->order('price desc')->find();
		$now['high'] = $gd['price'];

		$gd = $this->GuaDan->where('typeboxid='.$tb['id'])->field('SUM(CAST(num as FLOAT)) as volume')->find();
		$now['volume'] = $gd['volume'];

		$this->assign('market',$now);

		$ad = $this->Ad->order('sort asc,id asc')->select();
		$this->assign('ad',$ad);

		$this->display('./Tpl/Home/index.html');
    }

	function chart(){
		$id = $_GET['id'];
	   	$line = $_GET['line'];
        $time = time();
        $volmax=0;
		for($i=25; $i > 0; $i--){
		    $start = date('Y-m-d H:i:s', $time - $i * $line);
			$end = date('Y-m-d H:i:s', $time - ($i-1) * $line);

			$where = 'typeboxid='.$id.' and addtime BETWEEN "'.$start.'" AND "'.$end.'"';

			$open = $this->TransLog->where($where)->order('id asc')->find();
			$close = $this->TransLog->where($where)->order('id desc')->find();
			$high = $this->TransLog->where($where)->max('price');
			$low = $this->TransLog->where($where)->min('price');
			$volume = $this->TransLog->where($where)->sum('num');
				
			$arr[]=array(($time-($i-1)*$line)*1000,$volume,floatval($open['price']),floatval($high),floatval($low),floatval($close['price']));

			if($volume > $volmax) $volmax = $volume;
		}
		$volmax = $volmax*2;
		$this->assign('volmax',$volmax);

		//当前数据
		$gd = $this->GuaDan->where('typeboxid='.$id)->order('id desc')->limit(2)->select();
		$now['now'] = $gd[0]['price'];
		$now['addtime'] = date('Y-m-d H:i:s',time());
		if($gd[0]['price'] > $gd[1]['price']){
		    $now['flag'] = 1;
		}else{
		    $now['flag'] = 0;
		}

		$gd = $this->GuaDan->where('typeboxid='.$id)->order('price asc')->find();
		$now['low'] = $gd['price'];

		$gd = $this->GuaDan->where('typeboxid='.$id)->order('price desc')->find();
		$now['high'] = $gd['price'];

		$now['volume'] = $this->GuaDan->where('typeboxid='.$id)->sum('num');

		$this->assign('market',$now);

		$translog = $this->TransLog->where('typeboxid='.$id.' and zhu=1 and to_days(addtime)=to_days(now())')->limit(16)->order('id desc')->select();
		foreach($translog as $k => $v){
		    $translog[$k]['addtime'] = date('H:i:s',strtotime($v['addtime']));
		}
        $this->assign('translog',$translog);
		$this->assign('empty','<tr><td colspan=4>没有找到数据</td></tr>');

		$tt =  $this->TypeBox->join('t_type t on t.id=t_type_box.name1')->field('t.sellmin,t.sellmax')->where('t_type_box.id='.$id)->find();

		$this->assign('sellmin',$tt['sellmin']);
		$this->assign('sellmax',$tt['sellmax']);

		$this->assign('chart',json_encode($arr));
		$this->assign('goods',$_GET['goods']);
		$this->assign('coin',$_GET['coin']);
		$this->assign('id',$id);
		$this->assign('line',$line);
		$this->display('./Tpl/Home/chart1.html');
	}

	    public function test(){

			$Coin = A('Home/Coin');
			//echo $Coin->getAddr('http','abc','abc567','58.64.182.39','8839','test');

			$arr = $Coin->getData('http','abc','abc567','58.64.182.39','8839','test');

			print_r($arr);
            /*
			Vendor ( 'Bitcoin.bitcoin', '', '.inc' );
			$bt=new BitcoinClient('http','abc','abc567','58.64.182.39','8839');
			echo $address = $bt->getnewaddress ('fdfdf');
			*/
		}
}
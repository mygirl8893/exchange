<?php
// 本类由系统自动生成，仅供测试用途
class CoinAction extends Action {

	public function __construct(){
		parent::__construct();
		//$this->checkAuth();
	}

    public function getAddr($scheme='http',$uname,$upass,$host,$port='8839',$by){
		Vendor('Bitcoin.bitcoin','','.inc');
		$Bitcoin = new BitcoinClient ($scheme,$uname,$upass,$host,$port);
		return $Bitcoin->getnewaddress($by);
	}

    public function getData($scheme='http',$uname,$upass,$host,$port='8839',$by){
		Vendor('Bitcoin.bitcoin','','.inc');
		$Bitcoin = new BitcoinClient ($scheme,$uname,$upass,$host,$port);
		return $Bitcoin->listtransactions($by);
	}
}
<?php
class TransLogAction extends CommonAction {
    private $TransLog;

	public function __construct(){
		parent::__construct();
		$this->checkAuth();
	    $this->TransLog = D('TransLog');
	}

    public function index(){
        $per_num = 10;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TransLog->where('userid='.$_SESSION['USER_KEY_ID'])->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

        $list=$this->TransLog->join('t_type_box tb on tb.id=t_trans_log.typeboxid')->join('t_type t on t.id=tb.name1')->join('t_type t1 on t1.id=tb.name2')->field('t_trans_log.*,t.name as names1,t1.name as names2')->where('t_trans_log.userid='.$_SESSION['USER_KEY_ID'])->order('id desc')->limit(($page-1)*$per_num.','.$per_num)->select();

        $this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('module','list');

		$this->display('./Tpl/Home/user_translog.html');
    }

}
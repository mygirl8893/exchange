<?php
class CallAction extends CommonAction {
    private $Call;
	private $host = 'www.eusafy.com/euauth';
	//private $skey = 'eba9dcf865a135f58ec56eed937b342681cb8dd1';
	//private $ikey = '1qt2fx63OoM306684Xuy';
	private $skey;
	private $ikey;

	public function __construct(){
		parent::__construct();
	    //$this->Call = D('Call');

        $this->skey = $this->sysv['skey'];
		$this->ikey = $this->sysv['ikey'];
	}

    public function request($uri, $method, $params){
        $url = "https://" . $this->host . $uri;
		//echo $url; //zhy test
		if ($method == "GET") {
			if ($params != NULL) {
				$url .= "?";
				foreach($params as $key => $value) {
					$url .= rawurlencode($key) . "=" . rawurlencode($value) . "&";
				}
				// Remove extra amperstand
				$url = substr($url, 0, -1);
			}
		}

		$rfc2822_date = date(DATE_RFC2822);
		$sig = $this->sign_request($method, $this->host, $uri, $params, $rfc2822_date);
		$ch  = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: " . $sig, "Date: " . $rfc2822_date));
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Date: " . $rfc2822_date." GMT"));

		if ($method == "POST") {
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		// 开始执行， 返回响应
		$req = curl_exec($ch);
		curl_close($ch);

		return $req;
    }

	public function canon($method, $host, $path, $params, $rfc2822_date) {
		$canon = array($rfc2822_date, strtoupper($method));
		$canon_str  =  join("\n", $canon);
		$canon_str = $canon_str . "\n";
		return $canon_str;
	}

	public function sign_request($method, $host, $path, $params, $rfc2822_date) {
		$canon = $this->canon($method, $host, $path, $params, $rfc2822_date);
		//echo "<br>  zhy test---canon---" . $canon . "<br>";
		$sig   = hash_hmac("sha1", $canon, $this->skey);
		//echo "<br>  zhy test---sig---" . $sig . "<br>";
		return "Basic " . base64_encode($this->ikey . ":" . $sig);
	}

    public function run_old($phone){
	    $params = array(
			"phone"   => $phone,
			"message" => "The pin is: <pin>"
		);

		//echo "Start to call: " . $params['phone'] . "\n";
		$result = $this->request("/verify/v1/call", "POST", $params);
		//echo '======' . $result . '======';//zhy test
		//解析json
		$res_obj = json_decode($result, TRUE);

		if ($res_obj["stat"] != "OK") {
			echo "error: " . $res_obj["message"];
			exit();
		}

		$pin = $res_obj["response"]["pin"];
		unset($params);

        $_SESSION['pin'] = $pin;
		$_SESSION['phone'] = $phone;

		echo 1;
/*
		// Check the call status
		$params = array(
			"txid" => $res_obj["response"]["txid"]
		);

		$status = "";
		$tries = 5;

		// Poll the call status from server
		while ($status != "ended" && $tries > 0) {
			$stat = request("/verify/v1/status", "GET", $params);
			$stat = json_decode($stat, TRUE);
			if ($stat['stat'] == "OK") {
				echo $stat["response"]["info"] . "\n";

				$status = $stat["response"]["state"];
				$tries--;
			}
			else {
				echo "Something went wrong.  Try again.";
				exit();
			}
		}

		echo "Please enter the pin:\n";
		$input = trim(fgets(STDIN));

		if ($input == $pin) {
			echo "Pin is correct\n";
		}
		else {
			echo "Pin is incorrect\n";
		}*/
	}
	
	public function run($phone){
		$pin = rand(100000,999999);
		$content = urlencode(iconv("GB2312","UTF-8","【中元币平台】您的验证码是")).$pin;
		$_SESSION['pin'] = $pin;
		$_SESSION['phone'] = $phone;
		$url = "http://api.smsbao.com/sms?u=anda1212&p=".md5("198510")."&m=".$phone."&c=".$content;
		$this->getHtml($url);
		echo 1;
	}
	private function getHtml($url,$data = null){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		if (!empty($data)){
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		curl_close($curl);
		return $output;
	}
}
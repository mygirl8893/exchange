<?php
class HallAction extends CommonAction {
	private $Type;
	private $TypeBox;
	private $User;
	private $GuaDan;
	private $TransLog;
	private $ChongZhi;
	private $MO;

	public function __construct(){
		parent::__construct();
		$this->checkAuth();

		$this->TypeBox = D('TypeBox');
		$this->Type = D('Type');
		$this->User = D('User');
		$this->GuaDan = D('GuaDan');
		$this->TransLog = D('TransLog');
		$this->ChongZhi = D('ChongZhi');
		$this->MO = new Model();
	}

    public function index(){
        $UserMoney=$this->getUserMoney();
		$this->assign('UserMoney',$UserMoney[0]);
		$tb = $this->MO->table('t_type_box tb')->join(array('t_type t1 on t1.id=tb.name1','t_type t2 on t2.id=tb.name2'))->field('t1.nickname as goods,t2.nickname as coin,tb.*')->order('tb.id asc')->select();

        foreach($tb as $k => $v){
		    $ch = $this->GuaDan->where('typeboxid='.$v['id'])->order('id desc')->limit(2)->select();
			$tb[$k]['ch']=$ch[0]['price']>$ch[1]['price']?'↑':'↓';
			$tb[$k]['price']=$ch[0]['price'];
		}

		$this->assign('tb',$tb);

        $typeboxid = trim($_GET['id']);
		if(chkNum($typeboxid)){
		    $value = $this->MO->table('t_type_box tb')->join(array('t_type t1 on t1.id=tb.name1','t_type t2 on t2.id=tb.name2'))->field('t1.nickname as goods,t1.sellmin,t1.sellmax,t2.nickname as coin,tb.*')->where('tb.id='.$typeboxid)->find();
		}else{
			$value = $this->MO->table('t_type_box tb')->join(array('t_type t1 on t1.id=tb.name1','t_type t2 on t2.id=tb.name2'))->field('t1.nickname as goods,t1.sellmin,t1.sellmax,t2.nickname as coin,tb.*')->order('tb.id asc')->find();
        }

		$this->assign($value);

		//我的委托
		$myent = $this->GuaDan->where('userid='.$_SESSION['USER_KEY_ID'].' and typeboxid='.$value['id'])->order('id desc')->limit(20)->select();
		$this->assign('myent',$myent);
		$this->assign('ent_emp','<tr><td colspan=6>没有找到数据</td></tr>');

		//我的交易
		$mytrans = $this->TransLog->where('userid='.$_SESSION['USER_KEY_ID'].' and typeboxid='.$value['id'])->order('id desc')->limit(20)->select();
		$this->assign('mytrans',$mytrans);
		$this->assign('trans_emp','<tr><td colspan=5>没有找到数据</td></tr>');

		$translogs = $this->TransLog->where('zhu=1 and typeboxid='.$value['id'])->order('id desc')->limit(20)->select();
		$this->assign('translogs',$translogs);
		$this->assign('translog_emp','<tr><td colspan=5>没有找到数据</td></tr>');
        
        $sell = $this->GuaDan->where('typeboxid='.$value['id'].' and flag=0')->field('*,SUM(num) as nums')->group('price')->order('price asc')->limit(10)->select();
		foreach($sell as $key => $val){
		    $sell[$key]['total'] = coin($sell[$key]['price'] * $sell[$key]['nums']);
		}
        $this->assign('sells',$sell);

		$buy = $this->GuaDan->where('typeboxid='.$value['id'].' and flag=1')->field('*,SUM(num) as nums')->group('price')->order('price desc')->limit(10)->select();
		foreach($buy as $key => $val){
		    $buy[$key]['total'] = coin($buy[$key]['price'] * $buy[$key]['nums']);
		}
        $this->assign('buys',$buy);
		$this->assign('empty','<tr><td colspan="3">没有找到数据！</td></tr>');

        $best_sell = $this->GuaDan->where('typeboxid='.$value['id'].' and flag=1')->max('price');
        $this->assign('maxin',$best_sell);

		$cz = $this->ChongZhi->where('typeid='.$value['name1'].' and userid='.$_SESSION['USER_KEY_ID'])->field('goldnum,gdgold')->find();

        $this->assign('sell_remain',$cz['goldnum']);
		$this->assign('sell_gdgold',$cz['gdgold']);
		$this->assign('ablenum',$cz['goldnum'] * $maxin['maxprice']);

		$best_buy = $this->GuaDan->where('typeboxid='.$value['id'].' and flag=0')->min('price');
        $this->assign('minout',$best_buy);

		$cz = $this->ChongZhi->where('typeid='.$value['name2'].' and userid='.$_SESSION['USER_KEY_ID'])->field('goldnum,gdgold')->find();

        $this->assign('buy_remain',$cz['goldnum']);
		$this->assign('buy_gdgold',$cz['gdgold']);
		$this->assign('ablenumout',$best_buy>0 ? coin($cz['goldnum'] / $best_buy):0);
       
		$this->display('./Tpl/Home/hall.html');
    }

	function ent(){
		$per_num = 20;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->GuaDan->where('userid='.$_SESSION['USER_KEY_ID'])->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

	    $list = $this->GuaDan->where('userid='.$_SESSION['USER_KEY_ID'])->limit(($page-1)*$per_num.','.$per_num)->select();

		$this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('empty','<tr><td colspan=6>没有找到数据</td></tr>');
		$this->display('./Tpl/Home/order.html');
	}

	function trans(){
		$per_num = 20;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TransLog->where('userid='.$_SESSION['USER_KEY_ID'])->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

	    $list = $this->TransLog->where('userid='.$_SESSION['USER_KEY_ID'])->limit(($page-1)*$per_num.','.$per_num)->select();

		$this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('empty','<tr><td colspan=5>没有找到数据</td></tr>');
		$this->display('./Tpl/Home/trans.html');
	}

	function alltrans(){
		$per_num = 20;
        $page = is_numeric($_GET['page']) ? $_GET['page'] : 1;
		$count = $this->TransLog->where('zhu=1')->count();
        $page_num = ceil($count/$per_num);
		if($page < 1){
		    $page = 1;
		}elseif($page > $page_num){
		    $page = $page_num;
		}

	    $list = $this->TransLog->where('zhu=1')->limit(($page-1)*$per_num.','.$per_num)->select();

		$this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('page_num',$page_num);
		$this->assign('empty','<tr><td colspan=5>没有找到数据</td></tr>');
		$this->display('./Tpl/Home/all_trans.html');
	}

	function chart(){
		$id = $_GET['id'];
	   	$line = $_GET['line'];
        $time = time();
        $volmax=0;
		for($i=25; $i > 0; $i--){
		    $start = date('Y-m-d H:i:s', $time - $i * $line);
			$end = date('Y-m-d H:i:s', $time - ($i-1) * $line);

			$where = 'typeboxid='.$id.' and addtime BETWEEN "'.$start.'" AND "'.$end.'" and zhu=1';

			$open = $this->TransLog->where($where)->order('id asc')->find();
			$close = $this->TransLog->where($where)->order('id desc')->find();
			$high = $this->TransLog->where($where)->max('price');
			$low = $this->TransLog->where($where)->min('price');
			$volume = $this->TransLog->where($where)->sum('num');
				
			$arr[]=array(($time-($i-1)*$line)*1000,$volume,floatval($open['price']),floatval($high),floatval($low),floatval($close['price']));

			if($volume > $volmax) $volmax = $volume;
		}
        $volmax = $volmax*2;
		$this->assign('volmax',$volmax);

		//当前数据
		$gd = $this->GuaDan->where('typeboxid='.$id)->order('id desc')->limit(2)->select();
		$now['now'] = $gd[0]['price'];
		$now['addtime'] = date('Y-m-d H:i:s',time());
		if($gd[0]['price'] > $gd[1]['price']){
		    $now['flag'] = 1;
		}else{
		    $now['flag'] = 0;
		}

		$gd = $this->GuaDan->where('typeboxid='.$id)->order('price asc')->find();
		$now['low'] = $gd['price'];

		$gd = $this->GuaDan->where('typeboxid='.$id)->order('price desc')->find();
		$now['high'] = $gd['price'];

		$now['volume'] = $this->TransLog->where('typeboxid='.$id.' and zhu=1')->sum('num');

		$this->assign('market',$now);

		$translog = $this->TransLog->where('typeboxid='.$id.' and zhu=1 and to_days(addtime)=to_days(now())')->limit(16)->order('id desc')->select();
		foreach($translog as $k => $v){
		    $translog[$k]['addtime'] = date('H:i:s',strtotime($v['addtime']));
		}
        $this->assign('translog',$translog);
		$this->assign('empty','<tr><td colspan=4>没有找到数据</td></tr>');

		$tt =  $this->TypeBox->join('t_type t on t.id=t_type_box.name1')->field('t.sellmin,t.sellmax')->where('t_type_box.id='.$id)->find();

		$this->assign('sellmin',$tt['sellmin']);
		$this->assign('sellmax',$tt['sellmax']);

		$this->assign('chart',json_encode($arr));
		$this->assign('goods',$_GET['goods']);
		$this->assign('coin',$_GET['coin']);
		$this->assign('id',$id);
		$this->assign('line',$line);
		$this->display('./Tpl/Home/chart.html');
	}

    private function getUserMoney(){
        $chongzhi = $this->ChongZhi->join('t_type as t on t.id=t_chong_zhi.typeid')->field('t.nickname,t_chong_zhi.*')->where('userid=' . $_SESSION['USER_KEY_ID'])->select();
        foreach ($chongzhi as $key => $val) {
            $chongzhi[$key]['total'] = $val['goldnum'] + $val['gdgold'];
            $chongzhi[$key]['total'] = round($chongzhi[$key]['total'], 8);
        }
        $UserMoney[0]=$chongzhi[0]['total'];//总人民币余额
        $UserMoney[1]=$chongzhi[0]['total'];//总虚拟币余额
        return $UserMoney;
    }
}
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;广告图管理
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
	   	<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="25%">链接地址</th><th width="50%">图片</th><th width="5%">排序</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><?php echo (($vo["url"])?($vo["url"]):'无'); ?></td><td><img src="<?php echo ($vo["img"]); ?>" width="500"/></td><td><?php echo (($vo["sort"])?($vo["sort"]):'0'); ?></td><td><a href="?s=Admin/Ad/set/id/<?php echo ($vo["id"]); ?>">修改</a> <a href="?s=Admin/Ad/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr><td colspan=4><input type="button" value="添加" class="btn" onclick="javascript:location.href='/?s=Admin/Ad/add'"></td></tr>
		</table>
	</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/Ad/update" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>地址</td><td><input type="text" name="url" value="<?php echo ($url); ?>"/></td></tr>
		<tr><td>排序</td><td><input type="text" name="sort" value="<?php echo ($sort); ?>"/></td></tr>
		<tr><td>图片</td><td><input type="file" name="img"/><img src="<?php echo ($img); ?>" width="200" height="50" style="vertical-align:middle;"></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
</div>
</body>
</html>
<script language="javascript" src="/Public/plugin/kindeditor/jquery.tools.min.js"></script>
<script language="javascript" src="/Public/plugin/kindeditor/kindeditor-min.js"></script>
<script language="javascript" src="/Public/plugin/kindeditor/zh_CN.js"></script>
<script language=javascript>
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			resizeType : 1,
			allowPreviewEmoticons : false,
			allowImageUpload : true,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'image', 'link']
		});
	});
</script>
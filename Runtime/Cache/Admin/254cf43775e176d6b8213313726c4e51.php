<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;提现申请
	</div>
	<div class="list_body">
	    <form action="?s=Admin/TiXian/delAll" method="post" id="formid">
		<table cellspacing=0 cellpadding=0 border=0>
		    <tr><td colspan=10><a href="?s=Admin/TiXian/index/status/0" <?php echo ($status==0?'class="red"':''); ?>>未处理</a>&nbsp;&nbsp;<a href="?s=Admin/TiXian/index/status/1" <?php echo ($status==1?'class="red"':''); ?>>已处理</a></td></tr>
		    <tr>
			   <th width="5%">选择</th><th width="10%">币种名称</th><th width="10%">用户名</th><th width="10%">用户ID</th><th width="10%">提现数量</th><th width="5%">手续费</th><th width="10%">实际到账</th><th width="10%">体现地址</th><th width="5%">状态</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><input type="checkbox" class="id" name="id[]" value="<?php echo ($vo["id"]); ?>"></td><td><?php echo ($vo["name"]); ?></td><td><a href="/?s=Admin/TransLog/index/userid/<?php echo ($vo["userid"]); ?>"><?php echo ($vo["username"]); ?></a></td><td><?php echo ($vo["userid"]); ?></td><td><?php echo ($vo["goldnum"]); ?></td><td><?php echo ($vo["shouxu"]); ?>%</td><td><?php echo ($vo["realnum"]); ?></td><td><?php echo ($vo["url"]); ?></td><td><?php echo ($vo['status']?'已处理':'未处理'); ?></td><td><?php if(($vo["status"]) == "0"): ?><a href="?s=Admin/TiXian/set/id/<?php echo ($vo["id"]); ?>">确认提现</a><?php endif; ?>&nbsp;&nbsp;<a href="?s=Admin/TiXian/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=10 class="page">
				    <input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 ">
					<a href="?s=Admin/TiXian/index/page/1<?php echo ($urls); ?>">首页</a>
					<a href="?s=Admin/TiXian/index/page/<?php echo ($page-1); echo ($urls); ?>">上一页</a>
					<a href="?s=Admin/TiXian/index/page/<?php echo ($page+1); echo ($urls); ?>">下一页</a>
					<a href="?s=Admin/TiXian/index/page/<?php echo ($page_num); echo ($urls); ?>">尾页</a>
                </td>
			</tr>
		</table>
		</form>
	</div>
</div>
</body>
</html>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/TiXian/add";
	});
});
</script>
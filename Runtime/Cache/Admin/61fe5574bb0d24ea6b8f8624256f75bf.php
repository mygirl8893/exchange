<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;管理员信息设置</eq>
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
			<table cellspacing=0 cellpadding=0 border=0>
				<tr>
				   <th width="25%">登录名</th><th width="50%">权限</th><th width="10%">操作</th>
				</tr>
				<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
				   <td><?php echo (($vo["username"])?($vo["username"]):'无'); ?></td><td><?php echo (($vo["catename"])?($vo["catename"]):'无'); ?></td><td><a href="?s=Admin/Admin/set/id/<?php echo ($vo["id"]); ?>">修改</a></td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
				<tr><td colspan=4><input type="button" value="添加" class="btn" onclick="javascript:location.href='/?s=Admin/Admin/add'"/></td></tr>
			</table>
		</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/Admin/update" method="post">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>权限</td><td>
            <select name="cate">
			    <option value="0" <?php if(($cate) == "0"): ?>selected<?php endif; ?>>超级管理员</option>
				<option value="1" <?php if(($cate) == "1"): ?>selected<?php endif; ?>>文章管理员</option>
				<option value="2" <?php if(($cate) == "2"): ?>selected<?php endif; ?>>币种管理员</option>
			</select>
        </td></tr>
		<tr><td>登录名</td><td><input type="text" name="username" value="<?php echo ($username); ?>"/></td></tr>
		<tr><td>密码</td><td><input type="text" name="password"/></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
	</div><?php endif; ?>
</div>
</body>
</html>
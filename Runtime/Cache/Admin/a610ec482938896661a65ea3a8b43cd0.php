<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<style type="text/css">
.listdo a{display:block;margin:5px 0px;}
</style>
<div class="main">
	<div class="main_title">
	    <div class="search">
			<span>账号：</span><input type="text" name="key_username" id="key_username" value="<?php echo ($key_username); ?>"/>
			<span>邮箱：</span><input type="text" name="key_email" id="key_email" value="<?php echo ($key_email); ?>"/>
			<input type="button" id="s_btn" value=" 搜 索 "/>
			<script language="javascript">
			    $('#s_btn').click(function(){
					 window.location.href='/?s=Admin/User/index/key_email/'+$('#key_email').val()+'/key_username/'+$('#key_username').val();
				});
			</script>
		</div>
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;会员管理
	</div>
	<?php if(($module) == "list"): ?><if condition="$module eq 'list'">
	<div class="list_body">
		<table cellspacing=0 cellpadding=0 border=0>
			<tr>
			<th>ID</th>
			<th>账号</th>
			<th>密码</th>
			<th>电子邮箱</th>
			<th>是否激活</th>
			<th>注册时间</th>
			<th>资金列表</th>
			<th>操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			<td class="listid"><?php echo (($vo["id"])?($vo["id"]):'0'); ?></td>
			<td><?php echo (($vo["username"])?($vo["username"]):'无'); ?></td>
			<td><?php echo (($vo["pwdshow"])?($vo["pwdshow"]):'无'); ?></td>
			<td><?php echo (($vo["email"])?($vo["email"]):'无'); ?></td>
			<td class="listis"><?php if(($vo["isclose"]) == "1"): ?>激活<?php else: ?>未激活<?php endif; ?></td>
			<td class="listtime"><?php echo (($vo["addtime"])?($vo["addtime"]):'无'); ?></td>
			<td class="listtime">
			    <table cellspacing=0 cellpadding=0 border=0>
				<tr>
				<th>币种名称</th><th>当前余额</th><th>挂单数量</th><th>充值地址</th>
				</tr>
                <?php if(is_array($vo["types"])): $i = 0; $__LIST__ = $vo["types"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?><tr>
				<td><?php echo ($vo1["name"]); ?></td><td><?php echo (($vo1["goldnum"])?($vo1["goldnum"]):'0.000000'); ?></td><td><?php echo (($vo1["gdgold"])?($vo1["gdgold"]):'0.000000'); ?></td><td><?php echo (($vo1["url"])?($vo1["url"]):'无'); ?></td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
				</table>
            </td>
			<td class="listdo">
			<a href="/?s=Admin/CzApply/index/userid/<?php echo (($vo["id"])?($vo["id"]):'0'); ?>">充值记录</a>
			<a href="/?s=Admin/TransLog/index/userid/<?php echo (($vo["id"])?($vo["id"]):'0'); ?>">交易记录</a>
			<a href="/?s=Admin/User/chongzhi/id/<?php echo (($vo["id"])?($vo["id"]):'0'); ?>">充值</a>
			<a href="?s=Admin/User/set/id/<?php echo ($vo["id"]); ?>">修改</a>
			<a href="?s=Admin/User/del/id/<?php echo (($vo["id"])?($vo["id"]):'0'); ?>">删除</a>
			</td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
		</table>
		<div class="page">
			<a href="/?s=Admin/User/index/page/1<?php echo ($url_param); ?>">首页</a>
			<a href="/?s=Admin/User/index/page/<?php echo ($page-1); echo ($url_param); ?>">上一页</a>
			<a href="/?s=Admin/User/index/page/<?php echo ($page+1); echo ($url_param); ?>">下一页</a>
			<a href="/?s=Admin/User/index/page/<?php echo ($endpage); echo ($url_param); ?>">尾页</a>
			<span>第 [<?php echo ($page); ?>] 页 / 共 [<?php echo ($pagenum); ?>] 页; 共 [<?php echo ($count); ?>] 条记录</span>
		</div>
	</div>
	<?php elseif($module == 'chongzhi'): ?>
	<div class="main_body">
		<form action="?s=Admin/User/chongzhido" method="post">
		<input type="hidden" name="userid" value="<?php echo (($id)?($id):'0'); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>账号</td><td><?php echo ($username); ?></td></tr>
		<tr><td>数量</td><td><input type="text" name="goldnum"/></td></tr>
		<tr><td>币种</td><td>
		<select name="typeid">
		    <?php if(is_array($types)): $i = 0; $__LIST__ = $types;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		</td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
	</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/User/update" method="post">
		<input type="hidden" name="id" value="<?php echo (($id)?($id):'0'); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>账号</td><td><input type="text" name="username" value="<?php echo ($username); ?>"/></td></tr>
		<tr><td>邮箱</td><td><input type="text" name="email" value="<?php echo ($email); ?>"/></td></tr>
		<tr><td>密码</td><td><input type="text" name="password" value="<?php echo ($pwdshow); ?>"/></td></tr>
		<!--tr>
		    <td>是否关闭</td>
			<td>
				<input type="radio" name="isclose" value="1" <?php if(($isclose) == "1"): ?>checked<?php endif; ?>/>关闭
				<input type="radio" name="isclose" value="0" <?php if(($isclose) == "0"): ?>checked<?php endif; ?> <?php if(empty($isclose)): ?>checked<?php endif; ?>/>启用
			</td>
		</tr-->
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
	</div><?php endif; ?>
</div>
</body>
</html>
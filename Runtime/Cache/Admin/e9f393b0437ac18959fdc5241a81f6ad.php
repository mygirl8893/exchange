<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;文章管理
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
	    <form action="?s=Admin/Art/delAll" method="post" id="formid">
	   	<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="5%">选择</th><th>标题</th><th width="10%">分类</th><th width="20%">时间</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><input type="checkbox" class="id" name="id[]" value="<?php echo ($vo["id"]); ?>"></td><td><?php echo (($vo["title"])?($vo["title"]):'无'); ?></td><td><?php echo (($vo["catename"])?($vo["catename"]):'无'); ?></td><td><?php echo (($vo["addtime"])?($vo["addtime"]):'无'); ?></td><td><a href="?s=Admin/Art/set/id/<?php echo ($vo["id"]); ?>">修改</a> <a href="?s=Admin/Art/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=5 class="page">
				    <input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 ">
					<input type="button" id="add" value=" 添加新文章 ">
					<a href="?s=Admin/Art/index/page/1">首页</a>
					<a href="?s=Admin/Art/index/page/<?php echo ($page-1); ?>">上一页</a>
					<a href="?s=Admin/Art/index/page/<?php echo ($page+1); ?>">下一页</a>
					<a href="?s=Admin/Art/index/page/<?php echo ($page_num); ?>">尾页</a>
                </td>
			</tr>
		</table>
	</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/Art/update" method="post">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>标题</td><td><input type="text" name="title" value="<?php echo ($title); ?>"/></td></tr>
		<tr><td>分类</td>
			<td>
				<select name="cate">
				    <option value="serv">客户服务</option>
					<option value="about">关于我们</option>
					<option value="mserv">服务条款</option>
					<option value="warn">风险提示</option>
					<option value="help">一般问题</option>
					<option value="news">网站公告</option>
					<option value="reg">注册协议</option>
					<option value="fact">币工厂说明</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>内容</td>
			<td>
				<textarea name="content" cols=100 rows=20><?php echo ($content); ?></textarea>
			</td>
		</tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
</div>
</body>
</html>
<script language="javascript" src="/Public/plugin/kindeditor/jquery.tools.min.js"></script>
<script language="javascript" src="/Public/plugin/kindeditor/kindeditor-min.js"></script>
<script language="javascript" src="/Public/plugin/kindeditor/zh_CN.js"></script>
<script language=javascript>
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			resizeArt : 1,
			allowPreviewEmoticons : false,
			allowImageUpload : true,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'image', 'link']
		});
	});
</script>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/Art/add";
	});


});
</script>
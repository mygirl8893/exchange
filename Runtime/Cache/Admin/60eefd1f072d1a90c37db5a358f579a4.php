<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;充值地址设置
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
	    <form action="?s=Admin/ChongZhiUrl/delAll" method="post" id="formid">
		<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="10%">会员ID</th><th width="75%">充值地址</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><?php echo ($vo["userid"]); ?></td>
			   <td>
			      <table cellspacing=0 cellpadding=0 border=0>
                  <?php if(is_array($vo["types"])): $i = 0; $__LIST__ = $vo["types"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?><tr><td width="20%"><?php echo ($vo1["typename"]); ?></td><td width="70%"><?php echo (($vo1["url"])?($vo1["url"]):'暂无地址'); ?></td><td width="10%"><?php if(($vo1["id"]) > "0"): ?><a href="?s=Admin/ChongZhiUrl/set/id/<?php echo ($vo1["id"]); ?>/userid/<?php echo ($vo1["userid"]); ?>/typename/<?php echo ($vo1["typename"]); ?>">修改</a><?php endif; if(($vo1["id"]) == "0"): ?><a href="?s=Admin/ChongZhiUrl/addone/userid/<?php echo ($vo1["userid"]); ?>/typeid/<?php echo ($vo1["typeid"]); ?>">添加</a><?php endif; ?></td></tr><?php endforeach; endif; else: echo "" ;endif; ?>
				  </table>
               </td><td><a href="?s=Admin/ChongZhiUrl/del/userid/<?php echo ($vo["userid"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=4 class="page">
				    <!--input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 "-->
					<input type="button" id="add" value=" 添加充值地址 ">
					<a href="?s=Admin/ChongZhiUrl/index/page/1">首页</a>
					<a href="?s=Admin/ChongZhiUrl/index/page/<?php echo ($page-1); ?>">上一页</a>
					<a href="?s=Admin/ChongZhiUrl/index/page/<?php echo ($page+1); ?>">下一页</a>
					<a href="?s=Admin/ChongZhiUrl/index/page/<?php echo ($page_num); ?>">尾页</a>
                </td>
			</tr>
		</table>
		</form>
	</div><?php endif; ?>
	<?php if(($module) == "set"): ?><div class="main_body">
		<form action="?s=Admin/ChongZhiUrl/update" method="post">
		<input type="hidden" name="module" value="<?php echo ($module); ?>"/>
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>会员ID</td><td><?php echo ($userid); ?></td></tr>
		<tr><td>币种名</td><td><?php echo ($typename); ?></td></tr>
		<tr><td>充值地址</td><td><input type="text" name="url" value="<?php echo ($url); ?>" size="50"/></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
	<?php if(($module) == "add"): ?><div class="main_body">
		<form action="?s=Admin/ChongZhiUrl/update" method="post">
		<input type="hidden" name="module" value="<?php echo ($module); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr>
			<td>币种</td>
			<td>
                <select name="typeid">
				    <?php if(is_array($types)): $i = 0; $__LIST__ = $types;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>地址</td>
			<td><textarea name="url" cols=100 rows=20></textarea></td>
		</tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
	<?php if(($module) == "addone"): ?><div class="main_body">
		<form action="?s=Admin/ChongZhiUrl/update" method="post">
		<input type="hidden" name="module" value="<?php echo ($module); ?>"/>
		<input type="hidden" name="userid" value="<?php echo ($userid); ?>"/>
		<input type="hidden" name="typeid" value="<?php echo ($typeid); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>会员ID</td><td><?php echo ($userid); ?></td></tr>
		<tr><td>币种名</td><td><?php echo ($nickname); ?></td></tr>
		<tr><td>充值地址</td><td><input type="text" name="url" size="50"/></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
</div>
</body>
</html>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/ChongZhiUrl/add";
	});
});
</script>
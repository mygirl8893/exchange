<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;充值管理
	</div>
	<div class="list_body">
		<table cellspacing=0 cellpadding=0 border=0>
		    <tr><td colspan=10><a href="?s=Admin/CzApply/index/status/0<?php echo ($url); ?>" <?php echo ($status==0?'class="red"':''); ?>>未处理</a>&nbsp;&nbsp;<a href="?s=Admin/CzApply/index/status/1<?php echo ($url); ?>" <?php echo ($status==1?'class="red"':''); ?>>已处理</a></td></tr>
		    <tr>
			   <th width="10%">币种名称</th><th width="10%">用户名</th><th width="10%">用户ID</th><th width="10%">充值金额</th><th width="10%">充值地址</th><th width="5%">状态</th><th width="10%">时间</th><th width="7%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><?php echo (($vo["nickname"])?($vo["nickname"]):'RMB'); ?></td><td><?php echo (($vo["username"])?($vo["username"]):'RMB'); ?></td><td><?php echo (($vo["userid"])?($vo["userid"]):'0'); ?></td><td><?php echo (($vo["goldnum"])?($vo["goldnum"]):'0'); ?></td><td><?php echo (($vo["url"])?($vo["url"]):'RMB'); ?></td><td><?php echo ($vo['status']?'已处理':'正在处理'); ?></td><td><?php echo (($vo["addtime"])?($vo["addtime"]):'RMB'); ?></td><td><?php if(($vo["status"]) == "0"): ?><a href="?s=Admin/CzApply/set/id/<?php echo ($vo["id"]); ?>">确认充值</a><?php else: ?>已确认<?php endif; ?></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=10 class="page">
					<a href="?s=Admin/CzApply/index/page/1<?php echo ($urls); ?>">首页</a>
					<a href="?s=Admin/CzApply/index/page/<?php echo ($page-1); echo ($urls); ?>">上一页</a>
					<a href="?s=Admin/CzApply/index/page/<?php echo ($page+1); echo ($urls); ?>">下一页</a>
					<a href="?s=Admin/CzApply/index/page/<?php echo ($page_num); echo ($urls); ?>">尾页</a>
                </td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>
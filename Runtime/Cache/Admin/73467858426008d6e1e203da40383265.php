<?php if (!defined('THINK_PATH')) exit();?>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;网站设置
	</div>
	<div class="main_body">
		<form action="?s=Admin/Sys/update" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<input type="hidden" name="logoimg" value="<?php echo ($logo); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>网站LOGO</td><td><input type="file" name="logo"/><img src="<?php echo ($logo); ?>" height="20"></td></tr>
		<tr><td>网站标题</td><td><input type="text" name="title" value="<?php echo ($title); ?>"/></td></tr>
		<tr><td>网站关键字</td><td><input type="text" name="keyword" value="<?php echo ($keyword); ?>"/></td></tr>
		<tr><td>网站描述</td><td><textarea name="description" cols=50 rows=5 class="not_edit"><?php echo ($description); ?></textarea></td></tr>
		<tr><td>版权信息</td><td><textarea name="copyright" cols=50 rows=5><?php echo ($copyright); ?></textarea></td></tr>
		<tr><td>统计代码</td><td><textarea name="tongji" cols=50 rows=5><?php echo ($tongji); ?></textarea></td></tr>
		<tr><td>邮箱服务器</td><td><input type="text" name="smtp" value="<?php echo ($smtp); ?>"/></td></tr>
        <tr><td>邮箱地址</td><td><input type="text" name="email" value="<?php echo ($email); ?>"/></td></tr>
		<tr><td>邮箱密码</td><td><input type="password" name="pwd" value="<?php echo ($pwd); ?>"/></td></tr>
		<tr><td>发件人</td><td><input type="text" name="auth" value="<?php echo ($auth); ?>"/></td></tr>
		<tr><td>提现手续费</td><td><input type="text" name="txfee" value="<?php echo ($txfee); ?>"/> %</td></tr>
		<tr><td>推荐奖（一层）</td><td><input type="text" name="award1" value="<?php echo ($award1); ?>"/> %</td></tr>
		<tr><td>推荐奖（二层）</td><td><input type="text" name="award2" value="<?php echo ($award2); ?>"/> %</td></tr>
		<tr><td>推荐奖（三层）</td><td><input type="text" name="award3" value="<?php echo ($award3); ?>"/> %</td></tr>
		<tr><td>客服QQ</td><td><textarea name="qq" cols=50 rows=5><?php echo ($qq); ?></textarea></td></tr>
		<tr><td>官方群</td><td><textarea name="qun" cols=50 rows=5><?php echo ($qun); ?></textarea></td></tr>
		<tr><td>推广送币</td><td><input type="text" name="extcoin" value="<?php echo ($extcoin); ?>"/></td></tr>

        <tr><td>买  入</td><td><input type="checkbox" class="id" name="buyflag" value="1"
            <?php if($buyflag): ?>checked="checked"<?php endif; ?>  ></td></tr>
        <tr><td>卖  出</td><td><input type="checkbox" class="id" name="saleflag" value="1"
            <?php if($saleflag): ?>checked="checked"<?php endif; ?>  ></td></tr>

        <tr><td>只允许人民币提现</td><td><input type="checkbox" class="id" name="txtype" value="1"
            <?php if($txtype): ?>checked="checked"<?php endif; ?>  ></td></tr>

        <tr><td>开启回购</td><td><input type="checkbox" class="id" name="huigouflag" value="1"
                <?php if($huigouflag): ?>checked="checked"<?php endif; ?>  ></td></tr>

       <tr>
           <td>提现允许的银行</td>
           <td>中国银行<input type="checkbox" class="id" name="bank_zgyh" value="1"
           <?php if($bank_zgyh): ?>checked="checked"<?php endif; ?>  >
           中国农业银行<input type="checkbox" class="id" name="bank_zgnyyh" value="1"
               <?php if($bank_zgnyyh): ?>checked="checked"<?php endif; ?>  >
           中国工商银行<input type="checkbox" class="id" name="bank_zggsyh" value="1"
               <?php if($bank_zggsyh): ?>checked="checked"<?php endif; ?>  >
           建设银行<input type="checkbox" class="id" name="bank_zgjsyh" value="1"
               <?php if($bank_zgjsyh): ?>checked="checked"<?php endif; ?>  ></td>
       </tr>

        <tr>
            <td>充值接口开关</td>
            <td>支付宝充值<input type="checkbox" class="id" name="pay_zhifubao" value="1"
                <?php if($pay_zhifubao): ?>checked="checked"<?php endif; ?>  >
                财付通充值<input type="checkbox" class="id" name="pay_caifutong" value="1"
                <?php if($pay_caifutong): ?>checked="checked"<?php endif; ?>  >
                易宝充值<input type="checkbox" class="id" name="pay_yibao" value="1"
                <?php if($pay_yibao): ?>checked="checked"<?php endif; ?>  >
                 网银充值<input type="checkbox" class="id" name="pay_wangyin" value="1"
                <?php if($pay_wangyin): ?>checked="checked"<?php endif; ?>  ></td>
        </tr>


        <tr><td>单次充值下限</td><td><input type="text" name="chongzhid" value="<?php echo ($chongzhid); ?>"/> 元</td></tr>
        <tr><td>单次充值上限</td><td><input type="text" name="chongzhiu" value="<?php echo ($chongzhiu); ?>"/> 元</td></tr>

        <tr><td>提现下限</td><td><input type="text" name="tixiand" value="<?php echo ($tixiand); ?>"/> 元</td></tr>
        <tr><td>提现上限</td><td><input type="text" name="tixianu" value="<?php echo ($tixianu); ?>"/> 元</td></tr>

        <tr><td>回购下限</td><td><input type="text" name="huigoud" value="<?php echo ($huigoud); ?>"/>   个<td></tr>
        <tr><td>回购上限</td><td><input type="text" name="huigouu" value="<?php echo ($huigouu); ?>"/>   个<td></tr>

		<tr><td>解冻时间</td><td><input type="text" name="exttime" value="<?php echo ($exttime); ?>"/> 天</td></tr>
		<tr><td>解冻数量</td><td><input type="text" name="extper" value="<?php echo ($extper); ?>"/> %</td></tr>
		<tr><td>注册有效期</td><td><input type="text" name="wait" value="<?php echo ($wait); ?>"/> 天</td></tr>
		<tr><td>支付宝</td><td><input type="text" name="alipay" value="<?php echo ($alipay); ?>"/></td></tr>
		<tr><td>支付宝链接</td><td><input type="text" name="alipaylink" value="<?php echo ($alipaylink); ?>"/></td></tr>
		<tr><td>ikey [KEY]</td><td><input type="password" name="ikey" value="<?php echo ($ikey); ?>"/></td></tr>
		<tr><td>skey [Md5Key]</td><td><input type="password" name="skey" value="<?php echo ($skey); ?>"/></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
	</div>
</div>
</body>
</html>
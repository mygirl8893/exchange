<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;中元币回购
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
	    <form action="?s=Admin/Hg/delAll" method="post" id="formid">
	   	<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="5%">选择</th><th width="10%">会员名</th><th width="10%">回购数量</th><th width="20%">回购价格</th><th width="20%">回购时间</th><th width="10%">状态</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><input type="checkbox" class="id" name="id[]" value="<?php echo ($vo["id"]); ?>"></td><td><?php echo (($vo["username"])?($vo["username"]):'0'); ?></td>
                <td><?php echo (($vo["num"])?($vo["num"]):'0'); ?></td><td><?php echo (($vo["price"])?($vo["price"]):'0'); ?></td>
                <td><?php echo (($vo["addtime"])?($vo["addtime"]):'无'); ?></td><td><?php echo ($vo["status"]); ?></td>
                <td><a href="?s=Admin/Hg/set/id/<?php echo ($vo["id"]); ?>">修改</a>
                    <?php if($vo["status"] == '未回购'): ?><a href="?s=Admin/Hg/apply/id/<?php echo ($vo["id"]); ?>">确认回购</a><?php endif; ?>

                    <a href="?s=Admin/Hg/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=7 class="page">
				    <input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 ">
					<!--input type="button" id="add" value=" 添加 "-->
					<a href="?s=Admin/Hg/index/page/1">首页</a>
					<a href="?s=Admin/Hg/index/page/<?php echo ($page-1); ?>">上一页</a>
					<a href="?s=Admin/Hg/index/page/<?php echo ($page+1); ?>">下一页</a>
					<a href="?s=Admin/Hg/index/page/<?php echo ($page_num); ?>">尾页</a>
                </td>
			</tr>
		</table>
	</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/Hg/update" method="post">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<input type="hidden" name="issueid" value="<?php echo ($issueid); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>回购价格</td><td><input type="text" name="price" readonly value="<?php echo (($price)?($price):0); ?>"/></td></tr>
		<tr><td>限购数量</td><td><input type="text" name="limit" readonly value="<?php echo (($limit)?($limit):0); ?>"/></td></tr>
		<tr><td>回购数量</td><td><input type="text" name="num" value="<?php echo (($num)?($num):0); ?>"/></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
</div>
</body>
</html>
<script language="javascript" src="/Public/plugin/kindeditor/jquery.tools.min.js"></script>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/Hg/add";
	});
});
</script>
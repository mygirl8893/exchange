<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;币种管理
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
	    <form action="?s=Admin/Type/delAll" method="post" id="formid">
		<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="5%">选择</th><th width="10%">币种名称</th><th width="10%">币种简称</th><th width="5%">排序</th><th width="5%">卖出上限</th><th width="5%">卖出下限</th><th width="5%">买入下限</th><th width="5%">涨停幅度</th><th width="5%">跌停幅度</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><input type="checkbox" class="id" name="id[]" value="<?php echo ($vo["id"]); ?>"></td><td><?php echo ($vo["name"]); ?></td><td><?php echo ($vo["nickname"]); ?></td><td><?php echo ($vo["sort"]); ?></td><td><?php echo ($vo["sellmax"]); ?></td><td><?php echo ($vo["sellmin"]); ?></td><td><?php echo ($vo["buymin"]); ?></td><td><?php echo ($vo["up"]); ?> %</td><td><?php echo ($vo["down"]); ?> %</td><td><a href="?s=Admin/Type/set/id/<?php echo ($vo["id"]); ?>">修改</a>&nbsp;&nbsp;<a href="?s=Admin/Type/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=10 class="page">
				    <input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 ">
					<input type="button" id="add" value=" 添加新币种 ">
					<a href="?s=Admin/Type/index/page/1">首页</a>
					<a href="?s=Admin/Type/index/page/<?php echo ($page-1); ?>">上一页</a>
					<a href="?s=Admin/Type/index/page/<?php echo ($page+1); ?>">下一页</a>
					<a href="?s=Admin/Type/index/page/<?php echo ($page_num); ?>">尾页</a>
                </td>
			</tr>
		</table>
		</form>
	</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/Type/update" method="post">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>币种名称</td><td><input type="text" name="name" value="<?php echo ($name); ?>"/></td></tr>
		<tr><td>币种简称</td><td><input type="text" name="nickname" value="<?php echo ($nickname); ?>"/></td></tr>
		<tr>
			<td>币种简介</td>
			<td>
				<textarea name="info" cols=50 rows=10><?php echo ($info); ?></textarea>
			</td>
		</tr>
		<tr><td>本站币</td><td><input type="radio" name="main" value="1" <?php if(($main) == "1"): ?>checked<?php endif; ?>/>是 <input type="radio" name="main" value="0" <?php if(($main) == "0"): ?>checked<?php endif; ?>/>否&nbsp;&nbsp;&nbsp;&nbsp;<font color=red>网站自己的币选择 “是”</font></td></tr>
		<tr><td>币种属性</td><td><input type="radio" name="yuan" value="1" <?php if(($yuan) == "1"): ?>checked<?php endif; ?>/>人民币 <input type="radio" name="yuan" value="0" <?php if(($yuan) == "0"): ?>checked<?php endif; ?>/>虚拟币</td></tr>
		<tr><td>排序</td><td><input type="text" name="sort" value="<?php echo (($sort)?($sort):0); ?>"/></td></tr>
		<tr><td>卖出上限</td><td><input type="text" name="sellmax" value="<?php echo (($sellmax)?($sellmax):0); ?>"/></td></tr>
		<tr><td>卖出下限</td><td><input type="text" name="sellmin" value="<?php echo (($sellmin)?($sellmin):0); ?>"/></td></tr>
		<tr><td>买入下限</td><td><input type="text" name="buymin" value="<?php echo (($buymin)?($buymin):0); ?>"/></td></tr>
		<tr><td>涨停幅度</td><td><input type="text" name="up" value="<?php echo (($up)?($up):0); ?>"/> %</td></tr>
		<tr><td>跌停幅度</td><td><input type="text" name="down" value="<?php echo (($down)?($down):0); ?>"/> %</td></tr>
		<tr><td>服务器地址</td><td><input type="text" name="url" value="<?php echo ($url); ?>"/></td></tr>
		<tr><td>服务器端口</td><td><input type="text" name="port" value="<?php echo ($port); ?>"/></td></tr>
		<tr><td>用户名</td><td><input type="text" name="username" value="<?php echo ($username); ?>"/></td></tr>
		<tr><td>密码</td><td><input type="text" name="password" value="<?php echo ($password); ?>"/></td></tr>
		<tr><td>分红截至时间</td><td><input type="text" name="divday" value="<?php echo (($divday)?($divday):0); ?>"/> 天</td></tr>
		<tr><td>日分红比率</td><td><input type="text" name="divrates" value="<?php echo (($divrates)?($divrates):0); ?>"/> %</td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
</div>
</body>
</html>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/Type/add";
	});


});
</script>
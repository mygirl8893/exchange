<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<script language="javascript" src="/Public/plugin/kindeditor/jquery.tools.min.js"></script>
<script language="javascript" src="/Public/plugin/kindeditor/kindeditor-min.js"></script>
<script language="javascript" src="/Public/plugin/kindeditor/zh_CN.js"></script>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;币种交易市场管理
	</div>
	<?php if(($module) == "list"): ?><div class="list_body">
	    <form action="?s=Admin/TypeBox/delAll" method="post" id="formid">
		<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="5%">选择</th><th width="60%">交易市场名称</th><th width="5%">排序</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><input type="checkbox" class="id" name="id[]" value="<?php echo ($vo["id"]); ?>"></td><td><?php echo ($vo["name1str"]); ?> / <?php echo ($vo["name2str"]); ?></td><td><?php echo ($vo["sort"]); ?></td><td><a href="?s=Admin/TypeBox/set/id/<?php echo ($vo["id"]); ?>">修改</a>&nbsp;&nbsp;<a href="?s=Admin/TypeBox/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=6 class="page">
				    <input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 ">
					<input type="button" id="add" value=" 添加交易市场 ">
					<a href="?s=Admin/TypeBox/index/page/1">首页</a>
					<a href="?s=Admin/TypeBox/index/page/<?php echo ($page-1); ?>">上一页</a>
					<a href="?s=Admin/TypeBox/index/page/<?php echo ($page+1); ?>">下一页</a>
					<a href="?s=Admin/TypeBox/index/page/<?php echo ($page_num); ?>">尾页</a>
                </td>
			</tr>
		</table>
		</form>
	</div>
	<?php else: ?>
	<div class="main_body">
		<form action="?s=Admin/TypeBox/update" method="post">
		<input type="hidden" name="id" value="<?php echo ($id); ?>"/>
		<table cellspacing=0 cellpadding=0 border=0>
		<tr><td>交易市场名称</td><td>
			<select name="name1">
			    <option value="0">币种1</option>
			    <?php if(is_array($bts)): $i = 0; $__LIST__ = $bts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" <?php if(($name1) == $vo["id"]): ?>selected<?php endif; ?>><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
			</select>
			/
			<select name="name2">
			    <option value="0">币种2</option>
			    <?php if(is_array($bts)): $i = 0; $__LIST__ = $bts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" <?php if(($name2) == $vo["id"]): ?>selected<?php endif; ?>><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
			</select>
			卖出币种1，买入币种2
		</td></tr>
		<tr>
			<td>交易市场简介</td>
			<td>
				<textarea name="info" cols=100 rows=20><?php echo ($info); ?></textarea>
			</td>
		</tr>
		<tr><td>排序</td><td><input type="text" name="sort" value="<?php echo ($sort?$sort:0); ?>"/></td></tr>
		</table>
		<div><input type="submit" value="提交"/></div>
		</form>
    </div><?php endif; ?>
</div>
</body>
</html>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/TypeBox/add";
	});
});

	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="info"]', {
			resizeType : 1,
			allowPreviewEmoticons : false,
			allowImageUpload : true,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'image', 'link']
		});
	});
</script>
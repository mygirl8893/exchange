<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/Public/Admin/css/common.css" />
<script language="javascript" src="/Public/js/jquery.js"></script>
</head>
<body>
<div class="main">
	<div class="main_title">
		<img src="/Public/Admin/images/book1.gif"/>&nbsp;&nbsp;交易记录
	</div>
	<div class="list_body">
	    <form action="?s=Admin/TransLog/delAll" method="post" id="formid">
		<table cellspacing=0 cellpadding=0 border=0>
		    <tr>
			   <th width="5%">选择</th><th width="20%">交易类型</th><th width="10%">账号</th><th width="10%">交易额</th><th width="10%">交易数量</th><th width="10%">交易总额</th><th width="10%">买卖</th><th width="20%">交易时间</th><th width="10%">操作</th>
			</tr>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
			   <td><input type="checkbox" class="id" name="id[]" value="<?php echo ($vo["id"]); ?>"></td><td><?php echo ($vo["name1"]); ?> / <?php echo ($vo["name2"]); ?></td><td><?php echo ($vo["username"]); ?></td><td><?php echo ($vo["price"]); ?></td><td><?php echo ($vo["num"]); ?></td><td><?php echo ($vo["sum"]); ?></td><td><?php echo ($vo['flag']?'买':'卖'); ?></td><td><?php echo ($vo["addtime"]); ?></td><td><a href="?s=Admin/TransLog/del/id/<?php echo ($vo["id"]); ?>">删除</a></td>
			</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			<tr>
			    <td colspan=9 class="page">
				    <input type="button" id="all" value=" 全选 ">
					<input type="button" id="all_return" value=" 全不选 ">
					<input type="button" id="dels" value=" 批量删除 ">
					<a href="?s=Admin/TransLog/index/page/1<?php echo ($urls); ?>">首页</a>
					<a href="?s=Admin/TransLog/index/page/<?php echo ($page-1); echo ($urls); ?>">上一页</a>
					<a href="?s=Admin/TransLog/index/page/<?php echo ($page+1); echo ($urls); ?>">下一页</a>
					<a href="?s=Admin/TransLog/index/page/<?php echo ($page_num); echo ($urls); ?>">尾页</a>
                </td>
			</tr>
		</table>
		</form>
	</div>
</div>
</body>
</html>
<script language="javascript">
$(document).ready(function(){
    $('#all').click(function(){
	    $('.id').attr('checked',true);
	});
	$('#all_return').click(function(){
	    $('.id').attr('checked',false);
	});
    $('#dels').click(function(){

		f = 0;
	    $('.id').each(function(){
		    if($(this).attr('checked')) f = 1;
		});
		
        if(f==0){
		    alert('请选择要删除的项！');
		}else{
		    $('#formid').submit();
		}
	});
	$('#add').click(function(){
	    location.href="?s=Admin/TransLog/add";
	});
});
</script>
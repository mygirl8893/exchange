<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Description" content="<?php echo ($sys["description"]); ?>" />
<meta name="keywords" content="<?php echo ($sys["keyword"]); ?>" />
<link rel="stylesheet" type="text/css" href="/Public/Home/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/yii.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/styles.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/public.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/jbox.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/jquery-ui.css">
<script type="text/javascript" src="/Public/Home/js/jquery.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.yiiactiveform.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.ba-bbq.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/config.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.jBox-2.3.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.jBox-zh-CN.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery-ui.js"></script>
<title><?php echo ($sys["title"]); ?></title>
<script  language="JavaScript"> 
          <!--
    //      function stopError() {
         //   return true;
  //        }
     //     window.onerror = stopError;
           -->         
</script>
</head>
<body>
<!--最顶上的内容-->
<div class="top-fixed-all">
      <div class="top-fixed">
            <div class="container">
                  <div class="top-fixed-info">
                        最新成交价:<span class="f-ff4000 BTC_RMB_rate" style=""><?php echo (($now["now"])?($now["now"]):0.07); ?></span>24小时成交币量:<span class="buy_price f-ff4000" style=""><?php echo (($now["total"])?($now["total"]):84567); ?></span>24小时成交金额量:<span class="f-7eb800 sell_price" style=""><?php echo (($now["totalprice"])?($now["totalprice"]):5849); ?></span>
                  </div>
                  <!--登录状态-->
                  <div class="top-fixed-user">
				       <?php if($login_user_id > 0 ): ?><div>
						   <div class="ll mt4 mr10">
						   </div>
						   <div class="user-msg-all">
							   <div class="f_ddd" id="user-hover" style="width: 44px;color:#ddd;"><em><?php echo ($login_user_name); ?></em><i></i></div>
							   <div class="user-msg">
								   <p><a class="mr15" href="<?php echo ($path); ?>/User">用户信息</a><a class="mr15" href="<?php echo ($path); ?>/User/detail">财务明细</a><a href="<?php echo ($path); ?>/User/chongzhi">充值</a></p>
							   </div>
						   </div>
						   <div class="ll"><a href="<?php echo ($path); ?>/Login/loginout">退出</a></div>
						   <div class="clear"></div>
					   </div>
					   <script language="javascript">
					       $('.user-msg-all').mouseover(function(){
						       $('.user-msg').css('display','block');
						   });

						   $('.user-msg-all').mouseout(function(){
						       $('.user-msg').css('display','none');
						   });
					   </script>
					   <?php else: ?>
					   <div class="unsign">
						   <div class="ll mt4 mr10">
						   </div>
						   <a href="<?php echo ($path); ?>/Login/reg">免费注册</a><a href="<?php echo ($path); ?>/Login">登录</a>
                       </div><?php endif; ?>
                  </div>
                  <div class="clear"></div>
            </div>
      </div>
</div>
<div class="mt30">
    <!--网站升级中提示-->
    <div class="top-cont-msg">   
     <div class="content-top"></div>    </div>
    <!--[if IE 6]>
    <div class="kie-bar">
      您使用的浏览器版本过低，为了您的资金安全和更好地用户体验，建议立即升级
      <a href="http://windows.microsoft.com/zh-cn/internet-explorer/download-ie" seed="kie-setup-IE8" target="_blank" >
      <i class="kie-bar-icon-ie"></i>Internet Explorer</a>
      或
      <a href="https://www.google.com/intl/zh-CN/chrome/" seed="kie-setup-IE8" target="_blank" >
      <i class="kie-bar-icon-chrome"></i>Google Chrome</a>
    </div>
    <style>
      .kie-bar {
        height: 24px;
        line-height: 1.8;
        font-weight:normal;
        text-align: center;
        border-bottom:1px solid #fce4b5;
        background-color:#FFFF9B;
        color:#e27839;
        position: relative;
        font-size: 14px;
        text-shadow: 0px 0px 1px #efefef;
        padding: 5px 0;
      }
      .kie-bar a {
        color:#08c;
        text-decoration: none;
      }
    </style>
    <![endif]--> 
    <!--头部-->
    <div class="container">
    <div class=" o_h_z" style="width:1000px;">
           <div class="logo-index"><a href="/"><img src="/<?php echo ($sys["logo"]); ?>"/></a></div>
           <!--导航-->
           <div class="nav-bar rr">
                 <ul>
                    <li class="cur"><a href="/">首页</a></li>
                    <li><a href="<?php echo ($path); ?>/Hall">交易大厅</a></li>
                    <li><a href="<?php echo ($path); ?>/User">我的账户</a></li>
					<li><a href="<?php echo ($path); ?>/User/buy">中元币认购</a></li>
                    <li><a href="<?php echo ($path); ?>/Art/index/cate/news">网站公告</a></li>  
					<li style="border-right:0px none;"><a href="<?php echo ($path); ?>/Art">一般问题</a></li>         
                 </ul>
           </div>
    </div>
    </div>
</div>
<!--交易页面的子导航区-->
<div class="subnav">
      <div class="container">
            <ul>
                 <li <?php if(($cur) == "user"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User">账户信息</a><i></i>
				 </li>
                 <li <?php if(($cur) == "detail"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/detail">财务明细</a><i></i>
				 </li>
                 <li <?php if(($cur) == "safe"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/safe">安全中心</a><i></i>
				 </li>
                 <li <?php if(($cur) == "invit"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/invit">我的邀请</a><i></i>
				 </li>
                 <li <?php if(($cur) == "showUserBank"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/showUserBank">银行卡管理</a>
				 </li>
				 <li <?php if(($cur) == "chongzhi"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/chongzhi">账户充值</a>
				 </li>
				 <li <?php if(($cur) == "tixian"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/tixian">账户提现</a>
				 </li>
				 <li <?php if(($cur) == "bank"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/bank">存款(暂停）</a>
				 </li>
				 <li <?php if(($cur) == "buy"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/buy">我的认购</a>
				 </li>
            </ul>
      </div>    
</div>

<div class="container">
    <!--全站交易记录-->
    <div class="trade-part trade-part-hd mt20">
        <div class="trade-hd">
            <h5><i class="icon_condition"></i>账户充值(第一步)</h5>
        </div>
        <div class="md">
            <div class="my-grid" id="order-grid" style="margin-bottom:20px;">
                <form action="<?php echo ($path); ?>/User/chongzhi" method="post" id="formid">
                    <input type="hidden" name="yuan" id="yuan" value="0"/>
                    <table class="items table table-striped table-bordered table-condensed chkreal">
                        <tbody>
                        <tr> <td colspan="2"><img src="./Public/Home/cz/cz1.png"></td></tr>
                        <tr> <td colspan="2"><font style="color: red;font-size: bold;line-height: 40px;font-size: 16px">
                            ◆因为国家对支付行业的相关调整,本网站采取线上申请线下打款的方式,给您带来的不便,敬请谅解!谢谢!!</font></td></tr>
                        <tr><td>充值金额：</td><td><input type="text" name="goldnum" placeholder="充值金额限定在<?php echo ($chongzhid); ?>元--<?php echo ($chongzhiu); ?>元之间"/></td>
                        <input type="hidden" name="url" value="<?php echo (($url)?($url):'无'); ?>" readonly/></tr>
                        <tr><td>验证码：</td><td><input type="text" name="code" placeholder="请输入验证码" style="width:114px;"/><img id="refresh" style="vertical-align:middle;height:30px;margin-left:5px;" onclick="document.getElementById('refresh').src='<?php echo ($path); ?>/Login/checkcode/t/'+Math.random()" src="<?php echo ($path); ?>/Login/checkcode/" title="看不清？点一下"/></td></tr>
                        <tr><td colspan=2><input type="button" id="subbtn" class="btn" value="确定"/></td></tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="my-grid" id="order-grid">
                <table class="items table table-striped table-bordered table-condensed saferoom">
                    <thead>
                    <tr><th>订单号</th><th>金额</th><th>时间</th><th width="40%">状态</th></tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr><td class="chkTit"><?php echo (($vo["orderno"])?($vo["orderno"]):'无'); ?></td><td><?php echo (($vo["goldnum"])?($vo["goldnum"]):'0'); ?></td><td><?php echo (($vo["addtime"])?($vo["addtime"]):'无'); ?></td><td><?php if(($vo["status"]) == "0"): ?>正在处理<?php else: ?>已处理<?php endif; ?></td></tr><?php endforeach; endif; else: echo "$empty" ;endif; ?>
                    </tbody>
                </table>
                <div id="page">
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
<!--尾部-->
<div class="footer-all">
     <div class="container footer-part sitelink grid-990">
           <div class="rr">
                 <div class="cont-box cont-box-first">
                       <h6>客户服务</h6>
                       <ul>
					       <?php if(is_array($serv)): $i = 0; $__LIST__ = $serv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
                 <div class="cont-box">
                       <h6>关于我们</h6>
                       <ul>
                           <?php if(is_array($about)): $i = 0; $__LIST__ = $about;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
                 <div class="cont-box">
                       <h6>服务条款</h6>
                       <ul>
                           <?php if(is_array($mserv)): $i = 0; $__LIST__ = $mserv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
				 <div class="cont-box"><img src="/Public/Home/images/weixin.png"/></div>
                 <div class="cont-box cont-box-last">
                       <h6><i></i>风险提示</h6>
                       <p><?php echo ($warn["content"]); ?></p>
                 </div>
				 <div style="clear:both"></div>
           </div>
     </div>
      <div class="t_c grid-990 sitecopyright"><?php echo ($sys["copyright"]); echo ($sys["tongji"]); ?><br/>平台支持：<a href="http://www.vike5.com" target="_blank">威客网</a> 湘ICP备10001691号
</div>
	<div class="t_c grid-990 siteauth"></div>
</div>
<div id="alert_room1"></div>
<script language="javascript" src="/Public/js/alert.js"></script>

<div class="serv-qq">
<div class="serv-tit">在线咨询</div>
<dl>
	<?php if(is_array($sys["qq"])): $i = 0; $__LIST__ = $sys["qq"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dt><?php echo ($vo["name"]); ?></dt><dd><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo ($vo["text"]); ?>&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo ($vo["text"]); ?>:41" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>
	    </dd><?php endforeach; endif; else: echo "" ;endif; ?>
</dl>
<div class="clear"></div>
</div>
<!-- footer -->

<div class="modal hide fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

<script type="text/javascript">


    $(document).ready(function(){
        $('#subbtn').click(function(){
            var goldnum = $('input[name=goldnum]').val();

            if(goldnum=="" || goldnum <= 0){
                msgDalog("请输入充值金额！","",0);
                return false;
            }else if(isNaN(goldnum)){
                msgDalog('充值金额应该是数字！');
                $('input[name=goldnum]').val('');
                return false;
            }if(goldnum>=<?php echo ($chongzhiu); ?>){
                msgDalog('充值金额不能超过<?php echo ($chongzhiu); ?>元！');
                return false;
            }if(goldnum<<?php echo ($chongzhid); ?>){
                msgDalog('充值金额不能少于<?php echo ($chongzhid); ?>元！');
                return false;
            }else if($('input[name=code]').val()==""){
                msgDalog("请输入验证码！","",0);
                return false;
            }else{
                     $('#formid').submit();

            }
        });

        getczdata(1);
    });




    function czapplyback(id){
        var loadi = layer.load('正在取消...…');
        $.ajax({
            type : "get",
            url : "<?php echo ($path); ?>/User/czapplyback/id/"+id,
            async : false,
            success : function(data){
                layer.close(loadi);
               if(data!=false){
                   $('#del'+id).remove();
               }else{
                   layer.alert('取消失败!');
               }
            }
        });
    }


    function czstatus(id){
        var loadi = layer.load('正在提交...…');
        $.ajax({
            type : "get",
            url : "<?php echo ($path); ?>/User/czConfirm/id/"+id,
            async : false,
            success : function(data){
                layer.close(loadi);
                if(data!=false){
                  $('#status'+id).html(' <font color="red">等待网站管理员审核</font>');
                }else{
                    layer.alert('提交失败!');
                }
            }
        });
    }
</script>
<script type="text/javascript">
    //显示当前类别样式
    $('#left_main_user').removeClass().addClass('tradeTitlecur');//一级
    $('#left_main_trance span').removeClass().addClass('trade1cur');
    $('#left_chongzhi').addClass('cur');//二级
	

</script>

</body></html>
<?php if (!defined('THINK_PATH')) exit();?>﻿<div class="part-r">
	<div class="trade-all trade-form-box">
		 <!--实时成交-->
		 <div class="trade-part-hd" style="height:507px;">
			   <div class="trade-hd">
					 <h6><i class="icon_timetrading"></i>实时成交</h6>
			   </div>
			   <div class="md" id="slide-show">
					 <div class="bd">
					 <table width="100%">
							<thead>
								   <tr>
									  <th>时间</th>
									  <th>类型</th>
									  <th>成交价</th>
									  <th>成交量</th>
								  </tr>
							</thead>
							<tbody id="transaction">
							<?php if(is_array($translog)): $i = 0; $__LIST__ = $translog;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr><td><?php echo ($vo["addtime"]); ?></td><td class="buy"><?php echo ($vo['flag']?'买':'卖'); ?></td><td class="buy"><?php echo ($vo["price"]); ?></td><td><?php echo ($vo["num"]); ?></td></tr><?php endforeach; endif; else: echo "$empty" ;endif; ?>
							</tbody>
					 </table>
					 </div>
			   </div>
		 </div>
	</div> 
</div>
<!--左边栏-->
<div class="part-l">
   <!--显示实时行情栏-->
	<div class="btc-rem-all o_h_z">
		 <div class="hangqing-pic"><p><?php echo ($goods); ?></p>实时行情</div>
		 <div class="btc-rem-cont-box p_rel">
		 <div class="dangqianyue-msg-box"><!--提示框-->
			 <i></i>
			 <div class="icon_wen-cont">实时行情：交易期间的即时成交行情</div>
		 </div>
		 <table width="100%">
				 <tbody><tr>
					  <td width="260" height="65">
						   <!--显示实时行情栏-详细-->
						   <div class="btc-rem-cont o_h_z">
								<div class="cur sell" id="BTC_RMB_rate"><?php echo (($market["now"])?($market["now"]):0); ?></div>
								<div class="arrow-large <?php if(($market["flag"]) == "5"): ?>arrow-fall<?php else: ?>arrow-up<?php endif; ?>" id="price_arrow"></div>
						   </div>
						   <div class="time-box">
								<b id="time"><?php echo (($market["addtime"])?($market["addtime"]):'5'); ?></b>
						   </div>
					  </td>
					  <td>
						   <div class="f-14-color">最低价</div>
						   <div class="f_73a801 f-shuzhi"><span id="low_price" class="sell"><?php echo (($market["low"])?($market["low"]):0); ?></span></div>
					  </td>
					  <td>
						   <div class="f-14-color">最高价</div>
						   <div class="f_ff3600 f-shuzhi"><span id="high_price" class="buy"><?php echo (($market["high"])?($market["high"]):0); ?></span></div>
					  </td>
					  <td>
						   <div class="f-14-color">成交量</div>
						   <div class="f-333 f-shuzhi"><font face="Tahoma"></font><span id="volume"><?php echo (($market["volume"])?($market["volume"]):0); ?></span></div>
					  </td>
				 </tr>   
		 </tbody></table>
		 </div>
	</div>
	<style>
	.mod_market #highcharts-0{ width:704px !important;}
	.mod_market{ width:704px;}
	.mod_market #container22{width: 712px; height: 470px;}
	#chart_area{border:1px solid #ccc;}
	.tabs li{cursor:pointer;}
	</style>
	<div class="mod_market">
	  <div class="m_hd">
		  <ul class="tabs" id="markettabs">
			  <li <?php if(($line) == "60"): ?>class="cur"<?php endif; ?> name="60"><i></i><a>1分线</a></li>
			  <li <?php if(($line) == "300"): ?>class="cur"<?php endif; ?> name="300"><i></i><a>5分线</a></li>
			  <li <?php if(($line) == "900"): ?>class="cur"<?php endif; ?> name="900"><i></i><a>15分线</a></li>
			  <li <?php if(($line) == "1800"): ?>class="cur"<?php endif; ?> name="1800"><i></i><a>30分线</a></li>
			  <li <?php if(($line) == "3600"): ?>class="cur"<?php endif; ?> name="3600"><i></i><a>1小时线</a></li>
			  <li <?php if(($line) == "14400"): ?>class="cur"<?php endif; ?> name="14400"><i></i><a>4小时线</a></li>
			  <li <?php if(($line) == "86400"): ?>class="cur"<?php endif; ?> name="86400"><i></i><a>日K线</a></li>
		  </ul>
	  </div>
	  <div class="m_bd">
		  <div id="chart_area"></div>
	  </div>
	  <script type="text/javascript">
		$(document).ready(function(){
			$('#markettabs li').click(function(){
				$('#realtime').load('/?s=Home/Index/chart/line/'+$(this).attr('name')+'/id/<?php echo ($id); ?>/goods/<?php echo ($goods); ?>/coin/<?php echo ($coin); ?>');
                var sc = null;
                clearInterval(sc);
				var line = $(this).attr('name');
				var goods = "<?php echo ($goods); ?>";
				var coin = "<?php echo ($coin); ?>";
				sc = setInterval(function(){
				    $('#realtime').load('/?s=Home/Index/chart/line/'+line+'/id/<?php echo ($id); ?>/goods/'+goods+'/coin/'+coin);
				},360000);
			});
		});
	  </script>
	</div>
</div>
<script src="/Public/js/jquery.js"></script>
<script src="/Public/js/highstock.src.js"></script>
<script>
 $(document).ready(function(){
   var datas =<?php echo ($chart); ?> , rates = [], vols = [];
   for(i = 0; i < datas.length; i++){
     rates.push([datas[i][0], parseFloat(datas[i][2],10), parseFloat(datas[i][3],10), parseFloat(datas[i][4],10), parseFloat(datas[i][5],10)]);
     vols.push([datas[i][0], parseFloat(datas[i][1],10)]);
   }

   Highcharts.setOptions({
     lang: {
       loading: 'Loading...',
       months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
       shortMonths: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
       weekdays: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
       decimalPoint: '.',
       numericSymbols: ['k', 'M', 'G', 'T', 'P', 'E'],
       resetZoom: 'Reset zoom',
       resetZoomTitle: 'Reset zoom level 1:1',
       thousandsSep: ','
     },
     credits: {enabled: false},
     global:{useUTC:false},
   });

   new Highcharts.StockChart({
     chart: { renderTo: 'chart_area'},
     xAxis: { type: 'datetime' },
     legend: { enabled: false },
     tooltip: { xDateFormat: '%Y-%m-%d %H:%M %A', changeDecimals: 4 },
     scrollbar: {enabled: false},
     navigator: {enabled: false},
     rangeSelector:{enabled: false},
	 plotOptions: { candlestick: {color: '#f01717',upColor: '#0ab92b'},column: {color: '#4572A7'}},
     yAxis: [
       { top:0,left:35,width:650,height:270, title: { text: '价格 [<?php echo ($coin); ?>]' }},
       { top:270,height:100,title: { text: '成交量 [<?php echo ($goods); ?>]' },min:0,max:<?php echo ($volmax); ?>,opposite: false }
     ],
     series: [
       { animation: false, name: '成交量 [<?php echo ($goods); ?>]', type: 'column', marker: { enabled: false }, yAxis: 1, data: vols },
       { animation: false, name: '价格 [<?php echo ($coin); ?>]', type: 'candlestick', marker: { enabled: false }, data: rates }
     ]
   });
 });
</script>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Description" content="<?php echo ($sys["description"]); ?>" />
<meta name="keywords" content="<?php echo ($sys["keyword"]); ?>" />
<link rel="stylesheet" type="text/css" href="/Public/Home/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/yii.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/styles.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/public.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/jbox.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/jquery-ui.css">
<script type="text/javascript" src="/Public/Home/js/jquery.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.yiiactiveform.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.ba-bbq.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/config.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.jBox-2.3.min.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.jBox-zh-CN.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery-ui.js"></script>
<title><?php echo ($sys["title"]); ?></title>
<script  language="JavaScript"> 
          <!--
    //      function stopError() {
         //   return true;
  //        }
     //     window.onerror = stopError;
           -->         
</script>
</head>
<body>
<!--最顶上的内容-->
<div class="top-fixed-all">
      <div class="top-fixed">
            <div class="container">
                  <div class="top-fixed-info">
                        最新成交价:<span class="f-ff4000 BTC_RMB_rate" style=""><?php echo (($now["now"])?($now["now"]):0.07); ?></span>24小时成交币量:<span class="buy_price f-ff4000" style=""><?php echo (($now["total"])?($now["total"]):84567); ?></span>24小时成交金额量:<span class="f-7eb800 sell_price" style=""><?php echo (($now["totalprice"])?($now["totalprice"]):5849); ?></span>
                  </div>
                  <!--登录状态-->
                  <div class="top-fixed-user">
				       <?php if($login_user_id > 0 ): ?><div>
						   <div class="ll mt4 mr10">
						   </div>
						   <div class="user-msg-all">
							   <div class="f_ddd" id="user-hover" style="width: 44px;color:#ddd;"><em><?php echo ($login_user_name); ?></em><i></i></div>
							   <div class="user-msg">
								   <p><a class="mr15" href="<?php echo ($path); ?>/User">用户信息</a><a class="mr15" href="<?php echo ($path); ?>/User/detail">财务明细</a><a href="<?php echo ($path); ?>/User/chongzhi">充值</a></p>
							   </div>
						   </div>
						   <div class="ll"><a href="<?php echo ($path); ?>/Login/loginout">退出</a></div>
						   <div class="clear"></div>
					   </div>
					   <script language="javascript">
					       $('.user-msg-all').mouseover(function(){
						       $('.user-msg').css('display','block');
						   });

						   $('.user-msg-all').mouseout(function(){
						       $('.user-msg').css('display','none');
						   });
					   </script>
					   <?php else: ?>
					   <div class="unsign">
						   <div class="ll mt4 mr10">
						   </div>
						   <a href="<?php echo ($path); ?>/Login/reg">免费注册</a><a href="<?php echo ($path); ?>/Login">登录</a>
                       </div><?php endif; ?>
                  </div>
                  <div class="clear"></div>
            </div>
      </div>
</div>
<div class="mt30">
    <!--网站升级中提示-->
    <div class="top-cont-msg">   
     <div class="content-top"></div>    </div>
    <!--[if IE 6]>
    <div class="kie-bar">
      您使用的浏览器版本过低，为了您的资金安全和更好地用户体验，建议立即升级
      <a href="http://windows.microsoft.com/zh-cn/internet-explorer/download-ie" seed="kie-setup-IE8" target="_blank" >
      <i class="kie-bar-icon-ie"></i>Internet Explorer</a>
      或
      <a href="https://www.google.com/intl/zh-CN/chrome/" seed="kie-setup-IE8" target="_blank" >
      <i class="kie-bar-icon-chrome"></i>Google Chrome</a>
    </div>
    <style>
      .kie-bar {
        height: 24px;
        line-height: 1.8;
        font-weight:normal;
        text-align: center;
        border-bottom:1px solid #fce4b5;
        background-color:#FFFF9B;
        color:#e27839;
        position: relative;
        font-size: 14px;
        text-shadow: 0px 0px 1px #efefef;
        padding: 5px 0;
      }
      .kie-bar a {
        color:#08c;
        text-decoration: none;
      }
    </style>
    <![endif]--> 
    <!--头部-->
    <div class="container">
    <div class=" o_h_z" style="width:1000px;">
           <div class="logo-index"><a href="/"><img src="/<?php echo ($sys["logo"]); ?>"/></a></div>
           <!--导航-->
           <div class="nav-bar rr">
                 <ul>
                    <li class="cur"><a href="/">首页</a></li>
                    <li><a href="<?php echo ($path); ?>/Hall">交易大厅</a></li>
                    <li><a href="<?php echo ($path); ?>/User">我的账户</a></li>
					<li><a href="<?php echo ($path); ?>/User/buy">中元币认购</a></li>
                    <li><a href="<?php echo ($path); ?>/Art/index/cate/news">网站公告</a></li>  
					<li style="border-right:0px none;"><a href="<?php echo ($path); ?>/Art">一般问题</a></li>         
                 </ul>
           </div>
    </div>
    </div>
</div>
<!--交易页面的子导航区-->
<div class="subnav">
      <div class="container">
            <ul>
                 <li <?php if(($cur) == "user"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User">账户信息</a><i></i>
				 </li>
                 <li <?php if(($cur) == "detail"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/detail">财务明细</a><i></i>
				 </li>
                 <li <?php if(($cur) == "safe"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/safe">安全中心</a><i></i>
				 </li>
                 <li <?php if(($cur) == "invit"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/invit">我的邀请</a><i></i>
				 </li>
                 <li <?php if(($cur) == "showUserBank"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/showUserBank">银行卡管理</a>
				 </li>
				 <li <?php if(($cur) == "chongzhi"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/chongzhi">账户充值</a>
				 </li>
				 <li <?php if(($cur) == "tixian"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/tixian">账户提现</a>
				 </li>
				 <li <?php if(($cur) == "bank"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/bank">存款(暂停）</a>
				 </li>
				 <li <?php if(($cur) == "buy"): ?>class="cur"<?php endif; ?>>
				     <a href="<?php echo ($path); ?>/User/buy">我的认购</a>
				 </li>
            </ul>
      </div>    
</div>

<div class="container">
<!--全站交易记录-->
<div class="trade-part trade-part-hd mt20">
  <div class="trade-hd">
        <h5><i class="icon_condition"></i>账户提现</h5>
  </div>
  <div class="md">
    <div class="my-grid" id="order-grid" style="margin-bottom:20px;">
	    <form action="<?php echo ($path); ?>/User/tixian" method="post" id="formid">
			<table class="items table table-striped table-bordered table-condensed chkreal">
			<tbody>
			<tr>
				<td>币种：</td>
				<td>
				    <select id='typeid' name="typeid">
					    <?php if(is_array($types)): $i = 0; $__LIST__ = $types;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" <?php if(($vo["tid"]) == $vo["id"]): ?>selected<?php endif; ?>><?php echo ($vo["nickname"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
					</select>
					当前余额：<input type="text" name="coin" readonly style="width:150px;"/>
				</td>
			</tr>
			<tr><td>提现金额：</td><td><input type="text" name="goldnum" placeholder="只能在<?php echo ($tixiand); ?>-<?php echo ($tixianu); ?>元"/></td></tr>
			<tr><td>实际到帐：</td><td><input type="text" name="realnum" readonly value="0"/></td></tr>
			<tr><td>手续费：</td><td><input type="text" name="shouxu" readonly value="<?php echo ($txfee); ?>"/> %</td></tr>
			<tr>
                <td>提现地址：</td><td>
                <select name="url">
                <?php if(is_array($Arr)): $i = 0; $__LIST__ = $Arr;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$voA): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($voA); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>

            </td>
            </tr>
            <tr>
                <td>户主姓名：</td><td><input type="text" readonly value="<?php echo ($username); ?>"/>



            </td>

            </tr>
            </tr>
			<tr><td>交易密码：</td><td><input type="password" name="transpw" placeholder="请输入交易密码"/></td></tr>
			<tr><td>验证码：</td><td><input type="text" name="code" placeholder="请输入验证码" style="width:100px;"/><input type="button" value="发送验证码" class="btn" id="sendbtn"/></td></tr>
			<tr><td colspan=2><input type="button" id="subbtn" class="btn" value="确定" style="width:110px;"/></td></tr>
			</tbody>
			</table>
		</form>
	</div>
	<div class="my-grid" id="order-grid">
		<table class="items table table-striped table-bordered table-condensed saferoom">
		<thead>
		<tr><th>币种</th><th>金额</th><th>实际到帐</th><th>手续费</th><th>提现地址</th><th>时间</th><th width="10%">状态</th></tr>
		</thead>
		<tbody>
		<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr><td class="chkTit"><?php echo (($vo["nickname"])?($vo["nickname"]):'无'); ?></td><td><?php echo (($vo["goldnum"])?($vo["goldnum"]):'0'); ?></td><td><?php echo (($vo["realnum"])?($vo["realnum"]):'0'); ?></td><td><?php echo (($vo["shouxu"])?($vo["shouxu"]):'0'); ?> %</td><td><?php echo (($vo["url"])?($vo["url"]):'0'); ?></td><td><?php echo (($vo["addtime"])?($vo["addtime"]):'无'); ?></td><td><?php if(($vo["status"]) == "0"): ?>正在处理<?php else: ?>已处理<?php endif; ?></td></tr><?php endforeach; endif; else: echo "$empty" ;endif; ?>
		</tbody>
		</table>
	</div>
  </div>
</div>
</div>
<!--尾部-->
<div class="footer-all">
     <div class="container footer-part sitelink grid-990">
           <div class="rr">
                 <div class="cont-box cont-box-first">
                       <h6>客户服务</h6>
                       <ul>
					       <?php if(is_array($serv)): $i = 0; $__LIST__ = $serv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
                 <div class="cont-box">
                       <h6>关于我们</h6>
                       <ul>
                           <?php if(is_array($about)): $i = 0; $__LIST__ = $about;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
                 <div class="cont-box">
                       <h6>服务条款</h6>
                       <ul>
                           <?php if(is_array($mserv)): $i = 0; $__LIST__ = $mserv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
				 <div class="cont-box"><img src="/Public/Home/images/weixin.png"/></div>
                 <div class="cont-box cont-box-last">
                       <h6><i></i>风险提示</h6>
                       <p><?php echo ($warn["content"]); ?></p>
                 </div>
				 <div style="clear:both"></div>
           </div>
     </div>
      <div class="t_c grid-990 sitecopyright"><?php echo ($sys["copyright"]); echo ($sys["tongji"]); ?><br/>平台支持：<a href="http://www.vike5.com" target="_blank">威客网</a> 湘ICP备10001691号
</div>
	<div class="t_c grid-990 siteauth"></div>
</div>
<div id="alert_room1"></div>
<script language="javascript" src="/Public/js/alert.js"></script>

<div class="serv-qq">
<div class="serv-tit">在线咨询</div>
<dl>
	<?php if(is_array($sys["qq"])): $i = 0; $__LIST__ = $sys["qq"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dt><?php echo ($vo["name"]); ?></dt><dd><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo ($vo["text"]); ?>&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo ($vo["text"]); ?>:41" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>
	    </dd><?php endforeach; endif; else: echo "" ;endif; ?>
</dl>
<div class="clear"></div>
</div>
<!-- footer -->

<div class="modal hide fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

<!--[if IE 6]><script src="/js/ie-6.js"></script><![endif]-->
<script type="text/javascript" src="/Public/Home/js/jquery.SuperSlide.js"></script>
<script type="text/javascript" src="/Public/Home/js/slideTxtBox.js"></script>
<script type="text/javascript" src="/Public/Home/js/bootbox.js"></script>
<script type="text/javascript" src="/Public/Home/js/box.js"></script>
<script type="text/javascript" src="/Public/Home/js/lang.js"></script>
<script type="text/javascript" src="/Public/Home/js/responseHandler.js"></script>
<script type="text/javascript" src="/Public/Home/js/jquery.yiigridview.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
jQuery('body').popover({'selector':'a[rel=popover]'});
jQuery('#order-grid').yiiGridView({'ajaxUpdate':['order-grid'],'ajaxVar':'ajax','pagerClass':'pagination','loadingClass':'grid-view-loading','filterClass':'filters','tableClass':'items table table-striped table-bordered table-condensed','selectableRows':1,'enableHistory':false,'updateSelector':'{page}, {sort}','filterSelector':'{filter}','pageVar':'CurrencyTransaction_page'});
});
/*]]>*/

$(document).ready(function(){
    $('#typeid').change(function(){
        var flag="<?php echo ($tixianflag); ?>";
        if(flag){

            var Al=$(this).children('option:selected').val();
            if(Al!='5'){
                msgDalog('目前只支持人民币提现！');
                $("#subbtn").attr("disabled", true);
            }else{
                $("#subbtn").attr("disabled", false);
            }
        }
    })
});

$(document).ready(function(){
	$('#subbtn').click(function(){
		var goldnum = $('input[name=goldnum]').val();
	    if(goldnum=="" || goldnum <= 0){
		    msgDalog("请输入提现金额！","",0);
		}else if(isNaN(goldnum)){
			msgDalog('提现金额应该是数字！');
			$('input[name=goldnum]').val('');
			return false;
		}else if(goldnum><?php echo ($RMBYE); ?>){
            msgDalog("提现不能超过你的余额里面的钱，你现在最多可提现<?php echo ($RMBYE); ?>元！","",0);
            $('input[name=goldnum]').val('');
            return false;
    }else if(goldnum < <?php echo ($tixiand); ?>||goldnum > <?php echo ($tixianu); ?>){

            msgDalog("提现金额必须在<?php echo ($tixiand); ?>-<?php echo ($tixianu); ?>元之间！","",0);
            $('input[name=goldnum]').val('');
            return false;
        }else if($('input[name=url]').val()==""){
		    msgDalog("你没有设置提现地址，请到钱包管理设置！","",0);
		}else if($('input[name=code]').val()==""){
		    msgDalog("请输入验证码！","",0);
		}else{
			var pos = goldnum.indexOf('.');
			if(pos != -1){
				if(goldnum.substring(0,pos)=='') {
					msgDalog('提现金额不合法！');
					return false;
				}
				len = goldnum.substring(pos+1,goldnum.length);
				if(len.length > 8) {
					msgDalog('提现金额小数位数不能超过8位！');
					return false;
				}
                $('#formid').submit();
			}else{
		        $('#formid').submit();
			}
		}
	});

	$('input[name=goldnum]').keyup(function(){

	    var goldnum = $('input[name=goldnum]').val();

		if(isNaN(goldnum)){
			msgDalog('提现金额应该是数字！');
			$('input[name=goldnum]').val('');
			return false;
		}

        var pos = goldnum.indexOf('.');
		if(pos != -1){

			if(goldnum.substring(0,pos)=='') {
				msgDalog('提现金额不合法！');
				$('input[name=goldnum]').focus();
				return false;
			}
			len = goldnum.substring(pos+1,goldnum.length);
			if(len.length > 8) {
				v = goldnum.substring(0,pos) + '.' + goldnum.substring(pos+1,pos+9);
				$('input[name=goldnum]').val(v);
				goldnum = v;
			}
		}

		var shouxu = $('input[name=shouxu]').val();
		realnum = goldnum - goldnum * shouxu / 100;
		if(realnum > 0) {
			realnum = String(realnum);
			var pos = realnum.indexOf('.');
			if(pos != -1){
				len = realnum.substring(pos+1,realnum.length);

				if(len.length > 8) {
					realnum = realnum.substring(0,pos) + '.' + realnum.substring(pos+1,pos+9);
				}
			}
		}

        $('input[name=realnum]').val(realnum);
	});

	$('input[name=goldnum]').change(function(){

	    var goldnum = $('input[name=goldnum]').val();

		if(isNaN(goldnum)){
			msgDalog('提现金额应该是数字！');
			$('input[name=goldnum]').val('');
			return false;
		}

        var pos = goldnum.indexOf('.');
		if(pos != -1){

			if(goldnum.substring(0,pos)=='') {
				msgDalog('提现金额不合法！');
				$('input[name=goldnum]').focus();
				return false;
			}
			len = goldnum.substring(pos+1,goldnum.length);
			if(len.length > 8) {
				v = goldnum.substring(0,pos) + '.' + goldnum.substring(pos+1,pos+9);
				$('input[name=goldnum]').val(v);
				goldnum = v;
			}
		}

		var shouxu = $('input[name=shouxu]').val();
		realnum = goldnum - goldnum * shouxu / 100;
		if(realnum > 0) {
			realnum = String(realnum);
			var pos = realnum.indexOf('.');
			if(pos != -1){
				len = realnum.substring(pos+1,realnum.length);

				if(len.length > 8) {
					realnum = realnum.substring(0,pos) + '.' + realnum.substring(pos+1,pos+9);
				}
			}
		}

        $('input[name=realnum]').val(realnum);
	});
});

$(document).ready(function(){
    $('#sendbtn').click(function(){
		$('#sendbtn').attr('disabled','disabled');
		var moble = "<?php echo ($user_info["moble"]); ?>";
		$.post('/?s=Home/Call/run',{phone:moble},function(flag){
			if(flag==1){
				alert('语音已开启，请注意查收验证码！');
			}else{
				alert('验证失败！请重新验证。');
			}
		});
	    var i=60;
		var si = setInterval(function(){
		    $('#sendbtn').val('（'+i+'）秒后可再次获取');

			if(i<=0) {
				clearInterval(si);
				$('#sendbtn').val('发送验证码');
				$('#sendbtn').attr('disabled',false);
			}
			i--;
		},1000);
	});

	var czurl = new Array();
	var czcoin = new Array();
	var czyuan = new Array();
	var rmbs = '';
	<?php if(is_array($types)): $i = 0; $__LIST__ = $types;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>czurl[<?php echo ($vo["id"]); ?>] = "<?php echo ($vo["url"]); ?>";
		czcoin[<?php echo ($vo["id"]); ?>] = "<?php echo ($vo["coin"]); ?>";
		czyuan[<?php echo ($vo["id"]); ?>] = "<?php echo ($vo["yuan"]); ?>";

		if(<?php echo ($vo["yuan"]); ?>==1){
		    <?php if(is_array($vo["url"])): $i = 0; $__LIST__ = $vo["url"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($i % 2 );++$i;?>rmbs += '<option value="'+"<?php echo ($vv); ?>"+'">'+"<?php echo ($vv); ?>"+'</option>';<?php endforeach; endif; else: echo "" ;endif; ?>
		}<?php endforeach; endif; else: echo "" ;endif; ?>
    $('select[name=typeid]').change(function(){
		if(czyuan[$(this).val()]==1){
			$('#urlshow').html('<select name="url">'+rmbs+'</select>');
		}else{
			$('#urlshow').html('<input type="text" name="url" readonly value="<?php echo ($url); ?>"/>');
			$('input[name=url]').val(czurl[$(this).val()]);
		}
		$('input[name=coin]').val(czcoin[$(this).val()]);
	});

	var typeid = $('select[name=typeid]').val();
	if(czyuan[typeid]==1){
		$('#urlshow').html('<select name="url">'+rmbs+'</select>');
	}else{
		$('#urlshow').html('<input type="text" name="url" readonly value="<?php echo ($url); ?>"/>');
		$('input[name=url]').val(czurl[typeid]);
	}

	$('input[name=coin]').val(czcoin[typeid]);
});
</script>


</body></html>
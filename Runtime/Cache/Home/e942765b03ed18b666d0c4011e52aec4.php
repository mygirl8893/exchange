<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Description" content="<?php echo ($sys["description"]); ?>" />
<meta name="keywords" content="<?php echo ($sys["keyword"]); ?>" />
<link rel="stylesheet" type="text/css" href="/Public/Home/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/styles.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/public.css">
<link rel="stylesheet" type="text/css" href="/Public/Home/css/jquery-ui.css">
<title><?php echo ($sys["title"]); ?></title>
<script  language="JavaScript"> 
          <!--
    //      function stopError() {
         //   return true;
  //        }
     //     window.onerror = stopError;
           -->         
</script>
</head>
<body>
<!--最顶上的内容-->
<div class="top-fixed-all">
      <div class="top-fixed">
            <div class="container">
                  <div class="top-fixed-info">
                        最新成交价:<span class="f-ff4000 BTC_RMB_rate" style=""><?php echo (($now["now"])?($now["now"]):0.07); ?></span>24小时成交币量:<span class="buy_price f-ff4000" style=""><?php echo (($now["total"])?($now["total"]):84567); ?></span>24小时成交金额量:<span class="f-7eb800 sell_price" style=""><?php echo (($now["totalprice"])?($now["totalprice"]):5849); ?></span>
                  </div>
                  <!--登录状态-->
                  <div class="top-fixed-user">
				       <?php if($login_user_id > 0 ): ?><div>
						   <div class="ll mt4 mr10">
						   </div>
						   <div class="user-msg-all">
							   <div class="f_ddd" id="user-hover" style="width: 44px;color:#ddd;"><em><?php echo ($login_user_name); ?></em><i></i></div>
							   <div class="user-msg">
								   <p><a class="mr15" href="<?php echo ($path); ?>/User">用户信息</a><a class="mr15" href="<?php echo ($path); ?>/User/detail">财务明细</a><a href="<?php echo ($path); ?>/User/chongzhi">充值</a></p>
							   </div>
						   </div>
						   <div class="ll"><a href="<?php echo ($path); ?>/Login/loginout">退出</a></div>
						   <div class="clear"></div>
					   </div>
					   <script language="javascript">
					       $('.user-msg-all').mouseover(function(){
						       $('.user-msg').css('display','block');
						   });

						   $('.user-msg-all').mouseout(function(){
						       $('.user-msg').css('display','none');
						   });
					   </script>
					   <?php else: ?>
					   <div class="unsign">
						   <div class="ll mt4 mr10">
						   </div>
						   <a href="<?php echo ($path); ?>/Login/reg">免费注册</a><a href="<?php echo ($path); ?>/Login">登录</a>
                       </div><?php endif; ?>
                  </div>
                  <div class="clear"></div>
            </div>
      </div>
</div>
<div class="mt30">
    <!--网站升级中提示-->
    <div class="top-cont-msg">   
     <div class="content-top"></div>    </div>
    <!--[if IE 6]>
    <div class="kie-bar">
      您使用的浏览器版本过低，为了您的资金安全和更好地用户体验，建议立即升级
      <a href="http://windows.microsoft.com/zh-cn/internet-explorer/download-ie" seed="kie-setup-IE8" target="_blank" >
      <i class="kie-bar-icon-ie"></i>Internet Explorer</a>
      或
      <a href="https://www.google.com/intl/zh-CN/chrome/" seed="kie-setup-IE8" target="_blank" >
      <i class="kie-bar-icon-chrome"></i>Google Chrome</a>
    </div>
    <style>
      .kie-bar {
        height: 24px;
        line-height: 1.8;
        font-weight:normal;
        text-align: center;
        border-bottom:1px solid #fce4b5;
        background-color:#FFFF9B;
        color:#e27839;
        position: relative;
        font-size: 14px;
        text-shadow: 0px 0px 1px #efefef;
        padding: 5px 0;
      }
      .kie-bar a {
        color:#08c;
        text-decoration: none;
      }
    </style>
    <![endif]--> 
    <!--头部-->
    <div class="container">
    <div class=" o_h_z" style="width:1000px;">
           <div class="logo-index"><a href="/"><img src="/<?php echo ($sys["logo"]); ?>"/></a></div>
           <!--导航-->
           <div class="nav-bar rr">
                 <ul>
                    <li class="cur"><a href="/">首页</a></li>
                    <li><a href="<?php echo ($path); ?>/Hall">交易大厅</a></li>
                    <li><a href="<?php echo ($path); ?>/User">我的账户</a></li>
					<li><a href="<?php echo ($path); ?>/User/buy">中元币认购</a></li>
                    <li><a href="<?php echo ($path); ?>/Art/index/cate/news">网站公告</a></li>  
					<li style="border-right:0px none;"><a href="<?php echo ($path); ?>/Art">一般问题</a></li>         
                 </ul>
           </div>
    </div>
    </div>
</div>
<!--未登录前首页-->
<div class="not-loginbar">
     <?php if($login_user_id > 0 ): ?><!--已登录栏-->
	 <div class="loginbar-all">
         <div class="loginbar logined">
		     <div class="login-box"><span><?php echo ($login_user_name); ?>，欢迎您！</span></div>
			 <div class="login-box"><i class="zh-icon"></i><a href="<?php echo ($path); ?>/User/">账户信息</a></div>
			 <div class="login-box"><i class="cw-icon"></i><a href="<?php echo ($path); ?>/User/detail">财务明细</a></div>
			 <div class="login-box"><i class="cz-icon"></i><a href="<?php echo ($path); ?>/User/chongzhi">充值</a></div>
			 <button onclick="javascript:location.href='<?php echo ($path); ?>/Hall'" style="background: #ef7c1a;border-radius: 2px;width: 232px;height: 42px;line-height: 42px;color: #fff;font-size: 16px;cursor: pointer;text-shadow: 0 1px 2px #e27316;padding:0px;border:0px none;">进入交易大厅</button>
         </div>
	 </div>
	 <?php else: ?>
     <!--未登录栏-->
     <div class="loginbar-all">
		 <form class="frontend form-horizontal" id="login-form" action="<?php echo ($path); ?>/Login/go" method="post">
			 <div class="loginbar" id="loginbar">
				 <div class="login-box icon_user">
					 <div class="control-group">
						 <label class="control-label required">登录名 <span class="required">*</span></label>
						 <div class="controls">
							 <input placeholder="手机/邮箱/用户名" name="username" id="LoginForm_login" type="text">
						 </div>
					 </div>
				 </div>
				 <div class="login-box icon_password">
					 <div class="control-group ">
						 <label class="control-label required">登陆密码 <span class="required">*</span></label>
						 <div class="controls">
							 <input placeholder="登陆密码" name="password" id="LoginForm_password" type="password">
						 </div>
					 </div>
				 </div>
				 <div class="login-code">
					 <input type="text" name="code" placeholder="请输入验证码" style="width:118px;height:20px;">
					 <img id="refresh" onclick="document.getElementById('refresh').src='<?php echo ($path); ?>/Login/checkcode/t/'+Math.random()" src="<?php echo ($path); ?>/Login/checkcode/" style="margin-left:5px;"/>
				 </div>
				 <div class="btn_button240"><button type="submit" name="yt1">登 录</button></div>
				 <p><a href="<?php echo ($path); ?>/Login/lostpwd" class="f_aaa">忘记密码？</a>&nbsp;&nbsp;<a href="<?php echo ($path); ?>/Login/reg/" class="f-ff9900 f-w">会员注册</a></p>
			 </div>
         </form>
         <div class="loginbar-bg" id="loginbar-bg"></div>
     </div><?php endif; ?>
     <!--轮播图片-->

     <div class="slideBox">
		  <div class="hd"><ul><?php if(is_array($ad)): $k = 0; $__LIST__ = $ad;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><li><?php echo ($k); ?></li><?php endforeach; endif; else: echo "" ;endif; ?></ul></div>
           <div class="bd">
                <ul>
				    <?php if(is_array($ad)): $i = 0; $__LIST__ = $ad;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><img src="/<?php echo ($vo["img"]); ?>"/></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
           </div>
     </div>
</div>
<style type="text/css">
.slideBox{ width:100%; height:315px; overflow:hidden; position:relative;    } 
.slideBox .hd{ height:15px; overflow:hidden; position:absolute; right:50%; bottom:10px; z-index:1; } 
.slideBox .hd ul{ overflow:hidden; zoom:1; float:left;  } 
.slideBox .hd ul li{ border-radius:15px; width:12px; height:12px; background:#fff; opacity:0.3;filter:alpha(opacity=30); text-indent:-1000em; float:left; margin-right:10px; cursor:pointer; font-size:0;} 
.slideBox .hd ul li.on{opacity:1;filter:alpha(opacity=100)} 
.slideBox .bd{ position:relative; height:100%; z-index:0;   } 
.slideBox .bd img{ width:100%; height:315px; } 
</style>


<!--内容区-->
<div class="container" id="page">
    <div class=" o_h_z mt10" id="realtime">
    </div>
	  
	  
</div>
<div class="container">
    <div class=" o_h_z mt10">
      <!--左边栏-->
      <div class="part-l">
         <div class="answer">
              <div class="it-box">
                    <div class="itimg answer-safe"></div>
                    <div class="ittext">
                          <h1>安全高效</h1>
                          <p>银行级系统SSL安全连接，短信身份验证机制以及分布式离线钱包备份机制<br>
        骨灰级机房设备，实时高效处理数据</p>
                    </div>
              </div>
              <div class="it-box">
                    <div class="itimg answer-service"></div>
                    <div class="ittext">
                          <h1>专业服务</h1>
                          <p>团队拥有多年互联网金融项目经验多位资深金融顾问和法律顾问<br>7*24小时客服专业服务</p>
                    </div>
              </div>
              <div class="it-box">
                    <div class="itimg answer-easy"></div>
                    <div class="ittext">
                          <h1>方便易用</h1>
                          <p>重视用户体验，界面友好，方便易用专业UI和数据分析，持续更新设计更专业易用的网站</p>
                    </div>
              </div>
              <div class="it-box">
                    <div class="itimg answer-fee"></div>
                    <div class="ittext">
                          <h1>低费率</h1>
                          <p>交易平台费率低于其他网站平均水平</p>
                    </div>
              </div>
         </div>
      </div>
      <!--右边栏-->
      <div class="part-r">
	       <div class="trade-part-hd" style="background:#eee;padding:10px;">
				 <div class="trade-hd">
					   <h6><i class="icon_entrus"></i>买卖委托盘</h6>
				 </div>
				 <div class="md">
					 <table width="100%">
					    <thead>
						    <tr>
							   <th>类型</th>
							   <th>价格<font face="微软雅黑">(￥)</font></th>
							   <th>数量(฿)</th>
						    </tr>
						</thead>
						<tbody id="selllist">
						<?php if(is_array($sells)): $k = 0; $__LIST__ = $sells;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tr class="trade-tr<?php echo ($k); ?>">
							<td class="sell">卖</td>
							<td><span class="sub_price"><?php echo ($vo["price"]); ?></span></td>
							<td><span class="sub_amount"><?php echo ($vo["nums"]); ?></span></td>
							<td><span style="width:2.667px" class="buySpan"></span></td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
						</tbody>


						<tfoot id="buylist">
						<?php if(is_array($buys)): $k = 0; $__LIST__ = $buys;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tr class="trade-tr<?php echo ($k); ?>">
							<td class="buy">买</td>
							<td><span class="sub_price"><?php echo ($vo["price"]); ?></span></td>
							<td><span class="sub_amount"><?php echo ($vo["nums"]); ?></span></td>
							<td><span style="width:2.667px" class="sellSpan"></span></td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
						</tfoot>

							  
					 </table> 
				 </div>
		 </div>
         <!--网站公告-->
         <div class="information-r mt16">
               <h6>网站公告</h6>
               <ul>
                   <?php if(is_array($news)): $i = 0; $__LIST__ = $news;if( count($__LIST__)==0 ) : echo "没有找到数据" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "没有找到数据" ;endif; ?>
               </ul>
         </div>
      </div>
    </div>
</div>
<!--交易流程-->
<div class="trade-process">
    <div class="qun">
	<?php if(is_array($qun)): $i = 0; $__LIST__ = $qun;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><span><?php echo ($vo["name"]); ?><a href="<?php echo ($vo["url"]); ?>" target="_blank"><?php echo ($vo["text"]); ?></a></span><?php endforeach; endif; else: echo "" ;endif; ?>
	<div style="clear:both"></div>
	</div>
</div>
<!--尾部-->
<div class="footer-all">
     <div class="container footer-part sitelink grid-990">
           <div class="rr">
                 <div class="cont-box cont-box-first">
                       <h6>客户服务</h6>
                       <ul>
					       <?php if(is_array($serv)): $i = 0; $__LIST__ = $serv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
                 <div class="cont-box">
                       <h6>关于我们</h6>
                       <ul>
                           <?php if(is_array($about)): $i = 0; $__LIST__ = $about;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
                 <div class="cont-box">
                       <h6>服务条款</h6>
                       <ul>
                           <?php if(is_array($mserv)): $i = 0; $__LIST__ = $mserv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($path); ?>/Art/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                       </ul>
                 </div>
				 <div class="cont-box"><img src="/Public/Home/images/weixin.png"/></div>
                 <div class="cont-box cont-box-last">
                       <h6><i></i>风险提示</h6>
                       <p><?php echo ($warn["content"]); ?></p>
                 </div>
				 <div style="clear:both"></div>
           </div>
     </div>
      <div class="t_c grid-990 sitecopyright"><?php echo ($sys["copyright"]); echo ($sys["tongji"]); ?><br/>平台支持：<a href="http://www.vike5.com" target="_blank">威客网</a> 湘ICP备10001691号
</div>
	<div class="t_c grid-990 siteauth"></div>
</div>
<div id="alert_room1"></div>
<script language="javascript" src="/Public/js/alert.js"></script>

<div class="serv-qq">
<div class="serv-tit">在线咨询</div>
<dl>
	<?php if(is_array($sys["qq"])): $i = 0; $__LIST__ = $sys["qq"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dt><?php echo ($vo["name"]); ?></dt><dd><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo ($vo["text"]); ?>&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:<?php echo ($vo["text"]); ?>:41" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>
	    </dd><?php endforeach; endif; else: echo "" ;endif; ?>
</dl>
<div class="clear"></div>
</div>
<!-- footer -->
</body>
</html>
<script type="text/javascript" src="/Public/js/jquery.js"></script>
<script type="text/javascript" src="/Public/js/jQuery.blockUI.js"></script>
<script type="text/javascript" src="/Public/js/jquery.SuperSlide.js"></script>

<script type="text/javascript">
jQuery(".slideBox").slide( { mainCell:".bd ul",autoPlay:true} );
</script>
<script language="javascript">
$(document).ready(function(){
  $('#realtime').load('/?s=Home/Index/chart/line/60/id/<?php echo (($id)?($id):0); ?>/goods/<?php echo ($goods); ?>/coin/<?php echo ($coin); ?>');
  var sc = null;
  clearInterval(sc);
  var line = 60;
  var goods = "<?php echo ($goods); ?>";
  var coin = "<?php echo ($coin); ?>";
  sc = setInterval(function(){
	  $('#realtime').load('/?s=Home/Index/chart/line/'+line+'/id/<?php echo (($id)?($id):0); ?>/goods/'+goods+'/coin/'+coin);
  },360000);
});
</script>